﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class MainForm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.MenuStrip1 = New System.Windows.Forms.MenuStrip()
        Me.ArkivToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.NewProjectToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.OpenToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ImportExcelToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ImporteraWBSToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.WBSIIDAToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.LathundIIDAToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.OpenFileDialog1 = New System.Windows.Forms.OpenFileDialog()
        Me.WordFolderDialog = New System.Windows.Forms.FolderBrowserDialog()
        Me.OpenWordTempltateFileDialog = New System.Windows.Forms.OpenFileDialog()
        Me.WBSFileDialog = New System.Windows.Forms.OpenFileDialog()
        Me.TabPage6 = New System.Windows.Forms.TabPage()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.DateSort = New System.Windows.Forms.CheckBox()
        Me.Label44 = New System.Windows.Forms.Label()
        Me.Label43 = New System.Windows.Forms.Label()
        Me.SDate = New System.Windows.Forms.DateTimePicker()
        Me.EDate = New System.Windows.Forms.DateTimePicker()
        Me.TextBox2 = New System.Windows.Forms.TextBox()
        Me.Label34 = New System.Windows.Forms.Label()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.SearchButton = New System.Windows.Forms.Button()
        Me.ATRRisksCheckBox = New System.Windows.Forms.CheckBox()
        Me.ATROutPutCheckBox = New System.Windows.Forms.CheckBox()
        Me.ATRResultCheckBox = New System.Windows.Forms.CheckBox()
        Me.ATRInputsCheckbox = New System.Windows.Forms.CheckBox()
        Me.ATRScopeCheckbox = New System.Windows.Forms.CheckBox()
        Me.Label42 = New System.Windows.Forms.Label()
        Me.SearchText = New System.Windows.Forms.TextBox()
        Me.Label40 = New System.Windows.Forms.Label()
        Me.ListSelectProjectRButton = New System.Windows.Forms.RadioButton()
        Me.TextBox1 = New System.Windows.Forms.TextBox()
        Me.ListGenSubProjectNumber = New System.Windows.Forms.TextBox()
        Me.ListSelectStageRButton = New System.Windows.Forms.RadioButton()
        Me.ListSelectSubProjectRButton = New System.Windows.Forms.RadioButton()
        Me.CreateList = New System.Windows.Forms.Button()
        Me.ListSelectBox = New System.Windows.Forms.ListBox()
        Me.Label24 = New System.Windows.Forms.Label()
        Me.TabPage3 = New System.Windows.Forms.TabPage()
        Me.ChangeCatOnResourseButton = New System.Windows.Forms.Button()
        Me.UpdateResourceButton = New System.Windows.Forms.Button()
        Me.CatGrid = New System.Windows.Forms.DataGridView()
        Me.ResourceGrid = New System.Windows.Forms.DataGridView()
        Me.TabPage5 = New System.Windows.Forms.TabPage()
        Me.ATRInWordFormat = New System.Windows.Forms.CheckBox()
        Me.OpenPDFCatalog = New System.Windows.Forms.Button()
        Me.ATRPrintRevSignBox = New System.Windows.Forms.TextBox()
        Me.ATRPrintVersionBox = New System.Windows.Forms.TextBox()
        Me.ATRRevSignRadioButton = New System.Windows.Forms.RadioButton()
        Me.ATRVersionRadioButton = New System.Windows.Forms.RadioButton()
        Me.ATRDateRadioButton = New System.Windows.Forms.RadioButton()
        Me.ATRSelectGroupeBox = New System.Windows.Forms.GroupBox()
        Me.ATRRevDateRadioButton = New System.Windows.Forms.RadioButton()
        Me.ATRPrintDatePicker = New System.Windows.Forms.DateTimePicker()
        Me.ATREndDateRadioButton = New System.Windows.Forms.RadioButton()
        Me.ATRStartDateRadioButton = New System.Windows.Forms.RadioButton()
        Me.WordFolderLabel = New System.Windows.Forms.Label()
        Me.WordFilepath = New System.Windows.Forms.Button()
        Me.Label26 = New System.Windows.Forms.Label()
        Me.ATRRadioButton = New System.Windows.Forms.RadioButton()
        Me.StageATRListRadioButton = New System.Windows.Forms.RadioButton()
        Me.SubprojectATRListRadioButton = New System.Windows.Forms.RadioButton()
        Me.FullATRListRadioButton = New System.Windows.Forms.RadioButton()
        Me.CreateATR = New System.Windows.Forms.Button()
        Me.ATRMemo = New System.Windows.Forms.TabPage()
        Me.ATRNrMemo = New System.Windows.Forms.Label()
        Me.Label39 = New System.Windows.Forms.Label()
        Me.ATRMemoSave = New System.Windows.Forms.Button()
        Me.MemoText = New System.Windows.Forms.RichTextBox()
        Me.TabPage7 = New System.Windows.Forms.TabPage()
        Me.TotalCostATR = New System.Windows.Forms.Label()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.ATRPageNr2 = New System.Windows.Forms.Label()
        Me.Label22 = New System.Windows.Forms.Label()
        Me.SaveATRResCostButton = New System.Windows.Forms.Button()
        Me.FixedCostsGrid = New System.Windows.Forms.DataGridView()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.ATRResourcesGrid = New System.Windows.Forms.DataGridView()
        Me.Label28 = New System.Windows.Forms.Label()
        Me.TabPage2 = New System.Windows.Forms.TabPage()
        Me.UnderlineTextResult = New System.Windows.Forms.Button()
        Me.BoldTextResult = New System.Windows.Forms.Button()
        Me.ResultText = New System.Windows.Forms.RichTextBox()
        Me.ATRPageNr1 = New System.Windows.Forms.Label()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.SaveATRButton = New System.Windows.Forms.Button()
        Me.ATRInputGrid = New System.Windows.Forms.DataGridView()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.ATRRisksGrid = New System.Windows.Forms.DataGridView()
        Me.ATRDeliveriesGrid = New System.Windows.Forms.DataGridView()
        Me.Label21 = New System.Windows.Forms.Label()
        Me.Label18 = New System.Windows.Forms.Label()
        Me.Label17 = New System.Windows.Forms.Label()
        Me.TabPage4 = New System.Windows.Forms.TabPage()
        Me.UnderLineTextScope = New System.Windows.Forms.Button()
        Me.BoldTextScope = New System.Windows.Forms.Button()
        Me.ATRnumber = New System.Windows.Forms.ComboBox()
        Me.TRVNameBox = New System.Windows.Forms.TextBox()
        Me.GenATRnumber = New System.Windows.Forms.TextBox()
        Me.ATR_FK = New System.Windows.Forms.TextBox()
        Me.ATRLink = New System.Windows.Forms.TextBox()
        Me.ATRVersion = New System.Windows.Forms.TextBox()
        Me.ATRRevSign = New System.Windows.Forms.TextBox()
        Me.ATRScope = New System.Windows.Forms.RichTextBox()
        Me.ATRName = New System.Windows.Forms.TextBox()
        Me.ATRLeader = New System.Windows.Forms.TextBox()
        Me.Label41 = New System.Windows.Forms.Label()
        Me.DeleteATR = New System.Windows.Forms.Button()
        Me.WBSLinkLabel = New System.Windows.Forms.LinkLabel()
        Me.CopyATRButton = New System.Windows.Forms.Button()
        Me.Label35 = New System.Windows.Forms.Label()
        Me.StageGroupe = New System.Windows.Forms.GroupBox()
        Me.MakeStagelabel = New System.Windows.Forms.Label()
        Me.StageComboBox = New System.Windows.Forms.ComboBox()
        Me.Stage_DeleteButton = New System.Windows.Forms.Button()
        Me.Stage_NewButton = New System.Windows.Forms.Button()
        Me.Stage_UpdateButton = New System.Windows.Forms.Button()
        Me.StageListBox = New System.Windows.Forms.ListBox()
        Me.StageFK = New System.Windows.Forms.TextBox()
        Me.Label32 = New System.Windows.Forms.Label()
        Me.Label33 = New System.Windows.Forms.Label()
        Me.StageLeader = New System.Windows.Forms.TextBox()
        Me.HelpLinklabel = New System.Windows.Forms.LinkLabel()
        Me.ATR_UpdateButton = New System.Windows.Forms.Button()
        Me.ATR_NewButton = New System.Windows.Forms.Button()
        Me.ATRrevDate = New System.Windows.Forms.DateTimePicker()
        Me.EndDate = New System.Windows.Forms.DateTimePicker()
        Me.StartDate = New System.Windows.Forms.DateTimePicker()
        Me.Label38 = New System.Windows.Forms.Label()
        Me.Label36 = New System.Windows.Forms.Label()
        Me.Label37 = New System.Windows.Forms.Label()
        Me.Label27 = New System.Windows.Forms.Label()
        Me.Label25 = New System.Windows.Forms.Label()
        Me.Label23 = New System.Windows.Forms.Label()
        Me.Label20 = New System.Windows.Forms.Label()
        Me.Label19 = New System.Windows.Forms.Label()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.TabPage1 = New System.Windows.Forms.TabPage()
        Me.TRVNameCheckBox = New System.Windows.Forms.GroupBox()
        Me.ProjectNumber = New System.Windows.Forms.ComboBox()
        Me.FileNamcheckbox = New System.Windows.Forms.CheckBox()
        Me.Project_DeleteButton = New System.Windows.Forms.Button()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Project_NewButton = New System.Windows.Forms.Button()
        Me.ProjectName = New System.Windows.Forms.TextBox()
        Me.Project_UpdateButton = New System.Windows.Forms.Button()
        Me.ProjectOwner = New System.Windows.Forms.TextBox()
        Me.Projectleader = New System.Windows.Forms.TextBox()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.CustomerNumber = New System.Windows.Forms.TextBox()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.SubprojectGroupe = New System.Windows.Forms.GroupBox()
        Me.SubprojectNumber = New System.Windows.Forms.ComboBox()
        Me.Sub_DeleteButton = New System.Windows.Forms.Button()
        Me.Label29 = New System.Windows.Forms.Label()
        Me.Sub_NewButton = New System.Windows.Forms.Button()
        Me.Label31 = New System.Windows.Forms.Label()
        Me.Label30 = New System.Windows.Forms.Label()
        Me.Sub_UpdateButton = New System.Windows.Forms.Button()
        Me.SubProjectName = New System.Windows.Forms.TextBox()
        Me.SubProjectLeader = New System.Windows.Forms.TextBox()
        Me.SubprojectFK = New System.Windows.Forms.TextBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.tabControl = New System.Windows.Forms.TabControl()
        Me.MenuStrip1.SuspendLayout()
        Me.TabPage6.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        Me.TabPage3.SuspendLayout()
        CType(Me.CatGrid, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ResourceGrid, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabPage5.SuspendLayout()
        Me.ATRSelectGroupeBox.SuspendLayout()
        Me.ATRMemo.SuspendLayout()
        Me.TabPage7.SuspendLayout()
        CType(Me.FixedCostsGrid, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ATRResourcesGrid, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabPage2.SuspendLayout()
        CType(Me.ATRInputGrid, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ATRRisksGrid, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ATRDeliveriesGrid, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabPage4.SuspendLayout()
        Me.StageGroupe.SuspendLayout()
        Me.TabPage1.SuspendLayout()
        Me.TRVNameCheckBox.SuspendLayout()
        Me.SubprojectGroupe.SuspendLayout()
        Me.tabControl.SuspendLayout()
        Me.SuspendLayout()
        '
        'MenuStrip1
        '
        Me.MenuStrip1.ImageScalingSize = New System.Drawing.Size(20, 20)
        Me.MenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ArkivToolStripMenuItem, Me.WBSIIDAToolStripMenuItem, Me.LathundIIDAToolStripMenuItem})
        Me.MenuStrip1.Location = New System.Drawing.Point(0, 0)
        Me.MenuStrip1.Name = "MenuStrip1"
        Me.MenuStrip1.Padding = New System.Windows.Forms.Padding(7, 2, 0, 2)
        Me.MenuStrip1.Size = New System.Drawing.Size(1452, 28)
        Me.MenuStrip1.TabIndex = 3
        Me.MenuStrip1.Text = "MenuStrip1"
        '
        'ArkivToolStripMenuItem
        '
        Me.ArkivToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.NewProjectToolStripMenuItem, Me.OpenToolStripMenuItem, Me.ImportExcelToolStripMenuItem})
        Me.ArkivToolStripMenuItem.Name = "ArkivToolStripMenuItem"
        Me.ArkivToolStripMenuItem.Size = New System.Drawing.Size(54, 24)
        Me.ArkivToolStripMenuItem.Text = "Arkiv"
        '
        'NewProjectToolStripMenuItem
        '
        Me.NewProjectToolStripMenuItem.Name = "NewProjectToolStripMenuItem"
        Me.NewProjectToolStripMenuItem.Size = New System.Drawing.Size(188, 26)
        Me.NewProjectToolStripMenuItem.Text = "Nytt projekt"
        '
        'OpenToolStripMenuItem
        '
        Me.OpenToolStripMenuItem.Name = "OpenToolStripMenuItem"
        Me.OpenToolStripMenuItem.Size = New System.Drawing.Size(188, 26)
        Me.OpenToolStripMenuItem.Text = "Öppna projekt"
        '
        'ImportExcelToolStripMenuItem
        '
        Me.ImportExcelToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ImporteraWBSToolStripMenuItem})
        Me.ImportExcelToolStripMenuItem.Enabled = False
        Me.ImportExcelToolStripMenuItem.Name = "ImportExcelToolStripMenuItem"
        Me.ImportExcelToolStripMenuItem.Size = New System.Drawing.Size(188, 26)
        Me.ImportExcelToolStripMenuItem.Text = "Importera Excel"
        '
        'ImporteraWBSToolStripMenuItem
        '
        Me.ImporteraWBSToolStripMenuItem.Name = "ImporteraWBSToolStripMenuItem"
        Me.ImporteraWBSToolStripMenuItem.Size = New System.Drawing.Size(185, 26)
        Me.ImporteraWBSToolStripMenuItem.Text = "Importera WBS"
        '
        'WBSIIDAToolStripMenuItem
        '
        Me.WBSIIDAToolStripMenuItem.Name = "WBSIIDAToolStripMenuItem"
        Me.WBSIIDAToolStripMenuItem.Size = New System.Drawing.Size(89, 24)
        Me.WBSIIDAToolStripMenuItem.Text = "WBS i IDA"
        '
        'LathundIIDAToolStripMenuItem
        '
        Me.LathundIIDAToolStripMenuItem.Name = "LathundIIDAToolStripMenuItem"
        Me.LathundIIDAToolStripMenuItem.Size = New System.Drawing.Size(111, 24)
        Me.LathundIIDAToolStripMenuItem.Text = "Lathund i IDA"
        '
        'OpenFileDialog1
        '
        Me.OpenFileDialog1.Filter = """Database files (*.accdb)|*.accdb| All files |*.*"""
        '
        'OpenWordTempltateFileDialog
        '
        Me.OpenWordTempltateFileDialog.FileName = "OpenFileDialog2"
        '
        'WBSFileDialog
        '
        Me.WBSFileDialog.Filter = """Excel files (*.xlsx)|*.xlsx| All files |*.*"""
        '
        'TabPage6
        '
        Me.TabPage6.Controls.Add(Me.GroupBox2)
        Me.TabPage6.Controls.Add(Me.TextBox2)
        Me.TabPage6.Controls.Add(Me.Label34)
        Me.TabPage6.Controls.Add(Me.GroupBox1)
        Me.TabPage6.Controls.Add(Me.ListSelectProjectRButton)
        Me.TabPage6.Controls.Add(Me.TextBox1)
        Me.TabPage6.Controls.Add(Me.ListGenSubProjectNumber)
        Me.TabPage6.Controls.Add(Me.ListSelectStageRButton)
        Me.TabPage6.Controls.Add(Me.ListSelectSubProjectRButton)
        Me.TabPage6.Controls.Add(Me.CreateList)
        Me.TabPage6.Controls.Add(Me.ListSelectBox)
        Me.TabPage6.Controls.Add(Me.Label24)
        Me.TabPage6.Location = New System.Drawing.Point(4, 25)
        Me.TabPage6.Name = "TabPage6"
        Me.TabPage6.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage6.Size = New System.Drawing.Size(1444, 822)
        Me.TabPage6.TabIndex = 5
        Me.TabPage6.Text = "Listor"
        Me.TabPage6.UseVisualStyleBackColor = True
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.DateSort)
        Me.GroupBox2.Controls.Add(Me.Label44)
        Me.GroupBox2.Controls.Add(Me.Label43)
        Me.GroupBox2.Controls.Add(Me.SDate)
        Me.GroupBox2.Controls.Add(Me.EDate)
        Me.GroupBox2.Location = New System.Drawing.Point(441, 168)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(255, 197)
        Me.GroupBox2.TabIndex = 13
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Sortering för timmar/Resurslista"
        '
        'DateSort
        '
        Me.DateSort.AutoSize = True
        Me.DateSort.Location = New System.Drawing.Point(9, 37)
        Me.DateSort.Name = "DateSort"
        Me.DateSort.Size = New System.Drawing.Size(148, 22)
        Me.DateSort.TabIndex = 15
        Me.DateSort.Text = "Sortera på datum "
        Me.DateSort.UseVisualStyleBackColor = True
        '
        'Label44
        '
        Me.Label44.AutoSize = True
        Me.Label44.Location = New System.Drawing.Point(6, 134)
        Me.Label44.Name = "Label44"
        Me.Label44.Size = New System.Drawing.Size(78, 18)
        Me.Label44.TabIndex = 14
        Me.Label44.Text = "Slut datum"
        '
        'Label43
        '
        Me.Label43.AutoSize = True
        Me.Label43.Location = New System.Drawing.Point(6, 79)
        Me.Label43.Name = "Label43"
        Me.Label43.Size = New System.Drawing.Size(84, 18)
        Me.Label43.TabIndex = 13
        Me.Label43.Text = "Start datum"
        '
        'SDate
        '
        Me.SDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.SDate.Location = New System.Drawing.Point(6, 99)
        Me.SDate.Name = "SDate"
        Me.SDate.Size = New System.Drawing.Size(200, 24)
        Me.SDate.TabIndex = 11
        '
        'EDate
        '
        Me.EDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.EDate.Location = New System.Drawing.Point(6, 155)
        Me.EDate.Name = "EDate"
        Me.EDate.Size = New System.Drawing.Size(200, 24)
        Me.EDate.TabIndex = 12
        '
        'TextBox2
        '
        Me.TextBox2.Location = New System.Drawing.Point(0, 0)
        Me.TextBox2.Name = "TextBox2"
        Me.TextBox2.Size = New System.Drawing.Size(100, 24)
        Me.TextBox2.TabIndex = 10
        '
        'Label34
        '
        Me.Label34.AutoSize = True
        Me.Label34.Location = New System.Drawing.Point(0, 0)
        Me.Label34.Name = "Label34"
        Me.Label34.Size = New System.Drawing.Size(59, 18)
        Me.Label34.TabIndex = 9
        Me.Label34.Text = "Label34"
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.SearchButton)
        Me.GroupBox1.Controls.Add(Me.ATRRisksCheckBox)
        Me.GroupBox1.Controls.Add(Me.ATROutPutCheckBox)
        Me.GroupBox1.Controls.Add(Me.ATRResultCheckBox)
        Me.GroupBox1.Controls.Add(Me.ATRInputsCheckbox)
        Me.GroupBox1.Controls.Add(Me.ATRScopeCheckbox)
        Me.GroupBox1.Controls.Add(Me.Label42)
        Me.GroupBox1.Controls.Add(Me.SearchText)
        Me.GroupBox1.Controls.Add(Me.Label40)
        Me.GroupBox1.Location = New System.Drawing.Point(39, 371)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(606, 297)
        Me.GroupBox1.TabIndex = 8
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Sök ATR Blad"
        Me.GroupBox1.Visible = False
        '
        'SearchButton
        '
        Me.SearchButton.Location = New System.Drawing.Point(369, 218)
        Me.SearchButton.Name = "SearchButton"
        Me.SearchButton.Size = New System.Drawing.Size(227, 34)
        Me.SearchButton.TabIndex = 9
        Me.SearchButton.Text = "Sök"
        Me.SearchButton.UseVisualStyleBackColor = True
        '
        'ATRRisksCheckBox
        '
        Me.ATRRisksCheckBox.AutoSize = True
        Me.ATRRisksCheckBox.Location = New System.Drawing.Point(10, 231)
        Me.ATRRisksCheckBox.Name = "ATRRisksCheckBox"
        Me.ATRRisksCheckBox.Size = New System.Drawing.Size(106, 22)
        Me.ATRRisksCheckBox.TabIndex = 7
        Me.ATRRisksCheckBox.Text = "ATR Risker"
        Me.ATRRisksCheckBox.UseVisualStyleBackColor = True
        '
        'ATROutPutCheckBox
        '
        Me.ATROutPutCheckBox.AutoSize = True
        Me.ATROutPutCheckBox.Location = New System.Drawing.Point(10, 203)
        Me.ATROutPutCheckBox.Name = "ATROutPutCheckBox"
        Me.ATROutPutCheckBox.Size = New System.Drawing.Size(131, 22)
        Me.ATROutPutCheckBox.TabIndex = 6
        Me.ATROutPutCheckBox.Text = "ATR leveranser"
        Me.ATROutPutCheckBox.UseVisualStyleBackColor = True
        '
        'ATRResultCheckBox
        '
        Me.ATRResultCheckBox.AutoSize = True
        Me.ATRResultCheckBox.Location = New System.Drawing.Point(10, 175)
        Me.ATRResultCheckBox.Name = "ATRResultCheckBox"
        Me.ATRResultCheckBox.Size = New System.Drawing.Size(117, 22)
        Me.ATRResultCheckBox.TabIndex = 5
        Me.ATRResultCheckBox.Text = "ATR Resultat"
        Me.ATRResultCheckBox.UseVisualStyleBackColor = True
        '
        'ATRInputsCheckbox
        '
        Me.ATRInputsCheckbox.AutoSize = True
        Me.ATRInputsCheckbox.Location = New System.Drawing.Point(10, 147)
        Me.ATRInputsCheckbox.Name = "ATRInputsCheckbox"
        Me.ATRInputsCheckbox.Size = New System.Drawing.Size(162, 22)
        Me.ATRInputsCheckbox.TabIndex = 4
        Me.ATRInputsCheckbox.Text = "ATR Förutsättningar"
        Me.ATRInputsCheckbox.UseVisualStyleBackColor = True
        '
        'ATRScopeCheckbox
        '
        Me.ATRScopeCheckbox.AutoSize = True
        Me.ATRScopeCheckbox.Location = New System.Drawing.Point(10, 119)
        Me.ATRScopeCheckbox.Name = "ATRScopeCheckbox"
        Me.ATRScopeCheckbox.Size = New System.Drawing.Size(132, 22)
        Me.ATRScopeCheckbox.TabIndex = 3
        Me.ATRScopeCheckbox.Text = "ATR omfattning"
        Me.ATRScopeCheckbox.UseVisualStyleBackColor = True
        '
        'Label42
        '
        Me.Label42.AutoSize = True
        Me.Label42.Location = New System.Drawing.Point(7, 84)
        Me.Label42.Name = "Label42"
        Me.Label42.Size = New System.Drawing.Size(90, 18)
        Me.Label42.TabIndex = 2
        Me.Label42.Text = "Sök tabeller:"
        '
        'SearchText
        '
        Me.SearchText.Location = New System.Drawing.Point(10, 57)
        Me.SearchText.Name = "SearchText"
        Me.SearchText.Size = New System.Drawing.Size(590, 24)
        Me.SearchText.TabIndex = 1
        '
        'Label40
        '
        Me.Label40.AutoSize = True
        Me.Label40.Location = New System.Drawing.Point(7, 24)
        Me.Label40.Name = "Label40"
        Me.Label40.Size = New System.Drawing.Size(39, 18)
        Me.Label40.TabIndex = 0
        Me.Label40.Text = "Sök:"
        '
        'ListSelectProjectRButton
        '
        Me.ListSelectProjectRButton.AutoSize = True
        Me.ListSelectProjectRButton.Enabled = False
        Me.ListSelectProjectRButton.Location = New System.Drawing.Point(441, 60)
        Me.ListSelectProjectRButton.Name = "ListSelectProjectRButton"
        Me.ListSelectProjectRButton.Size = New System.Drawing.Size(161, 22)
        Me.ListSelectProjectRButton.TabIndex = 7
        Me.ListSelectProjectRButton.TabStop = True
        Me.ListSelectProjectRButton.Text = "På Resultatenheten "
        Me.ListSelectProjectRButton.UseVisualStyleBackColor = True
        '
        'TextBox1
        '
        Me.TextBox1.Enabled = False
        Me.TextBox1.Location = New System.Drawing.Point(664, 138)
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.Size = New System.Drawing.Size(135, 24)
        Me.TextBox1.TabIndex = 6
        '
        'ListGenSubProjectNumber
        '
        Me.ListGenSubProjectNumber.Enabled = False
        Me.ListGenSubProjectNumber.Location = New System.Drawing.Point(664, 97)
        Me.ListGenSubProjectNumber.Name = "ListGenSubProjectNumber"
        Me.ListGenSubProjectNumber.Size = New System.Drawing.Size(135, 24)
        Me.ListGenSubProjectNumber.TabIndex = 4
        '
        'ListSelectStageRButton
        '
        Me.ListSelectStageRButton.AutoSize = True
        Me.ListSelectStageRButton.Enabled = False
        Me.ListSelectStageRButton.Location = New System.Drawing.Point(441, 140)
        Me.ListSelectStageRButton.Name = "ListSelectStageRButton"
        Me.ListSelectStageRButton.Size = New System.Drawing.Size(194, 22)
        Me.ListSelectStageRButton.TabIndex = 5
        Me.ListSelectStageRButton.TabStop = True
        Me.ListSelectStageRButton.Text = "På deluppdragsnummer: "
        Me.ListSelectStageRButton.UseVisualStyleBackColor = True
        '
        'ListSelectSubProjectRButton
        '
        Me.ListSelectSubProjectRButton.AutoSize = True
        Me.ListSelectSubProjectRButton.Enabled = False
        Me.ListSelectSubProjectRButton.Location = New System.Drawing.Point(441, 99)
        Me.ListSelectSubProjectRButton.Name = "ListSelectSubProjectRButton"
        Me.ListSelectSubProjectRButton.Size = New System.Drawing.Size(217, 22)
        Me.ListSelectSubProjectRButton.TabIndex = 3
        Me.ListSelectSubProjectRButton.TabStop = True
        Me.ListSelectSubProjectRButton.Text = "På Huvuduppdragsnummer: "
        Me.ListSelectSubProjectRButton.UseVisualStyleBackColor = True
        '
        'CreateList
        '
        Me.CreateList.Location = New System.Drawing.Point(39, 276)
        Me.CreateList.Name = "CreateList"
        Me.CreateList.Size = New System.Drawing.Size(96, 32)
        Me.CreateList.TabIndex = 2
        Me.CreateList.Text = "Skapa lista"
        Me.CreateList.UseVisualStyleBackColor = True
        '
        'ListSelectBox
        '
        Me.ListSelectBox.FormattingEnabled = True
        Me.ListSelectBox.ItemHeight = 18
        Me.ListSelectBox.Items.AddRange(New Object() {"Timmar/Resurslista", "ATRLista leverans", "ATRLista för IT", "Timmar/Kategori/Delprojekt", "Timmar/Kategori/ATR", "ATRLista med resultaten", "Resurslista", "Risklista", "Primavera Baslista", "Primavera Inputslista", "Primavera leveranserLista"})
        Me.ListSelectBox.Location = New System.Drawing.Point(39, 88)
        Me.ListSelectBox.Name = "ListSelectBox"
        Me.ListSelectBox.Size = New System.Drawing.Size(244, 184)
        Me.ListSelectBox.TabIndex = 1
        '
        'Label24
        '
        Me.Label24.AutoSize = True
        Me.Label24.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label24.Location = New System.Drawing.Point(35, 42)
        Me.Label24.Name = "Label24"
        Me.Label24.Size = New System.Drawing.Size(248, 24)
        Me.Label24.TabIndex = 0
        Me.Label24.Text = "Skapa någon av följane listor"
        '
        'TabPage3
        '
        Me.TabPage3.Controls.Add(Me.ChangeCatOnResourseButton)
        Me.TabPage3.Controls.Add(Me.UpdateResourceButton)
        Me.TabPage3.Controls.Add(Me.CatGrid)
        Me.TabPage3.Controls.Add(Me.ResourceGrid)
        Me.TabPage3.Location = New System.Drawing.Point(4, 25)
        Me.TabPage3.Margin = New System.Windows.Forms.Padding(4)
        Me.TabPage3.Name = "TabPage3"
        Me.TabPage3.Padding = New System.Windows.Forms.Padding(4)
        Me.TabPage3.Size = New System.Drawing.Size(1444, 822)
        Me.TabPage3.TabIndex = 2
        Me.TabPage3.Text = "Resurser"
        Me.TabPage3.UseVisualStyleBackColor = True
        '
        'ChangeCatOnResourseButton
        '
        Me.ChangeCatOnResourseButton.Enabled = False
        Me.ChangeCatOnResourseButton.Location = New System.Drawing.Point(909, 626)
        Me.ChangeCatOnResourseButton.Name = "ChangeCatOnResourseButton"
        Me.ChangeCatOnResourseButton.Size = New System.Drawing.Size(248, 72)
        Me.ChangeCatOnResourseButton.TabIndex = 14
        Me.ChangeCatOnResourseButton.Text = "Byt Kategori på markerad resurs"
        Me.ChangeCatOnResourseButton.UseVisualStyleBackColor = True
        '
        'UpdateResourceButton
        '
        Me.UpdateResourceButton.Enabled = False
        Me.UpdateResourceButton.Location = New System.Drawing.Point(94, 624)
        Me.UpdateResourceButton.Name = "UpdateResourceButton"
        Me.UpdateResourceButton.Size = New System.Drawing.Size(801, 72)
        Me.UpdateResourceButton.TabIndex = 12
        Me.UpdateResourceButton.Text = "Spara/Uppdatera alla tabeller"
        Me.UpdateResourceButton.UseVisualStyleBackColor = True
        '
        'CatGrid
        '
        Me.CatGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.CatGrid.Location = New System.Drawing.Point(11, 8)
        Me.CatGrid.Margin = New System.Windows.Forms.Padding(4)
        Me.CatGrid.Name = "CatGrid"
        Me.CatGrid.RowTemplate.Height = 24
        Me.CatGrid.Size = New System.Drawing.Size(540, 590)
        Me.CatGrid.TabIndex = 2
        '
        'ResourceGrid
        '
        Me.ResourceGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.ResourceGrid.Location = New System.Drawing.Point(601, 8)
        Me.ResourceGrid.Margin = New System.Windows.Forms.Padding(4)
        Me.ResourceGrid.Name = "ResourceGrid"
        Me.ResourceGrid.RowTemplate.Height = 24
        Me.ResourceGrid.Size = New System.Drawing.Size(655, 590)
        Me.ResourceGrid.TabIndex = 0
        '
        'TabPage5
        '
        Me.TabPage5.Controls.Add(Me.ATRInWordFormat)
        Me.TabPage5.Controls.Add(Me.OpenPDFCatalog)
        Me.TabPage5.Controls.Add(Me.ATRPrintRevSignBox)
        Me.TabPage5.Controls.Add(Me.ATRPrintVersionBox)
        Me.TabPage5.Controls.Add(Me.ATRRevSignRadioButton)
        Me.TabPage5.Controls.Add(Me.ATRVersionRadioButton)
        Me.TabPage5.Controls.Add(Me.ATRDateRadioButton)
        Me.TabPage5.Controls.Add(Me.ATRSelectGroupeBox)
        Me.TabPage5.Controls.Add(Me.WordFolderLabel)
        Me.TabPage5.Controls.Add(Me.WordFilepath)
        Me.TabPage5.Controls.Add(Me.Label26)
        Me.TabPage5.Controls.Add(Me.ATRRadioButton)
        Me.TabPage5.Controls.Add(Me.StageATRListRadioButton)
        Me.TabPage5.Controls.Add(Me.SubprojectATRListRadioButton)
        Me.TabPage5.Controls.Add(Me.FullATRListRadioButton)
        Me.TabPage5.Controls.Add(Me.CreateATR)
        Me.TabPage5.Location = New System.Drawing.Point(4, 25)
        Me.TabPage5.Margin = New System.Windows.Forms.Padding(4)
        Me.TabPage5.Name = "TabPage5"
        Me.TabPage5.Padding = New System.Windows.Forms.Padding(4)
        Me.TabPage5.Size = New System.Drawing.Size(1444, 822)
        Me.TabPage5.TabIndex = 4
        Me.TabPage5.Text = "Skapa PDF-filer"
        Me.TabPage5.UseVisualStyleBackColor = True
        '
        'ATRInWordFormat
        '
        Me.ATRInWordFormat.AutoSize = True
        Me.ATRInWordFormat.Enabled = False
        Me.ATRInWordFormat.Location = New System.Drawing.Point(56, 545)
        Me.ATRInWordFormat.Name = "ATRInWordFormat"
        Me.ATRInWordFormat.Size = New System.Drawing.Size(223, 22)
        Me.ATRInWordFormat.TabIndex = 19
        Me.ATRInWordFormat.Text = "Skapa ATRblad i Wordformat"
        Me.ATRInWordFormat.UseVisualStyleBackColor = True
        '
        'OpenPDFCatalog
        '
        Me.OpenPDFCatalog.Location = New System.Drawing.Point(405, 582)
        Me.OpenPDFCatalog.Name = "OpenPDFCatalog"
        Me.OpenPDFCatalog.Size = New System.Drawing.Size(209, 66)
        Me.OpenPDFCatalog.TabIndex = 18
        Me.OpenPDFCatalog.Text = "Öppna Katalog "
        Me.OpenPDFCatalog.UseVisualStyleBackColor = True
        '
        'ATRPrintRevSignBox
        '
        Me.ATRPrintRevSignBox.Enabled = False
        Me.ATRPrintRevSignBox.Location = New System.Drawing.Point(391, 312)
        Me.ATRPrintRevSignBox.Name = "ATRPrintRevSignBox"
        Me.ATRPrintRevSignBox.Size = New System.Drawing.Size(78, 24)
        Me.ATRPrintRevSignBox.TabIndex = 17
        '
        'ATRPrintVersionBox
        '
        Me.ATRPrintVersionBox.Enabled = False
        Me.ATRPrintVersionBox.Location = New System.Drawing.Point(391, 264)
        Me.ATRPrintVersionBox.Name = "ATRPrintVersionBox"
        Me.ATRPrintVersionBox.Size = New System.Drawing.Size(78, 24)
        Me.ATRPrintVersionBox.TabIndex = 15
        '
        'ATRRevSignRadioButton
        '
        Me.ATRRevSignRadioButton.AutoSize = True
        Me.ATRRevSignRadioButton.Location = New System.Drawing.Point(56, 312)
        Me.ATRRevSignRadioButton.Name = "ATRRevSignRadioButton"
        Me.ATRRevSignRadioButton.Size = New System.Drawing.Size(248, 22)
        Me.ATRRevSignRadioButton.TabIndex = 16
        Me.ATRRevSignRadioButton.Text = "ATR-blad ändrade av (SwecoID): "
        Me.ATRRevSignRadioButton.UseVisualStyleBackColor = True
        '
        'ATRVersionRadioButton
        '
        Me.ATRVersionRadioButton.AutoSize = True
        Me.ATRVersionRadioButton.Location = New System.Drawing.Point(56, 264)
        Me.ATRVersionRadioButton.Name = "ATRVersionRadioButton"
        Me.ATRVersionRadioButton.Size = New System.Drawing.Size(183, 22)
        Me.ATRVersionRadioButton.TabIndex = 14
        Me.ATRVersionRadioButton.Text = "ATR-blad med version: "
        Me.ATRVersionRadioButton.UseVisualStyleBackColor = True
        '
        'ATRDateRadioButton
        '
        Me.ATRDateRadioButton.AutoSize = True
        Me.ATRDateRadioButton.Location = New System.Drawing.Point(56, 217)
        Me.ATRDateRadioButton.Name = "ATRDateRadioButton"
        Me.ATRDateRadioButton.Size = New System.Drawing.Size(168, 22)
        Me.ATRDateRadioButton.TabIndex = 11
        Me.ATRDateRadioButton.Text = "ATR-blad från datum "
        Me.ATRDateRadioButton.UseVisualStyleBackColor = True
        '
        'ATRSelectGroupeBox
        '
        Me.ATRSelectGroupeBox.Controls.Add(Me.ATRRevDateRadioButton)
        Me.ATRSelectGroupeBox.Controls.Add(Me.ATRPrintDatePicker)
        Me.ATRSelectGroupeBox.Controls.Add(Me.ATREndDateRadioButton)
        Me.ATRSelectGroupeBox.Controls.Add(Me.ATRStartDateRadioButton)
        Me.ATRSelectGroupeBox.Enabled = False
        Me.ATRSelectGroupeBox.Location = New System.Drawing.Point(391, 80)
        Me.ATRSelectGroupeBox.Name = "ATRSelectGroupeBox"
        Me.ATRSelectGroupeBox.Size = New System.Drawing.Size(435, 161)
        Me.ATRSelectGroupeBox.TabIndex = 13
        Me.ATRSelectGroupeBox.TabStop = False
        Me.ATRSelectGroupeBox.Text = "Datum information "
        '
        'ATRRevDateRadioButton
        '
        Me.ATRRevDateRadioButton.AutoSize = True
        Me.ATRRevDateRadioButton.Checked = True
        Me.ATRRevDateRadioButton.Location = New System.Drawing.Point(14, 123)
        Me.ATRRevDateRadioButton.Name = "ATRRevDateRadioButton"
        Me.ATRRevDateRadioButton.Size = New System.Drawing.Size(224, 22)
        Me.ATRRevDateRadioButton.TabIndex = 15
        Me.ATRRevDateRadioButton.TabStop = True
        Me.ATRRevDateRadioButton.Text = "ATR-blad från ändringsdatum "
        Me.ATRRevDateRadioButton.UseVisualStyleBackColor = True
        '
        'ATRPrintDatePicker
        '
        Me.ATRPrintDatePicker.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.ATRPrintDatePicker.Location = New System.Drawing.Point(261, 79)
        Me.ATRPrintDatePicker.Name = "ATRPrintDatePicker"
        Me.ATRPrintDatePicker.Size = New System.Drawing.Size(129, 24)
        Me.ATRPrintDatePicker.TabIndex = 14
        '
        'ATREndDateRadioButton
        '
        Me.ATREndDateRadioButton.AutoSize = True
        Me.ATREndDateRadioButton.Location = New System.Drawing.Point(14, 82)
        Me.ATREndDateRadioButton.Name = "ATREndDateRadioButton"
        Me.ATREndDateRadioButton.Size = New System.Drawing.Size(191, 22)
        Me.ATREndDateRadioButton.TabIndex = 13
        Me.ATREndDateRadioButton.Text = "ATR-blad från slutdatum "
        Me.ATREndDateRadioButton.UseVisualStyleBackColor = True
        '
        'ATRStartDateRadioButton
        '
        Me.ATRStartDateRadioButton.AutoSize = True
        Me.ATRStartDateRadioButton.Location = New System.Drawing.Point(14, 40)
        Me.ATRStartDateRadioButton.Name = "ATRStartDateRadioButton"
        Me.ATRStartDateRadioButton.Size = New System.Drawing.Size(197, 22)
        Me.ATRStartDateRadioButton.TabIndex = 12
        Me.ATRStartDateRadioButton.Text = "ATR-blad från startdatum "
        Me.ATRStartDateRadioButton.UseVisualStyleBackColor = True
        '
        'WordFolderLabel
        '
        Me.WordFolderLabel.AutoSize = True
        Me.WordFolderLabel.Location = New System.Drawing.Point(52, 516)
        Me.WordFolderLabel.Name = "WordFolderLabel"
        Me.WordFolderLabel.Size = New System.Drawing.Size(41, 18)
        Me.WordFolderLabel.TabIndex = 10
        Me.WordFolderLabel.Text = "CCC"
        '
        'WordFilepath
        '
        Me.WordFilepath.Location = New System.Drawing.Point(403, 465)
        Me.WordFilepath.Name = "WordFilepath"
        Me.WordFilepath.Size = New System.Drawing.Size(84, 40)
        Me.WordFilepath.TabIndex = 9
        Me.WordFilepath.Text = "Sök"
        Me.WordFilepath.UseVisualStyleBackColor = True
        Me.WordFilepath.Visible = False
        '
        'Label26
        '
        Me.Label26.AutoSize = True
        Me.Label26.Location = New System.Drawing.Point(52, 475)
        Me.Label26.Name = "Label26"
        Me.Label26.Size = New System.Drawing.Size(210, 18)
        Me.Label26.TabIndex = 8
        Me.Label26.Text = "Sökväg för generade ATR-blad"
        '
        'ATRRadioButton
        '
        Me.ATRRadioButton.AutoSize = True
        Me.ATRRadioButton.Checked = True
        Me.ATRRadioButton.Location = New System.Drawing.Point(56, 54)
        Me.ATRRadioButton.Name = "ATRRadioButton"
        Me.ATRRadioButton.Size = New System.Drawing.Size(129, 22)
        Me.ATRRadioButton.TabIndex = 6
        Me.ATRRadioButton.TabStop = True
        Me.ATRRadioButton.Text = "Aktivt ATR-blad"
        Me.ATRRadioButton.UseVisualStyleBackColor = True
        '
        'StageATRListRadioButton
        '
        Me.StageATRListRadioButton.AutoSize = True
        Me.StageATRListRadioButton.Location = New System.Drawing.Point(56, 174)
        Me.StageATRListRadioButton.Name = "StageATRListRadioButton"
        Me.StageATRListRadioButton.Size = New System.Drawing.Size(251, 22)
        Me.StageATRListRadioButton.TabIndex = 5
        Me.StageATRListRadioButton.Text = "Alla ATR-blad på aktivt deluppdrag"
        Me.StageATRListRadioButton.UseVisualStyleBackColor = True
        '
        'SubprojectATRListRadioButton
        '
        Me.SubprojectATRListRadioButton.AutoSize = True
        Me.SubprojectATRListRadioButton.Location = New System.Drawing.Point(56, 132)
        Me.SubprojectATRListRadioButton.Name = "SubprojectATRListRadioButton"
        Me.SubprojectATRListRadioButton.Size = New System.Drawing.Size(271, 22)
        Me.SubprojectATRListRadioButton.TabIndex = 4
        Me.SubprojectATRListRadioButton.Text = "Alla ATR-blad på aktivt huvuduppdrag"
        Me.SubprojectATRListRadioButton.UseVisualStyleBackColor = True
        '
        'FullATRListRadioButton
        '
        Me.FullATRListRadioButton.AutoSize = True
        Me.FullATRListRadioButton.Location = New System.Drawing.Point(56, 92)
        Me.FullATRListRadioButton.Name = "FullATRListRadioButton"
        Me.FullATRListRadioButton.Size = New System.Drawing.Size(259, 22)
        Me.FullATRListRadioButton.TabIndex = 3
        Me.FullATRListRadioButton.Text = "Alla ATR-blad på aktiv resultatenhet"
        Me.FullATRListRadioButton.UseVisualStyleBackColor = True
        '
        'CreateATR
        '
        Me.CreateATR.Location = New System.Drawing.Point(52, 582)
        Me.CreateATR.Name = "CreateATR"
        Me.CreateATR.Size = New System.Drawing.Size(209, 66)
        Me.CreateATR.TabIndex = 2
        Me.CreateATR.Text = "Skapa ATRblad"
        Me.CreateATR.UseVisualStyleBackColor = True
        '
        'ATRMemo
        '
        Me.ATRMemo.Controls.Add(Me.ATRNrMemo)
        Me.ATRMemo.Controls.Add(Me.Label39)
        Me.ATRMemo.Controls.Add(Me.ATRMemoSave)
        Me.ATRMemo.Controls.Add(Me.MemoText)
        Me.ATRMemo.Location = New System.Drawing.Point(4, 25)
        Me.ATRMemo.Name = "ATRMemo"
        Me.ATRMemo.Size = New System.Drawing.Size(1444, 822)
        Me.ATRMemo.TabIndex = 8
        Me.ATRMemo.Text = "ATR-Minnesanteckningar"
        Me.ATRMemo.UseVisualStyleBackColor = True
        '
        'ATRNrMemo
        '
        Me.ATRNrMemo.AutoSize = True
        Me.ATRNrMemo.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ATRNrMemo.Location = New System.Drawing.Point(538, 16)
        Me.ATRNrMemo.Name = "ATRNrMemo"
        Me.ATRNrMemo.Size = New System.Drawing.Size(20, 24)
        Me.ATRNrMemo.TabIndex = 3
        Me.ATRNrMemo.Text = "1"
        '
        'Label39
        '
        Me.Label39.AutoSize = True
        Me.Label39.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label39.Location = New System.Drawing.Point(8, 16)
        Me.Label39.Name = "Label39"
        Me.Label39.Size = New System.Drawing.Size(533, 24)
        Me.Label39.TabIndex = 2
        Me.Label39.Text = "Minnesanteckningar för ATR-blad (Skrivs inte ut på ATR-blad): "
        '
        'ATRMemoSave
        '
        Me.ATRMemoSave.Location = New System.Drawing.Point(1082, 772)
        Me.ATRMemoSave.Name = "ATRMemoSave"
        Me.ATRMemoSave.Size = New System.Drawing.Size(299, 39)
        Me.ATRMemoSave.TabIndex = 1
        Me.ATRMemoSave.Text = "Spara"
        Me.ATRMemoSave.UseVisualStyleBackColor = True
        '
        'MemoText
        '
        Me.MemoText.Location = New System.Drawing.Point(8, 48)
        Me.MemoText.Name = "MemoText"
        Me.MemoText.Size = New System.Drawing.Size(1373, 704)
        Me.MemoText.TabIndex = 0
        Me.MemoText.Text = ""
        '
        'TabPage7
        '
        Me.TabPage7.Controls.Add(Me.TotalCostATR)
        Me.TabPage7.Controls.Add(Me.Label12)
        Me.TabPage7.Controls.Add(Me.ATRPageNr2)
        Me.TabPage7.Controls.Add(Me.Label22)
        Me.TabPage7.Controls.Add(Me.SaveATRResCostButton)
        Me.TabPage7.Controls.Add(Me.FixedCostsGrid)
        Me.TabPage7.Controls.Add(Me.Label13)
        Me.TabPage7.Controls.Add(Me.ATRResourcesGrid)
        Me.TabPage7.Controls.Add(Me.Label28)
        Me.TabPage7.Location = New System.Drawing.Point(4, 25)
        Me.TabPage7.Name = "TabPage7"
        Me.TabPage7.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage7.Size = New System.Drawing.Size(1444, 822)
        Me.TabPage7.TabIndex = 7
        Me.TabPage7.Text = "ATR-Resurser/Kostnader"
        Me.TabPage7.UseVisualStyleBackColor = True
        '
        'TotalCostATR
        '
        Me.TotalCostATR.AutoSize = True
        Me.TotalCostATR.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TotalCostATR.Location = New System.Drawing.Point(1270, 534)
        Me.TotalCostATR.Name = "TotalCostATR"
        Me.TotalCostATR.Size = New System.Drawing.Size(18, 20)
        Me.TotalCostATR.TabIndex = 57
        Me.TotalCostATR.Text = "1"
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label12.Location = New System.Drawing.Point(1108, 534)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(152, 20)
        Me.Label12.TabIndex = 56
        Me.Label12.Text = "Totalkostnad ATR: "
        '
        'ATRPageNr2
        '
        Me.ATRPageNr2.AutoSize = True
        Me.ATRPageNr2.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!)
        Me.ATRPageNr2.Location = New System.Drawing.Point(573, 26)
        Me.ATRPageNr2.Name = "ATRPageNr2"
        Me.ATRPageNr2.Size = New System.Drawing.Size(68, 20)
        Me.ATRPageNr2.TabIndex = 55
        Me.ATRPageNr2.Text = "Label12"
        '
        'Label22
        '
        Me.Label22.AutoSize = True
        Me.Label22.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!)
        Me.Label22.Location = New System.Drawing.Point(439, 26)
        Me.Label22.Name = "Label22"
        Me.Label22.Size = New System.Drawing.Size(104, 20)
        Me.Label22.TabIndex = 54
        Me.Label22.Text = "ATR-bladnr: "
        '
        'SaveATRResCostButton
        '
        Me.SaveATRResCostButton.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.SaveATRResCostButton.Location = New System.Drawing.Point(1100, 594)
        Me.SaveATRResCostButton.Name = "SaveATRResCostButton"
        Me.SaveATRResCostButton.Size = New System.Drawing.Size(231, 169)
        Me.SaveATRResCostButton.TabIndex = 53
        Me.SaveATRResCostButton.Text = "Spara/ Uppdatera ATR information"
        Me.SaveATRResCostButton.UseVisualStyleBackColor = True
        '
        'FixedCostsGrid
        '
        Me.FixedCostsGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.FixedCostsGrid.Location = New System.Drawing.Point(12, 445)
        Me.FixedCostsGrid.Name = "FixedCostsGrid"
        Me.FixedCostsGrid.RowTemplate.Height = 24
        Me.FixedCostsGrid.Size = New System.Drawing.Size(1045, 321)
        Me.FixedCostsGrid.TabIndex = 50
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label13.Location = New System.Drawing.Point(8, 420)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(129, 20)
        Me.Label13.TabIndex = 52
        Me.Label13.Text = "Fasta kostnader"
        '
        'ATRResourcesGrid
        '
        Me.ATRResourcesGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.ATRResourcesGrid.Location = New System.Drawing.Point(12, 49)
        Me.ATRResourcesGrid.Name = "ATRResourcesGrid"
        Me.ATRResourcesGrid.RowTemplate.Height = 24
        Me.ATRResourcesGrid.Size = New System.Drawing.Size(1090, 319)
        Me.ATRResourcesGrid.TabIndex = 49
        '
        'Label28
        '
        Me.Label28.AutoSize = True
        Me.Label28.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label28.Location = New System.Drawing.Point(8, 26)
        Me.Label28.Name = "Label28"
        Me.Label28.Size = New System.Drawing.Size(83, 20)
        Me.Label28.TabIndex = 51
        Me.Label28.Text = "Resurser:"
        '
        'TabPage2
        '
        Me.TabPage2.Controls.Add(Me.UnderlineTextResult)
        Me.TabPage2.Controls.Add(Me.BoldTextResult)
        Me.TabPage2.Controls.Add(Me.ResultText)
        Me.TabPage2.Controls.Add(Me.ATRPageNr1)
        Me.TabPage2.Controls.Add(Me.Label11)
        Me.TabPage2.Controls.Add(Me.SaveATRButton)
        Me.TabPage2.Controls.Add(Me.ATRInputGrid)
        Me.TabPage2.Controls.Add(Me.Label15)
        Me.TabPage2.Controls.Add(Me.ATRRisksGrid)
        Me.TabPage2.Controls.Add(Me.ATRDeliveriesGrid)
        Me.TabPage2.Controls.Add(Me.Label21)
        Me.TabPage2.Controls.Add(Me.Label18)
        Me.TabPage2.Controls.Add(Me.Label17)
        Me.TabPage2.Location = New System.Drawing.Point(4, 25)
        Me.TabPage2.Name = "TabPage2"
        Me.TabPage2.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage2.Size = New System.Drawing.Size(1444, 822)
        Me.TabPage2.TabIndex = 6
        Me.TabPage2.Text = "ATR-Input/Output/Risker"
        Me.TabPage2.UseVisualStyleBackColor = True
        '
        'UnderlineTextResult
        '
        Me.UnderlineTextResult.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!)
        Me.UnderlineTextResult.Location = New System.Drawing.Point(1191, 248)
        Me.UnderlineTextResult.Name = "UnderlineTextResult"
        Me.UnderlineTextResult.Size = New System.Drawing.Size(34, 33)
        Me.UnderlineTextResult.TabIndex = 85
        Me.UnderlineTextResult.Text = "U"
        Me.UnderlineTextResult.UseVisualStyleBackColor = True
        Me.UnderlineTextResult.Visible = False
        '
        'BoldTextResult
        '
        Me.BoldTextResult.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!)
        Me.BoldTextResult.Location = New System.Drawing.Point(1150, 248)
        Me.BoldTextResult.Name = "BoldTextResult"
        Me.BoldTextResult.Size = New System.Drawing.Size(34, 33)
        Me.BoldTextResult.TabIndex = 84
        Me.BoldTextResult.Text = "F"
        Me.BoldTextResult.UseVisualStyleBackColor = True
        Me.BoldTextResult.Visible = False
        '
        'ResultText
        '
        Me.ResultText.Location = New System.Drawing.Point(24, 248)
        Me.ResultText.MaxLength = 2500
        Me.ResultText.Name = "ResultText"
        Me.ResultText.Size = New System.Drawing.Size(1115, 175)
        Me.ResultText.TabIndex = 42
        Me.ResultText.Text = ""
        '
        'ATRPageNr1
        '
        Me.ATRPageNr1.AutoSize = True
        Me.ATRPageNr1.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!)
        Me.ATRPageNr1.Location = New System.Drawing.Point(566, 19)
        Me.ATRPageNr1.Name = "ATRPageNr1"
        Me.ATRPageNr1.Size = New System.Drawing.Size(68, 20)
        Me.ATRPageNr1.TabIndex = 41
        Me.ATRPageNr1.Text = "Label12"
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!)
        Me.Label11.Location = New System.Drawing.Point(432, 19)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(104, 20)
        Me.Label11.TabIndex = 40
        Me.Label11.Text = "ATR-bladnr: "
        '
        'SaveATRButton
        '
        Me.SaveATRButton.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.SaveATRButton.Location = New System.Drawing.Point(1150, 648)
        Me.SaveATRButton.Name = "SaveATRButton"
        Me.SaveATRButton.Size = New System.Drawing.Size(162, 169)
        Me.SaveATRButton.TabIndex = 39
        Me.SaveATRButton.Text = "Spara/ Uppdatera ATR information"
        Me.SaveATRButton.UseVisualStyleBackColor = True
        '
        'ATRInputGrid
        '
        Me.ATRInputGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.ATRInputGrid.Location = New System.Drawing.Point(23, 48)
        Me.ATRInputGrid.Name = "ATRInputGrid"
        Me.ATRInputGrid.RowTemplate.Height = 24
        Me.ATRInputGrid.Size = New System.Drawing.Size(1116, 165)
        Me.ATRInputGrid.TabIndex = 37
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label15.Location = New System.Drawing.Point(20, 19)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(277, 20)
        Me.Label15.TabIndex = 38
        Me.Label15.Text = "Förutsättningar för aktiviteten, Input:"
        '
        'ATRRisksGrid
        '
        Me.ATRRisksGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.ATRRisksGrid.Location = New System.Drawing.Point(25, 651)
        Me.ATRRisksGrid.Name = "ATRRisksGrid"
        Me.ATRRisksGrid.RowTemplate.Height = 24
        Me.ATRRisksGrid.Size = New System.Drawing.Size(1114, 165)
        Me.ATRRisksGrid.TabIndex = 33
        '
        'ATRDeliveriesGrid
        '
        Me.ATRDeliveriesGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.ATRDeliveriesGrid.Location = New System.Drawing.Point(25, 452)
        Me.ATRDeliveriesGrid.Name = "ATRDeliveriesGrid"
        Me.ATRDeliveriesGrid.RowTemplate.Height = 24
        Me.ATRDeliveriesGrid.Size = New System.Drawing.Size(1114, 165)
        Me.ATRDeliveriesGrid.TabIndex = 32
        '
        'Label21
        '
        Me.Label21.AutoSize = True
        Me.Label21.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!)
        Me.Label21.Location = New System.Drawing.Point(21, 626)
        Me.Label21.Name = "Label21"
        Me.Label21.Size = New System.Drawing.Size(181, 20)
        Me.Label21.TabIndex = 36
        Me.Label21.Text = "Riskminimering/åtgärd:"
        '
        'Label18
        '
        Me.Label18.AutoSize = True
        Me.Label18.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label18.Location = New System.Drawing.Point(21, 425)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(304, 20)
        Me.Label18.TabIndex = 35
        Me.Label18.Text = "Interna och extrena leveranser,  Output:"
        '
        'Label17
        '
        Me.Label17.AutoSize = True
        Me.Label17.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label17.Location = New System.Drawing.Point(20, 225)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(76, 20)
        Me.Label17.TabIndex = 34
        Me.Label17.Text = "Resultat:"
        '
        'TabPage4
        '
        Me.TabPage4.Controls.Add(Me.UnderLineTextScope)
        Me.TabPage4.Controls.Add(Me.BoldTextScope)
        Me.TabPage4.Controls.Add(Me.ATRnumber)
        Me.TabPage4.Controls.Add(Me.TRVNameBox)
        Me.TabPage4.Controls.Add(Me.GenATRnumber)
        Me.TabPage4.Controls.Add(Me.ATR_FK)
        Me.TabPage4.Controls.Add(Me.ATRLink)
        Me.TabPage4.Controls.Add(Me.ATRVersion)
        Me.TabPage4.Controls.Add(Me.ATRRevSign)
        Me.TabPage4.Controls.Add(Me.ATRScope)
        Me.TabPage4.Controls.Add(Me.ATRName)
        Me.TabPage4.Controls.Add(Me.ATRLeader)
        Me.TabPage4.Controls.Add(Me.Label41)
        Me.TabPage4.Controls.Add(Me.DeleteATR)
        Me.TabPage4.Controls.Add(Me.WBSLinkLabel)
        Me.TabPage4.Controls.Add(Me.CopyATRButton)
        Me.TabPage4.Controls.Add(Me.Label35)
        Me.TabPage4.Controls.Add(Me.StageGroupe)
        Me.TabPage4.Controls.Add(Me.HelpLinklabel)
        Me.TabPage4.Controls.Add(Me.ATR_UpdateButton)
        Me.TabPage4.Controls.Add(Me.ATR_NewButton)
        Me.TabPage4.Controls.Add(Me.ATRrevDate)
        Me.TabPage4.Controls.Add(Me.EndDate)
        Me.TabPage4.Controls.Add(Me.StartDate)
        Me.TabPage4.Controls.Add(Me.Label38)
        Me.TabPage4.Controls.Add(Me.Label36)
        Me.TabPage4.Controls.Add(Me.Label37)
        Me.TabPage4.Controls.Add(Me.Label27)
        Me.TabPage4.Controls.Add(Me.Label25)
        Me.TabPage4.Controls.Add(Me.Label23)
        Me.TabPage4.Controls.Add(Me.Label20)
        Me.TabPage4.Controls.Add(Me.Label19)
        Me.TabPage4.Controls.Add(Me.Label16)
        Me.TabPage4.Controls.Add(Me.Label14)
        Me.TabPage4.Location = New System.Drawing.Point(4, 25)
        Me.TabPage4.Margin = New System.Windows.Forms.Padding(4)
        Me.TabPage4.Name = "TabPage4"
        Me.TabPage4.Padding = New System.Windows.Forms.Padding(4)
        Me.TabPage4.Size = New System.Drawing.Size(1444, 822)
        Me.TabPage4.TabIndex = 3
        Me.TabPage4.Text = "ATR-information"
        Me.TabPage4.UseVisualStyleBackColor = True
        '
        'UnderLineTextScope
        '
        Me.UnderLineTextScope.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!)
        Me.UnderLineTextScope.Location = New System.Drawing.Point(866, 228)
        Me.UnderLineTextScope.Name = "UnderLineTextScope"
        Me.UnderLineTextScope.Size = New System.Drawing.Size(34, 33)
        Me.UnderLineTextScope.TabIndex = 83
        Me.UnderLineTextScope.Text = "U"
        Me.UnderLineTextScope.UseVisualStyleBackColor = True
        Me.UnderLineTextScope.Visible = False
        '
        'BoldTextScope
        '
        Me.BoldTextScope.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!)
        Me.BoldTextScope.Location = New System.Drawing.Point(825, 228)
        Me.BoldTextScope.Name = "BoldTextScope"
        Me.BoldTextScope.Size = New System.Drawing.Size(34, 33)
        Me.BoldTextScope.TabIndex = 82
        Me.BoldTextScope.Text = "F"
        Me.BoldTextScope.UseVisualStyleBackColor = True
        Me.BoldTextScope.Visible = False
        '
        'ATRnumber
        '
        Me.ATRnumber.FormattingEnabled = True
        Me.ATRnumber.Location = New System.Drawing.Point(510, 89)
        Me.ATRnumber.MaxLength = 10
        Me.ATRnumber.Name = "ATRnumber"
        Me.ATRnumber.Size = New System.Drawing.Size(165, 26)
        Me.ATRnumber.TabIndex = 81
        '
        'TRVNameBox
        '
        Me.TRVNameBox.Location = New System.Drawing.Point(963, 91)
        Me.TRVNameBox.Margin = New System.Windows.Forms.Padding(4)
        Me.TRVNameBox.MaxLength = 12
        Me.TRVNameBox.Name = "TRVNameBox"
        Me.TRVNameBox.ReadOnly = True
        Me.TRVNameBox.Size = New System.Drawing.Size(130, 24)
        Me.TRVNameBox.TabIndex = 79
        Me.TRVNameBox.Visible = False
        '
        'GenATRnumber
        '
        Me.GenATRnumber.Location = New System.Drawing.Point(699, 95)
        Me.GenATRnumber.Margin = New System.Windows.Forms.Padding(4)
        Me.GenATRnumber.Name = "GenATRnumber"
        Me.GenATRnumber.ReadOnly = True
        Me.GenATRnumber.Size = New System.Drawing.Size(130, 24)
        Me.GenATRnumber.TabIndex = 75
        '
        'ATR_FK
        '
        Me.ATR_FK.Location = New System.Drawing.Point(18, 755)
        Me.ATR_FK.Margin = New System.Windows.Forms.Padding(4)
        Me.ATR_FK.Name = "ATR_FK"
        Me.ATR_FK.Size = New System.Drawing.Size(148, 24)
        Me.ATR_FK.TabIndex = 60
        '
        'ATRLink
        '
        Me.ATRLink.Location = New System.Drawing.Point(510, 39)
        Me.ATRLink.Margin = New System.Windows.Forms.Padding(4)
        Me.ATRLink.Name = "ATRLink"
        Me.ATRLink.ReadOnly = True
        Me.ATRLink.Size = New System.Drawing.Size(534, 24)
        Me.ATRLink.TabIndex = 59
        '
        'ATRVersion
        '
        Me.ATRVersion.Location = New System.Drawing.Point(737, 718)
        Me.ATRVersion.Margin = New System.Windows.Forms.Padding(4)
        Me.ATRVersion.MaxLength = 10
        Me.ATRVersion.Name = "ATRVersion"
        Me.ATRVersion.Size = New System.Drawing.Size(80, 24)
        Me.ATRVersion.TabIndex = 13
        Me.ATRVersion.Text = "00.01"
        '
        'ATRRevSign
        '
        Me.ATRRevSign.Location = New System.Drawing.Point(936, 718)
        Me.ATRRevSign.Margin = New System.Windows.Forms.Padding(4)
        Me.ATRRevSign.MaxLength = 6
        Me.ATRRevSign.Name = "ATRRevSign"
        Me.ATRRevSign.ReadOnly = True
        Me.ATRRevSign.Size = New System.Drawing.Size(80, 24)
        Me.ATRRevSign.TabIndex = 15
        '
        'ATRScope
        '
        Me.ATRScope.Location = New System.Drawing.Point(512, 279)
        Me.ATRScope.MaxLength = 40000
        Me.ATRScope.Name = "ATRScope"
        Me.ATRScope.Size = New System.Drawing.Size(734, 410)
        Me.ATRScope.TabIndex = 7
        Me.ATRScope.Text = ""
        '
        'ATRName
        '
        Me.ATRName.Location = New System.Drawing.Point(512, 149)
        Me.ATRName.Margin = New System.Windows.Forms.Padding(4)
        Me.ATRName.MaxLength = 30
        Me.ATRName.Name = "ATRName"
        Me.ATRName.Size = New System.Drawing.Size(255, 24)
        Me.ATRName.TabIndex = 2
        '
        'ATRLeader
        '
        Me.ATRLeader.Location = New System.Drawing.Point(512, 213)
        Me.ATRLeader.Margin = New System.Windows.Forms.Padding(4)
        Me.ATRLeader.Name = "ATRLeader"
        Me.ATRLeader.Size = New System.Drawing.Size(300, 24)
        Me.ATRLeader.TabIndex = 3
        '
        'Label41
        '
        Me.Label41.AutoSize = True
        Me.Label41.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label41.Location = New System.Drawing.Point(959, 67)
        Me.Label41.Name = "Label41"
        Me.Label41.Size = New System.Drawing.Size(303, 18)
        Me.Label41.TabIndex = 80
        Me.Label41.Text = "ATR-namn enligt projektets namnkonvension"
        Me.Label41.Visible = False
        '
        'DeleteATR
        '
        Me.DeleteATR.Location = New System.Drawing.Point(1075, 752)
        Me.DeleteATR.Name = "DeleteATR"
        Me.DeleteATR.Size = New System.Drawing.Size(184, 33)
        Me.DeleteATR.TabIndex = 78
        Me.DeleteATR.Text = "Radera ATR"
        Me.DeleteATR.UseVisualStyleBackColor = True
        Me.DeleteATR.Visible = False
        '
        'WBSLinkLabel
        '
        Me.WBSLinkLabel.AutoSize = True
        Me.WBSLinkLabel.Location = New System.Drawing.Point(923, 155)
        Me.WBSLinkLabel.Name = "WBSLinkLabel"
        Me.WBSLinkLabel.Size = New System.Drawing.Size(77, 18)
        Me.WBSLinkLabel.TabIndex = 77
        Me.WBSLinkLabel.TabStop = True
        Me.WBSLinkLabel.Text = "WBS i IDA"
        '
        'CopyATRButton
        '
        Me.CopyATRButton.Location = New System.Drawing.Point(886, 752)
        Me.CopyATRButton.Name = "CopyATRButton"
        Me.CopyATRButton.Size = New System.Drawing.Size(184, 33)
        Me.CopyATRButton.TabIndex = 76
        Me.CopyATRButton.Text = "Kopiera ATR"
        Me.CopyATRButton.UseVisualStyleBackColor = True
        '
        'Label35
        '
        Me.Label35.AutoSize = True
        Me.Label35.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label35.Location = New System.Drawing.Point(697, 67)
        Me.Label35.Name = "Label35"
        Me.Label35.Size = New System.Drawing.Size(237, 18)
        Me.Label35.TabIndex = 74
        Me.Label35.Text = "Förslag på internt uppdagsnummer"
        '
        'StageGroupe
        '
        Me.StageGroupe.Controls.Add(Me.MakeStagelabel)
        Me.StageGroupe.Controls.Add(Me.StageComboBox)
        Me.StageGroupe.Controls.Add(Me.Stage_DeleteButton)
        Me.StageGroupe.Controls.Add(Me.Stage_NewButton)
        Me.StageGroupe.Controls.Add(Me.Stage_UpdateButton)
        Me.StageGroupe.Controls.Add(Me.StageListBox)
        Me.StageGroupe.Controls.Add(Me.StageFK)
        Me.StageGroupe.Controls.Add(Me.Label32)
        Me.StageGroupe.Controls.Add(Me.Label33)
        Me.StageGroupe.Controls.Add(Me.StageLeader)
        Me.StageGroupe.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.StageGroupe.Location = New System.Drawing.Point(17, 18)
        Me.StageGroupe.Name = "StageGroupe"
        Me.StageGroupe.Size = New System.Drawing.Size(473, 671)
        Me.StageGroupe.TabIndex = 73
        Me.StageGroupe.TabStop = False
        Me.StageGroupe.Text = "Deluppdragsinformation"
        '
        'MakeStagelabel
        '
        Me.MakeStagelabel.AutoSize = True
        Me.MakeStagelabel.Location = New System.Drawing.Point(22, 131)
        Me.MakeStagelabel.Name = "MakeStagelabel"
        Me.MakeStagelabel.Size = New System.Drawing.Size(203, 18)
        Me.MakeStagelabel.TabIndex = 37
        Me.MakeStagelabel.Text = "Namn på möjliga deluppdrags"
        Me.MakeStagelabel.Visible = False
        '
        'StageComboBox
        '
        Me.StageComboBox.FormattingEnabled = True
        Me.StageComboBox.Location = New System.Drawing.Point(22, 77)
        Me.StageComboBox.Name = "StageComboBox"
        Me.StageComboBox.Size = New System.Drawing.Size(422, 26)
        Me.StageComboBox.TabIndex = 36
        '
        'Stage_DeleteButton
        '
        Me.Stage_DeleteButton.Location = New System.Drawing.Point(292, 561)
        Me.Stage_DeleteButton.Name = "Stage_DeleteButton"
        Me.Stage_DeleteButton.Size = New System.Drawing.Size(96, 33)
        Me.Stage_DeleteButton.TabIndex = 33
        Me.Stage_DeleteButton.Text = "radera rad"
        Me.Stage_DeleteButton.UseVisualStyleBackColor = True
        Me.Stage_DeleteButton.Visible = False
        '
        'Stage_NewButton
        '
        Me.Stage_NewButton.Location = New System.Drawing.Point(16, 552)
        Me.Stage_NewButton.Name = "Stage_NewButton"
        Me.Stage_NewButton.Size = New System.Drawing.Size(130, 50)
        Me.Stage_NewButton.TabIndex = 32
        Me.Stage_NewButton.Text = "Skapa nytt deluppdrag"
        Me.Stage_NewButton.UseVisualStyleBackColor = True
        '
        'Stage_UpdateButton
        '
        Me.Stage_UpdateButton.Location = New System.Drawing.Point(152, 552)
        Me.Stage_UpdateButton.Name = "Stage_UpdateButton"
        Me.Stage_UpdateButton.Size = New System.Drawing.Size(130, 50)
        Me.Stage_UpdateButton.TabIndex = 31
        Me.Stage_UpdateButton.Text = "Uppdatera deluppdrag"
        Me.Stage_UpdateButton.UseVisualStyleBackColor = True
        Me.Stage_UpdateButton.Visible = False
        '
        'StageListBox
        '
        Me.StageListBox.FormattingEnabled = True
        Me.StageListBox.ItemHeight = 18
        Me.StageListBox.Location = New System.Drawing.Point(23, 155)
        Me.StageListBox.Name = "StageListBox"
        Me.StageListBox.Size = New System.Drawing.Size(270, 148)
        Me.StageListBox.TabIndex = 28
        Me.StageListBox.Visible = False
        '
        'StageFK
        '
        Me.StageFK.Location = New System.Drawing.Point(22, 445)
        Me.StageFK.Margin = New System.Windows.Forms.Padding(4)
        Me.StageFK.Name = "StageFK"
        Me.StageFK.Size = New System.Drawing.Size(227, 24)
        Me.StageFK.TabIndex = 27
        Me.StageFK.Text = "Valfritt..."
        '
        'Label32
        '
        Me.Label32.AutoSize = True
        Me.Label32.Location = New System.Drawing.Point(19, 325)
        Me.Label32.Name = "Label32"
        Me.Label32.Size = New System.Drawing.Size(131, 18)
        Me.Label32.TabIndex = 22
        Me.Label32.Text = "Deluppdragsledare"
        Me.Label32.Visible = False
        '
        'Label33
        '
        Me.Label33.AutoSize = True
        Me.Label33.Location = New System.Drawing.Point(22, 49)
        Me.Label33.Name = "Label33"
        Me.Label33.Size = New System.Drawing.Size(128, 18)
        Me.Label33.TabIndex = 20
        Me.Label33.Text = "Deluppdragsnamn"
        '
        'StageLeader
        '
        Me.StageLeader.Location = New System.Drawing.Point(23, 351)
        Me.StageLeader.Margin = New System.Windows.Forms.Padding(4)
        Me.StageLeader.Name = "StageLeader"
        Me.StageLeader.Size = New System.Drawing.Size(282, 24)
        Me.StageLeader.TabIndex = 19
        Me.StageLeader.Text = "Valfritt..."
        Me.StageLeader.Visible = False
        '
        'HelpLinklabel
        '
        Me.HelpLinklabel.AutoSize = True
        Me.HelpLinklabel.Location = New System.Drawing.Point(1021, 155)
        Me.HelpLinklabel.Name = "HelpLinklabel"
        Me.HelpLinklabel.Size = New System.Drawing.Size(94, 18)
        Me.HelpLinklabel.TabIndex = 72
        Me.HelpLinklabel.TabStop = True
        Me.HelpLinklabel.Text = "Lathund i IDA"
        '
        'ATR_UpdateButton
        '
        Me.ATR_UpdateButton.Location = New System.Drawing.Point(697, 752)
        Me.ATR_UpdateButton.Name = "ATR_UpdateButton"
        Me.ATR_UpdateButton.Size = New System.Drawing.Size(184, 33)
        Me.ATR_UpdateButton.TabIndex = 69
        Me.ATR_UpdateButton.Text = "Uppdatera ATR"
        Me.ATR_UpdateButton.UseVisualStyleBackColor = True
        '
        'ATR_NewButton
        '
        Me.ATR_NewButton.Location = New System.Drawing.Point(508, 752)
        Me.ATR_NewButton.Name = "ATR_NewButton"
        Me.ATR_NewButton.Size = New System.Drawing.Size(184, 33)
        Me.ATR_NewButton.TabIndex = 67
        Me.ATR_NewButton.Text = "Skapa nytt ATR"
        Me.ATR_NewButton.UseVisualStyleBackColor = True
        '
        'ATRrevDate
        '
        Me.ATRrevDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.ATRrevDate.Location = New System.Drawing.Point(822, 718)
        Me.ATRrevDate.Name = "ATRrevDate"
        Me.ATRrevDate.Size = New System.Drawing.Size(109, 24)
        Me.ATRrevDate.TabIndex = 66
        Me.ATRrevDate.Value = New Date(2015, 2, 23, 0, 0, 0, 0)
        '
        'EndDate
        '
        Me.EndDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.EndDate.Location = New System.Drawing.Point(623, 718)
        Me.EndDate.Name = "EndDate"
        Me.EndDate.Size = New System.Drawing.Size(109, 24)
        Me.EndDate.TabIndex = 65
        Me.EndDate.Value = New Date(2015, 2, 23, 0, 0, 0, 0)
        '
        'StartDate
        '
        Me.StartDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.StartDate.Location = New System.Drawing.Point(509, 718)
        Me.StartDate.Name = "StartDate"
        Me.StartDate.Size = New System.Drawing.Size(109, 24)
        Me.StartDate.TabIndex = 64
        Me.StartDate.Value = New Date(2015, 2, 23, 0, 0, 0, 0)
        '
        'Label38
        '
        Me.Label38.AutoSize = True
        Me.Label38.Location = New System.Drawing.Point(737, 693)
        Me.Label38.Name = "Label38"
        Me.Label38.Size = New System.Drawing.Size(58, 18)
        Me.Label38.TabIndex = 57
        Me.Label38.Text = "Version"
        '
        'Label36
        '
        Me.Label36.AutoSize = True
        Me.Label36.Location = New System.Drawing.Point(936, 693)
        Me.Label36.Name = "Label36"
        Me.Label36.Size = New System.Drawing.Size(68, 18)
        Me.Label36.TabIndex = 55
        Me.Label36.Text = "SwecoID"
        '
        'Label37
        '
        Me.Label37.AutoSize = True
        Me.Label37.Location = New System.Drawing.Point(822, 693)
        Me.Label37.Name = "Label37"
        Me.Label37.Size = New System.Drawing.Size(78, 18)
        Me.Label37.TabIndex = 54
        Me.Label37.Text = "RevDatum"
        '
        'Label27
        '
        Me.Label27.AutoSize = True
        Me.Label27.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label27.Location = New System.Drawing.Point(507, 122)
        Me.Label27.Name = "Label27"
        Me.Label27.Size = New System.Drawing.Size(114, 18)
        Me.Label27.TabIndex = 39
        Me.Label27.Text = "ATR-bladsnamn"
        '
        'Label25
        '
        Me.Label25.AutoSize = True
        Me.Label25.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label25.Location = New System.Drawing.Point(508, 11)
        Me.Label25.Name = "Label25"
        Me.Label25.Size = New System.Drawing.Size(325, 18)
        Me.Label25.TabIndex = 35
        Me.Label25.Text = "Aktiv Resultatenhet -Huvuduppdrag -Deluppdrag "
        '
        'Label23
        '
        Me.Label23.AutoSize = True
        Me.Label23.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label23.Location = New System.Drawing.Point(507, 67)
        Me.Label23.Name = "Label23"
        Me.Label23.Size = New System.Drawing.Size(168, 18)
        Me.Label23.TabIndex = 31
        Me.Label23.Text = "Internt uppdragsnummer"
        '
        'Label20
        '
        Me.Label20.AutoSize = True
        Me.Label20.Location = New System.Drawing.Point(623, 693)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(77, 18)
        Me.Label20.TabIndex = 27
        Me.Label20.Text = "SlutDatum"
        '
        'Label19
        '
        Me.Label19.AutoSize = True
        Me.Label19.Location = New System.Drawing.Point(509, 693)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(80, 18)
        Me.Label19.TabIndex = 25
        Me.Label19.Text = "Startdatum"
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label16.Location = New System.Drawing.Point(508, 252)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(146, 18)
        Me.Label16.TabIndex = 20
        Me.Label16.Text = "Omfattning/Arbetsätt:"
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label14.Location = New System.Drawing.Point(507, 178)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(101, 18)
        Me.Label14.TabIndex = 16
        Me.Label14.Text = "ATR-ansvarig:"
        '
        'TabPage1
        '
        Me.TabPage1.Controls.Add(Me.TRVNameCheckBox)
        Me.TabPage1.Controls.Add(Me.SubprojectGroupe)
        Me.TabPage1.Controls.Add(Me.Label5)
        Me.TabPage1.Controls.Add(Me.Label4)
        Me.TabPage1.Controls.Add(Me.Label3)
        Me.TabPage1.Controls.Add(Me.Label2)
        Me.TabPage1.Controls.Add(Me.Label1)
        Me.TabPage1.Location = New System.Drawing.Point(4, 27)
        Me.TabPage1.Margin = New System.Windows.Forms.Padding(4)
        Me.TabPage1.Name = "TabPage1"
        Me.TabPage1.Padding = New System.Windows.Forms.Padding(4)
        Me.TabPage1.Size = New System.Drawing.Size(1444, 820)
        Me.TabPage1.TabIndex = 0
        Me.TabPage1.Text = "Uppdragsinformation"
        Me.TabPage1.UseVisualStyleBackColor = True
        '
        'TRVNameCheckBox
        '
        Me.TRVNameCheckBox.Controls.Add(Me.ProjectNumber)
        Me.TRVNameCheckBox.Controls.Add(Me.FileNamcheckbox)
        Me.TRVNameCheckBox.Controls.Add(Me.Project_DeleteButton)
        Me.TRVNameCheckBox.Controls.Add(Me.Label6)
        Me.TRVNameCheckBox.Controls.Add(Me.Project_NewButton)
        Me.TRVNameCheckBox.Controls.Add(Me.ProjectName)
        Me.TRVNameCheckBox.Controls.Add(Me.Project_UpdateButton)
        Me.TRVNameCheckBox.Controls.Add(Me.ProjectOwner)
        Me.TRVNameCheckBox.Controls.Add(Me.Projectleader)
        Me.TRVNameCheckBox.Controls.Add(Me.Label10)
        Me.TRVNameCheckBox.Controls.Add(Me.CustomerNumber)
        Me.TRVNameCheckBox.Controls.Add(Me.Label9)
        Me.TRVNameCheckBox.Controls.Add(Me.Label8)
        Me.TRVNameCheckBox.Controls.Add(Me.Label7)
        Me.TRVNameCheckBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TRVNameCheckBox.Location = New System.Drawing.Point(8, 26)
        Me.TRVNameCheckBox.Name = "TRVNameCheckBox"
        Me.TRVNameCheckBox.Size = New System.Drawing.Size(466, 640)
        Me.TRVNameCheckBox.TabIndex = 25
        Me.TRVNameCheckBox.TabStop = False
        Me.TRVNameCheckBox.Text = "Resultatenhetsinformation"
        '
        'ProjectNumber
        '
        Me.ProjectNumber.FormattingEnabled = True
        Me.ProjectNumber.Location = New System.Drawing.Point(10, 192)
        Me.ProjectNumber.MaxLength = 10
        Me.ProjectNumber.Name = "ProjectNumber"
        Me.ProjectNumber.Size = New System.Drawing.Size(286, 26)
        Me.ProjectNumber.TabIndex = 35
        '
        'FileNamcheckbox
        '
        Me.FileNamcheckbox.AutoSize = True
        Me.FileNamcheckbox.Location = New System.Drawing.Point(10, 463)
        Me.FileNamcheckbox.Name = "FileNamcheckbox"
        Me.FileNamcheckbox.Size = New System.Drawing.Size(369, 22)
        Me.FileNamcheckbox.TabIndex = 34
        Me.FileNamcheckbox.Text = "Använd annat ATR-namn än Internuppdragsnummer"
        Me.FileNamcheckbox.UseVisualStyleBackColor = True
        Me.FileNamcheckbox.Visible = False
        '
        'Project_DeleteButton
        '
        Me.Project_DeleteButton.Location = New System.Drawing.Point(297, 522)
        Me.Project_DeleteButton.Name = "Project_DeleteButton"
        Me.Project_DeleteButton.Size = New System.Drawing.Size(131, 63)
        Me.Project_DeleteButton.TabIndex = 33
        Me.Project_DeleteButton.Text = "radera rad"
        Me.Project_DeleteButton.UseVisualStyleBackColor = True
        Me.Project_DeleteButton.Visible = False
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(6, 100)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(143, 18)
        Me.Label6.TabIndex = 10
        Me.Label6.Text = "Resultatenhetsnamn"
        '
        'Project_NewButton
        '
        Me.Project_NewButton.Location = New System.Drawing.Point(9, 520)
        Me.Project_NewButton.Name = "Project_NewButton"
        Me.Project_NewButton.Size = New System.Drawing.Size(131, 63)
        Me.Project_NewButton.TabIndex = 32
        Me.Project_NewButton.Text = "Skapa ny Resultatenhet"
        Me.Project_NewButton.UseVisualStyleBackColor = True
        Me.Project_NewButton.Visible = False
        '
        'ProjectName
        '
        Me.ProjectName.Location = New System.Drawing.Point(10, 126)
        Me.ProjectName.Margin = New System.Windows.Forms.Padding(4)
        Me.ProjectName.Name = "ProjectName"
        Me.ProjectName.Size = New System.Drawing.Size(286, 24)
        Me.ProjectName.TabIndex = 1
        '
        'Project_UpdateButton
        '
        Me.Project_UpdateButton.Location = New System.Drawing.Point(151, 520)
        Me.Project_UpdateButton.Name = "Project_UpdateButton"
        Me.Project_UpdateButton.Size = New System.Drawing.Size(131, 63)
        Me.Project_UpdateButton.TabIndex = 31
        Me.Project_UpdateButton.Text = "Uppdatera resultatenhet"
        Me.Project_UpdateButton.UseVisualStyleBackColor = True
        Me.Project_UpdateButton.Visible = False
        '
        'ProjectOwner
        '
        Me.ProjectOwner.Location = New System.Drawing.Point(10, 277)
        Me.ProjectOwner.Margin = New System.Windows.Forms.Padding(4)
        Me.ProjectOwner.Name = "ProjectOwner"
        Me.ProjectOwner.Size = New System.Drawing.Size(286, 24)
        Me.ProjectOwner.TabIndex = 3
        '
        'Projectleader
        '
        Me.Projectleader.Location = New System.Drawing.Point(10, 345)
        Me.Projectleader.Margin = New System.Windows.Forms.Padding(4)
        Me.Projectleader.Name = "Projectleader"
        Me.Projectleader.Size = New System.Drawing.Size(286, 24)
        Me.Projectleader.TabIndex = 4
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(6, 387)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(175, 18)
        Me.Label10.TabIndex = 14
        Me.Label10.Text = "Uppdragsnummer (Kund)"
        Me.Label10.Visible = False
        '
        'CustomerNumber
        '
        Me.CustomerNumber.Location = New System.Drawing.Point(10, 413)
        Me.CustomerNumber.Margin = New System.Windows.Forms.Padding(4)
        Me.CustomerNumber.Name = "CustomerNumber"
        Me.CustomerNumber.Size = New System.Drawing.Size(286, 24)
        Me.CustomerNumber.TabIndex = 5
        Me.CustomerNumber.Visible = False
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(4, 321)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(112, 18)
        Me.Label9.TabIndex = 13
        Me.Label9.Text = "Uppdragsledare"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(6, 251)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(151, 18)
        Me.Label8.TabIndex = 12
        Me.Label8.Text = "Resultatenhetssägare"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(6, 171)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(161, 18)
        Me.Label7.TabIndex = 11
        Me.Label7.Text = "Resultatenhetsnummer"
        '
        'SubprojectGroupe
        '
        Me.SubprojectGroupe.Controls.Add(Me.SubprojectNumber)
        Me.SubprojectGroupe.Controls.Add(Me.Sub_DeleteButton)
        Me.SubprojectGroupe.Controls.Add(Me.Label29)
        Me.SubprojectGroupe.Controls.Add(Me.Sub_NewButton)
        Me.SubprojectGroupe.Controls.Add(Me.Label31)
        Me.SubprojectGroupe.Controls.Add(Me.Label30)
        Me.SubprojectGroupe.Controls.Add(Me.Sub_UpdateButton)
        Me.SubprojectGroupe.Controls.Add(Me.SubProjectName)
        Me.SubprojectGroupe.Controls.Add(Me.SubProjectLeader)
        Me.SubprojectGroupe.Controls.Add(Me.SubprojectFK)
        Me.SubprojectGroupe.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.SubprojectGroupe.Location = New System.Drawing.Point(533, 26)
        Me.SubprojectGroupe.Name = "SubprojectGroupe"
        Me.SubprojectGroupe.Size = New System.Drawing.Size(425, 640)
        Me.SubprojectGroupe.TabIndex = 15
        Me.SubprojectGroupe.TabStop = False
        Me.SubprojectGroupe.Text = "Huvuduppdragsinformation"
        '
        'SubprojectNumber
        '
        Me.SubprojectNumber.FormattingEnabled = True
        Me.SubprojectNumber.Location = New System.Drawing.Point(21, 171)
        Me.SubprojectNumber.Name = "SubprojectNumber"
        Me.SubprojectNumber.Size = New System.Drawing.Size(286, 26)
        Me.SubprojectNumber.TabIndex = 31
        '
        'Sub_DeleteButton
        '
        Me.Sub_DeleteButton.Location = New System.Drawing.Point(274, 381)
        Me.Sub_DeleteButton.Name = "Sub_DeleteButton"
        Me.Sub_DeleteButton.Size = New System.Drawing.Size(120, 52)
        Me.Sub_DeleteButton.TabIndex = 30
        Me.Sub_DeleteButton.Text = "radera rad"
        Me.Sub_DeleteButton.UseVisualStyleBackColor = True
        Me.Sub_DeleteButton.Visible = False
        '
        'Label29
        '
        Me.Label29.AutoSize = True
        Me.Label29.Location = New System.Drawing.Point(17, 222)
        Me.Label29.Name = "Label29"
        Me.Label29.Size = New System.Drawing.Size(151, 18)
        Me.Label29.TabIndex = 22
        Me.Label29.Text = "Huvuduppdragsledare"
        '
        'Sub_NewButton
        '
        Me.Sub_NewButton.Location = New System.Drawing.Point(20, 379)
        Me.Sub_NewButton.Name = "Sub_NewButton"
        Me.Sub_NewButton.Size = New System.Drawing.Size(120, 52)
        Me.Sub_NewButton.TabIndex = 27
        Me.Sub_NewButton.Text = "Skapa nytt huvuduppdrag"
        Me.Sub_NewButton.UseVisualStyleBackColor = True
        Me.Sub_NewButton.Visible = False
        '
        'Label31
        '
        Me.Label31.AutoSize = True
        Me.Label31.Location = New System.Drawing.Point(17, 71)
        Me.Label31.Name = "Label31"
        Me.Label31.Size = New System.Drawing.Size(148, 18)
        Me.Label31.TabIndex = 20
        Me.Label31.Text = "Huvuduppdragsnamn"
        '
        'Label30
        '
        Me.Label30.AutoSize = True
        Me.Label30.Location = New System.Drawing.Point(17, 142)
        Me.Label30.Name = "Label30"
        Me.Label30.Size = New System.Drawing.Size(166, 18)
        Me.Label30.TabIndex = 21
        Me.Label30.Text = "Huvuduppdragsnummer"
        '
        'Sub_UpdateButton
        '
        Me.Sub_UpdateButton.Location = New System.Drawing.Point(146, 379)
        Me.Sub_UpdateButton.Name = "Sub_UpdateButton"
        Me.Sub_UpdateButton.Size = New System.Drawing.Size(120, 52)
        Me.Sub_UpdateButton.TabIndex = 23
        Me.Sub_UpdateButton.Text = "Uppdatera huvuduppdrag"
        Me.Sub_UpdateButton.UseVisualStyleBackColor = True
        Me.Sub_UpdateButton.Visible = False
        '
        'SubProjectName
        '
        Me.SubProjectName.Location = New System.Drawing.Point(21, 97)
        Me.SubProjectName.Margin = New System.Windows.Forms.Padding(4)
        Me.SubProjectName.MaxLength = 30
        Me.SubProjectName.Name = "SubProjectName"
        Me.SubProjectName.Size = New System.Drawing.Size(286, 24)
        Me.SubProjectName.TabIndex = 17
        '
        'SubProjectLeader
        '
        Me.SubProjectLeader.Location = New System.Drawing.Point(21, 248)
        Me.SubProjectLeader.Margin = New System.Windows.Forms.Padding(4)
        Me.SubProjectLeader.Name = "SubProjectLeader"
        Me.SubProjectLeader.Size = New System.Drawing.Size(286, 24)
        Me.SubProjectLeader.TabIndex = 19
        Me.SubProjectLeader.Text = "Valfritt..."
        '
        'SubprojectFK
        '
        Me.SubprojectFK.Location = New System.Drawing.Point(21, 302)
        Me.SubprojectFK.Margin = New System.Windows.Forms.Padding(4)
        Me.SubprojectFK.Name = "SubprojectFK"
        Me.SubprojectFK.Size = New System.Drawing.Size(349, 24)
        Me.SubprojectFK.TabIndex = 26
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(9, 313)
        Me.Label5.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(0, 18)
        Me.Label5.TabIndex = 8
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(9, 97)
        Me.Label4.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(0, 18)
        Me.Label4.TabIndex = 6
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(9, 245)
        Me.Label3.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(0, 18)
        Me.Label3.TabIndex = 4
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(9, 177)
        Me.Label2.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(0, 18)
        Me.Label2.TabIndex = 2
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(9, 26)
        Me.Label1.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(0, 18)
        Me.Label1.TabIndex = 0
        '
        'tabControl
        '
        Me.tabControl.Controls.Add(Me.TabPage1)
        Me.tabControl.Controls.Add(Me.TabPage4)
        Me.tabControl.Controls.Add(Me.TabPage2)
        Me.tabControl.Controls.Add(Me.TabPage7)
        Me.tabControl.Controls.Add(Me.ATRMemo)
        Me.tabControl.Controls.Add(Me.TabPage5)
        Me.tabControl.Controls.Add(Me.TabPage3)
        Me.tabControl.Controls.Add(Me.TabPage6)
        Me.tabControl.Dock = System.Windows.Forms.DockStyle.Top
        Me.tabControl.Location = New System.Drawing.Point(0, 28)
        Me.tabControl.Margin = New System.Windows.Forms.Padding(4)
        Me.tabControl.Name = "tabControl"
        Me.tabControl.SelectedIndex = 0
        Me.tabControl.Size = New System.Drawing.Size(1452, 851)
        Me.tabControl.TabIndex = 1
        '
        'MainForm
        '
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None
        Me.AutoScroll = True
        Me.AutoSize = True
        Me.ClientSize = New System.Drawing.Size(1473, 803)
        Me.Controls.Add(Me.tabControl)
        Me.Controls.Add(Me.MenuStrip1)
        Me.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.MainMenuStrip = Me.MenuStrip1
        Me.Margin = New System.Windows.Forms.Padding(4)
        Me.Name = "MainForm"
        Me.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Text = "SwecoATR"
        Me.MenuStrip1.ResumeLayout(False)
        Me.MenuStrip1.PerformLayout()
        Me.TabPage6.ResumeLayout(False)
        Me.TabPage6.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.TabPage3.ResumeLayout(False)
        CType(Me.CatGrid, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ResourceGrid, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabPage5.ResumeLayout(False)
        Me.TabPage5.PerformLayout()
        Me.ATRSelectGroupeBox.ResumeLayout(False)
        Me.ATRSelectGroupeBox.PerformLayout()
        Me.ATRMemo.ResumeLayout(False)
        Me.ATRMemo.PerformLayout()
        Me.TabPage7.ResumeLayout(False)
        Me.TabPage7.PerformLayout()
        CType(Me.FixedCostsGrid, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ATRResourcesGrid, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabPage2.ResumeLayout(False)
        Me.TabPage2.PerformLayout()
        CType(Me.ATRInputGrid, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ATRRisksGrid, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ATRDeliveriesGrid, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabPage4.ResumeLayout(False)
        Me.TabPage4.PerformLayout()
        Me.StageGroupe.ResumeLayout(False)
        Me.StageGroupe.PerformLayout()
        Me.TabPage1.ResumeLayout(False)
        Me.TabPage1.PerformLayout()
        Me.TRVNameCheckBox.ResumeLayout(False)
        Me.TRVNameCheckBox.PerformLayout()
        Me.SubprojectGroupe.ResumeLayout(False)
        Me.SubprojectGroupe.PerformLayout()
        Me.tabControl.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents MenuStrip1 As System.Windows.Forms.MenuStrip
    Friend WithEvents ArkivToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents OpenToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents NewProjectToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ImportExcelToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents OpenFileDialog1 As System.Windows.Forms.OpenFileDialog
    Friend WithEvents WordTemplatePath As System.Windows.Forms.Label
    Friend WithEvents WordFolderDialog As System.Windows.Forms.FolderBrowserDialog
    Friend WithEvents OpenWordTempltateFileDialog As System.Windows.Forms.OpenFileDialog
    Friend WithEvents ImporteraWBSToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents WBSFileDialog As System.Windows.Forms.OpenFileDialog
    Friend WithEvents TabPage6 As System.Windows.Forms.TabPage
    Friend WithEvents ListSelectProjectRButton As System.Windows.Forms.RadioButton
    Friend WithEvents TextBox1 As System.Windows.Forms.TextBox
    Friend WithEvents ListGenSubProjectNumber As System.Windows.Forms.TextBox
    Friend WithEvents ListSelectStageRButton As System.Windows.Forms.RadioButton
    Friend WithEvents ListSelectSubProjectRButton As System.Windows.Forms.RadioButton
    Friend WithEvents CreateList As System.Windows.Forms.Button
    Friend WithEvents ListSelectBox As System.Windows.Forms.ListBox
    Friend WithEvents Label24 As System.Windows.Forms.Label
    Friend WithEvents TabPage3 As System.Windows.Forms.TabPage
    Friend WithEvents ChangeCatOnResourseButton As System.Windows.Forms.Button
    Friend WithEvents UpdateResourceButton As System.Windows.Forms.Button
    Friend WithEvents CatGrid As System.Windows.Forms.DataGridView
    Friend WithEvents ResourceGrid As System.Windows.Forms.DataGridView
    Friend WithEvents TabPage5 As System.Windows.Forms.TabPage
    Friend WithEvents ATRInWordFormat As System.Windows.Forms.CheckBox
    Friend WithEvents OpenPDFCatalog As System.Windows.Forms.Button
    Friend WithEvents ATRPrintRevSignBox As System.Windows.Forms.TextBox
    Friend WithEvents ATRPrintVersionBox As System.Windows.Forms.TextBox
    Friend WithEvents ATRRevSignRadioButton As System.Windows.Forms.RadioButton
    Friend WithEvents ATRVersionRadioButton As System.Windows.Forms.RadioButton
    Friend WithEvents ATRDateRadioButton As System.Windows.Forms.RadioButton
    Friend WithEvents ATRSelectGroupeBox As System.Windows.Forms.GroupBox
    Friend WithEvents ATRRevDateRadioButton As System.Windows.Forms.RadioButton
    Friend WithEvents ATRPrintDatePicker As System.Windows.Forms.DateTimePicker
    Friend WithEvents ATREndDateRadioButton As System.Windows.Forms.RadioButton
    Friend WithEvents ATRStartDateRadioButton As System.Windows.Forms.RadioButton
    Friend WithEvents WordFolderLabel As System.Windows.Forms.Label
    Friend WithEvents WordFilepath As System.Windows.Forms.Button
    Friend WithEvents Label26 As System.Windows.Forms.Label
    Friend WithEvents ATRRadioButton As System.Windows.Forms.RadioButton
    Friend WithEvents StageATRListRadioButton As System.Windows.Forms.RadioButton
    Friend WithEvents SubprojectATRListRadioButton As System.Windows.Forms.RadioButton
    Friend WithEvents FullATRListRadioButton As System.Windows.Forms.RadioButton
    Friend WithEvents CreateATR As System.Windows.Forms.Button
    Friend WithEvents ATRMemo As System.Windows.Forms.TabPage
    Friend WithEvents ATRNrMemo As System.Windows.Forms.Label
    Friend WithEvents Label39 As System.Windows.Forms.Label
    Friend WithEvents ATRMemoSave As System.Windows.Forms.Button
    Friend WithEvents MemoText As System.Windows.Forms.RichTextBox
    Friend WithEvents TabPage7 As System.Windows.Forms.TabPage
    Friend WithEvents TotalCostATR As System.Windows.Forms.Label
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents ATRPageNr2 As System.Windows.Forms.Label
    Friend WithEvents Label22 As System.Windows.Forms.Label
    Friend WithEvents SaveATRResCostButton As System.Windows.Forms.Button
    Friend WithEvents FixedCostsGrid As System.Windows.Forms.DataGridView
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents ATRResourcesGrid As System.Windows.Forms.DataGridView
    Friend WithEvents Label28 As System.Windows.Forms.Label
    Friend WithEvents TabPage2 As System.Windows.Forms.TabPage
    Friend WithEvents ResultText As System.Windows.Forms.RichTextBox
    Friend WithEvents ATRPageNr1 As System.Windows.Forms.Label
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents SaveATRButton As System.Windows.Forms.Button
    Friend WithEvents ATRInputGrid As System.Windows.Forms.DataGridView
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents ATRRisksGrid As System.Windows.Forms.DataGridView
    Friend WithEvents ATRDeliveriesGrid As System.Windows.Forms.DataGridView
    Friend WithEvents Label21 As System.Windows.Forms.Label
    Friend WithEvents Label18 As System.Windows.Forms.Label
    Friend WithEvents Label17 As System.Windows.Forms.Label
    Friend WithEvents TabPage4 As System.Windows.Forms.TabPage
    Friend WithEvents TRVNameBox As System.Windows.Forms.TextBox
    Friend WithEvents GenATRnumber As System.Windows.Forms.TextBox
    Friend WithEvents ATR_FK As System.Windows.Forms.TextBox
    Friend WithEvents ATRLink As System.Windows.Forms.TextBox
    Friend WithEvents ATRVersion As System.Windows.Forms.TextBox
    Friend WithEvents ATRRevSign As System.Windows.Forms.TextBox
    Friend WithEvents ATRScope As System.Windows.Forms.RichTextBox
    Friend WithEvents ATRName As System.Windows.Forms.TextBox
    Friend WithEvents ATRLeader As System.Windows.Forms.TextBox
    Friend WithEvents Label41 As System.Windows.Forms.Label
    Friend WithEvents DeleteATR As System.Windows.Forms.Button
    Friend WithEvents WBSLinkLabel As System.Windows.Forms.LinkLabel
    Friend WithEvents CopyATRButton As System.Windows.Forms.Button
    Friend WithEvents Label35 As System.Windows.Forms.Label
    Friend WithEvents StageGroupe As System.Windows.Forms.GroupBox
    Friend WithEvents MakeStagelabel As System.Windows.Forms.Label
    Friend WithEvents StageComboBox As System.Windows.Forms.ComboBox
    Friend WithEvents Stage_DeleteButton As System.Windows.Forms.Button
    Friend WithEvents Stage_NewButton As System.Windows.Forms.Button
    Friend WithEvents Stage_UpdateButton As System.Windows.Forms.Button
    Friend WithEvents StageListBox As System.Windows.Forms.ListBox
    Friend WithEvents StageFK As System.Windows.Forms.TextBox
    Friend WithEvents Label32 As System.Windows.Forms.Label
    Friend WithEvents Label33 As System.Windows.Forms.Label
    Friend WithEvents StageLeader As System.Windows.Forms.TextBox
    Friend WithEvents HelpLinklabel As System.Windows.Forms.LinkLabel
    Friend WithEvents ATR_UpdateButton As System.Windows.Forms.Button
    Friend WithEvents ATR_NewButton As System.Windows.Forms.Button
    Friend WithEvents ATRrevDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents EndDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents StartDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label38 As System.Windows.Forms.Label
    Friend WithEvents Label36 As System.Windows.Forms.Label
    Friend WithEvents Label37 As System.Windows.Forms.Label
    Friend WithEvents Label27 As System.Windows.Forms.Label
    Friend WithEvents Label25 As System.Windows.Forms.Label
    Friend WithEvents Label23 As System.Windows.Forms.Label
    Friend WithEvents Label20 As System.Windows.Forms.Label
    Friend WithEvents Label19 As System.Windows.Forms.Label
    Friend WithEvents Label16 As System.Windows.Forms.Label
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents TabPage1 As System.Windows.Forms.TabPage
    Friend WithEvents TRVNameCheckBox As System.Windows.Forms.GroupBox
    Public WithEvents ProjectNumber As System.Windows.Forms.ComboBox
    Friend WithEvents FileNamcheckbox As System.Windows.Forms.CheckBox
    Friend WithEvents Project_DeleteButton As System.Windows.Forms.Button
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Project_NewButton As System.Windows.Forms.Button
    Friend WithEvents ProjectName As System.Windows.Forms.TextBox
    Friend WithEvents Project_UpdateButton As System.Windows.Forms.Button
    Friend WithEvents ProjectOwner As System.Windows.Forms.TextBox
    Friend WithEvents Projectleader As System.Windows.Forms.TextBox
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents CustomerNumber As System.Windows.Forms.TextBox
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents SubprojectGroupe As System.Windows.Forms.GroupBox
    Friend WithEvents SubprojectNumber As System.Windows.Forms.ComboBox
    Friend WithEvents Sub_DeleteButton As System.Windows.Forms.Button
    Friend WithEvents Label29 As System.Windows.Forms.Label
    Friend WithEvents Sub_NewButton As System.Windows.Forms.Button
    Friend WithEvents Label31 As System.Windows.Forms.Label
    Friend WithEvents Label30 As System.Windows.Forms.Label
    Friend WithEvents Sub_UpdateButton As System.Windows.Forms.Button
    Friend WithEvents SubProjectName As System.Windows.Forms.TextBox
    Friend WithEvents SubProjectLeader As System.Windows.Forms.TextBox
    Friend WithEvents SubprojectFK As System.Windows.Forms.TextBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents tabControl As System.Windows.Forms.TabControl
    Friend WithEvents ATRnumber As System.Windows.Forms.ComboBox
    Friend WithEvents WBSIIDAToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents LathundIIDAToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents BoldTextScope As System.Windows.Forms.Button
    Friend WithEvents UnderLineTextScope As System.Windows.Forms.Button
    Friend WithEvents UnderlineTextResult As System.Windows.Forms.Button
    Friend WithEvents BoldTextResult As System.Windows.Forms.Button
    Friend WithEvents TextBox2 As TextBox
    Friend WithEvents Label34 As Label
    Friend WithEvents GroupBox1 As GroupBox
    Friend WithEvents SearchText As TextBox
    Friend WithEvents Label40 As Label
    Friend WithEvents ATRRisksCheckBox As CheckBox
    Friend WithEvents ATROutPutCheckBox As CheckBox
    Friend WithEvents ATRResultCheckBox As CheckBox
    Friend WithEvents ATRInputsCheckbox As CheckBox
    Friend WithEvents ATRScopeCheckbox As CheckBox
    Friend WithEvents Label42 As Label
    Friend WithEvents SearchButton As Button
    Friend WithEvents GroupBox2 As GroupBox
    Friend WithEvents Label44 As Label
    Friend WithEvents Label43 As Label
    Friend WithEvents SDate As DateTimePicker
    Friend WithEvents EDate As DateTimePicker
    Friend WithEvents DateSort As CheckBox
End Class
