﻿Imports System
Imports System.IO

Module Basicfunctions
    Public MainFolder As String = "C:\temp"
    Private ReadOnly ReportFileName As String = "Report.txt"
    Private ReadOnly ChangeLog As String = "ChangeLog.txt"
    ''' <summary>
    ''' Skapara en rad i en textfil med felet som skapats
    ''' </summary>
    ''' <param name="FunctionName">
    ''' sträng  som innehåller namnet på funktionen som skapade felet
    ''' </param>
    ''' <param name="ex">
    ''' det undantag som har skapats
    ''' </param>
    ''' 
    ''' <remarks></remarks>
    Public Sub MakeErrorReport(FunctionName As String, ex As System.Exception)
        Dim TxtString As String
        TxtString = FunctionName & " " & ex.Message & " ; Fel loggat av:" & Environment.UserName & " ,datum :" & System.DateTime.Now & Environment.NewLine
        File.AppendAllText(MainFolder & "\" & ReportFileName, TxtString)
    End Sub
    ''' <summary>
    ''' Funktionen används för att skapa en rad i textfil med aktiviteter i programmet
    ''' </summary>
    ''' <param name="Action">
    ''' Sträng som innehåller aktiviteten som är genomförd
    ''' </param>
    ''' <remarks></remarks>
    Public Sub MakeLogUpdate(Action As String)
        Dim TxtString As String
        TxtString = Action & Environment.NewLine
        File.AppendAllText(MainFolder & "\" & ChangeLog, TxtString)
    End Sub
    ''' <summary>
    ''' Funktion som hittar en given symbol och delar strängen i två delar 
    ''' Första delen retuneras och andra delen sparas i invariabeln
    ''' </summary>
    ''' <param name="Rad">
    ''' Rad innehåller strängen som skall genomsökas
    ''' Efter funktionen innehåller Rad strängen som följer Symbol
    ''' </param>
    ''' <param name="Symbol">
    ''' Symbol innehåller det tecken som skall letas efter
    ''' </param>
    ''' <returns>
    ''' Retunerar strängen fram till teknet i Symbol
    ''' </returns>
    ''' <remarks></remarks>
    Public Function FindInString(ByRef Rad As String, ByVal Symbol As String)
        Dim pos As Integer
        pos = Strings.InStr(1, Rad, Symbol)
        If pos > 0 Then
            FindInString = Trim(Mid(Rad, Len(Symbol), pos - Len(Symbol)))
            Rad = Trim(Mid(Rad, pos + Len(Symbol), Rad.Length))
        Else
            FindInString = Rad
            Rad = ""
        End If
    End Function 
End Module
