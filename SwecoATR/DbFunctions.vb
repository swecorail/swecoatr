﻿Imports System.Data
Imports System.Data.OleDb
Imports System.Configuration
Imports Microsoft.Office.Interop
Imports System.IO

Module DbFunctions
    Public ReadOnly FULL_LIST As Integer = 0
    Public ReadOnly Singel_FK_ID As Integer = 1
    Public ReadOnly ONLY_ID As Integer = 2
    Public ReadOnly CLASS_SPEC_TYPE As Integer = 3
    Public ReadOnly CLASS_SPEC_TYPE2 As Integer = 4

    Private ATRDbMasterAdapter As ATRDbDataSetTableAdapters.TableAdapterManager

    Private DbBuilder As OleDbConnectionStringBuilder = New OleDbConnectionStringBuilder With {.PersistSecurityInfo = False, _
                                                                                               .Provider = "Microsoft.ACE.OLEDB.12.0"}
    Private con As OleDbConnection = New OleDbConnection("")


    Public Sub CreateDb(dbPath As String)
        Try
            If File.Exists(dbPath) = False Then
                Dim AccApp As New Access.Application()
                AccApp.NewCurrentDatabase(dbPath, Access.AcNewDatabaseFormat.acNewDatabaseFormatAccess2007, Type.Missing)
                AccApp.CloseCurrentDatabase()
                Runtime.InteropServices.Marshal.FinalReleaseComObject(AccApp)
                AccApp = Nothing
            End If

            OpenDb(dbPath)
            CreateTables()
        Catch ex As Exception
            MsgBox("Fel vid skapande av databas")
            MakeErrorReport("CreateDb: ", ex)
            Throw New ApplicationException("CreateDb Error:")
        End Try
    End Sub

    Public Sub OpenDb(dbPath As String)
        Try
            con = New OleDbConnection()

            Const DataProvider As String = "Provider = Microsoft.ACE.OLEDB.12.0; Data Source = "
            con.ConnectionString = DataProvider + dbPath

        Catch ex As Exception
            MsgBox("Fel vid öppnadet av databas")
            MakeErrorReport("OpenDb: ", ex)
            Throw New ApplicationException("OpenDbError")
            con = Nothing
        End Try
    End Sub

    Public Function ConnectDataSet() As DataSet
        Try

            Dim DSet As ATRDbDataSet = New ATRDbDataSet
            ATRDbMasterAdapter = New ATRDbDataSetTableAdapters.TableAdapterManager
            ATRDbMasterAdapter.ProjectTableAdapter = New ATRDbDataSetTableAdapters.ProjectTableAdapter()
            ATRDbMasterAdapter.SubProjectTableAdapter = New ATRDbDataSetTableAdapters.SubProjectTableAdapter()
            ATRDbMasterAdapter.StageTableAdapter = New ATRDbDataSetTableAdapters.StageTableAdapter()

            ATRDbMasterAdapter.ATRTableAdapter = New ATRDbDataSetTableAdapters.ATRTableAdapter()

            ATRDbMasterAdapter.RisksTableAdapter = New ATRDbDataSetTableAdapters.RisksTableAdapter()
            ATRDbMasterAdapter.ResultsTableAdapter = New ATRDbDataSetTableAdapters.ResultsTableAdapter()
            ATRDbMasterAdapter.InputsTableAdapter = New ATRDbDataSetTableAdapters.InputsTableAdapter
            ATRDbMasterAdapter.DelivieriesTableAdapter = New ATRDbDataSetTableAdapters.DelivieriesTableAdapter()

            ATRDbMasterAdapter.FixedCostTableAdapter = New ATRDbDataSetTableAdapters.FixedCostTableAdapter()
            ATRDbMasterAdapter.AReCatJuncTableAdapter = New ATRDbDataSetTableAdapters.AReCatJuncTableAdapter()

            ATRDbMasterAdapter.CatTableAdapter = New ATRDbDataSetTableAdapters.CatTableAdapter()
            ATRDbMasterAdapter.ResourceTableAdapter = New ATRDbDataSetTableAdapters.ResourceTableAdapter()

            ATRDbMasterAdapter.ProjectTableAdapter.Connection = con
            ATRDbMasterAdapter.SubProjectTableAdapter.Connection = con
            ATRDbMasterAdapter.StageTableAdapter.Connection = con

            ATRDbMasterAdapter.ATRTableAdapter.Connection = con

            ATRDbMasterAdapter.RisksTableAdapter.Connection = con
            ATRDbMasterAdapter.ResultsTableAdapter.Connection = con
            ATRDbMasterAdapter.InputsTableAdapter.Connection = con
            ATRDbMasterAdapter.DelivieriesTableAdapter.Connection = con

            ATRDbMasterAdapter.FixedCostTableAdapter.Connection = con
            ATRDbMasterAdapter.AReCatJuncTableAdapter.Connection = con

            ATRDbMasterAdapter.CatTableAdapter.Connection = con
            ATRDbMasterAdapter.ResourceTableAdapter.Connection = con

            ATRDbMasterAdapter.ProjectTableAdapter.Fill(DSet.Project)
            ATRDbMasterAdapter.SubProjectTableAdapter.Fill(DSet.SubProject)
            ATRDbMasterAdapter.StageTableAdapter.Fill(DSet.Stage)

            ATRDbMasterAdapter.ATRTableAdapter.Fill(DSet.ATR)

            ATRDbMasterAdapter.RisksTableAdapter.Fill(DSet.Risks)
            ATRDbMasterAdapter.ResultsTableAdapter.Fill(DSet.Results)
            ATRDbMasterAdapter.InputsTableAdapter.Fill(DSet.Inputs)
            ATRDbMasterAdapter.DelivieriesTableAdapter.Fill(DSet.Delivieries)

            ATRDbMasterAdapter.FixedCostTableAdapter.Fill(DSet.FixedCost)
            ATRDbMasterAdapter.AReCatJuncTableAdapter.Fill(DSet.AReCatJunc)

            ATRDbMasterAdapter.CatTableAdapter.Fill(DSet.Cat)
            ATRDbMasterAdapter.ResourceTableAdapter.Fill(DSet.Resource)

            AddHandler ATRDbMasterAdapter.RisksTableAdapter.Adapter.RowUpdated, New OleDbRowUpdatedEventHandler(AddressOf HandleRiskRowUpdated)

            AddHandler ATRDbMasterAdapter.InputsTableAdapter.Adapter.RowUpdated, New OleDbRowUpdatedEventHandler(AddressOf HandleInputsRowUpdated)

            AddHandler ATRDbMasterAdapter.DelivieriesTableAdapter.Adapter.RowUpdated, New OleDbRowUpdatedEventHandler(AddressOf HandleDelivieriesRowUpdated)

            AddHandler ATRDbMasterAdapter.FixedCostTableAdapter.Adapter.RowUpdated, New OleDbRowUpdatedEventHandler(AddressOf HandleFixedCostRowUpdated)

            Return DSet
        Catch ex As Exception
            MakeErrorReport("Error in ConnectDataSet: ", ex)
            Throw New ApplicationException("ConnectDataSetError")
            Return Nothing
        End Try
    End Function

    Private Sub CreateTables()
        Try

        Catch ex As Exception
            MakeErrorReport("CreateTables: ", ex)
            Throw New ApplicationException("OpenDbError")
            con = Nothing
        End Try
    End Sub

    Public Function InsertTable(DSet As DataSet, TableName As String) As Integer
        Dim Result As Integer = 0
        Try
            ATRDbMasterAdapter.UpdateAll(DSet)
            Result = 1

        Catch ex As Exception
            If InStr(ex.Message, "Your network access was interrupted.") > 0 Then
                MsgBox("Probelm med nätverket, inget har sparats i databasen" & Environment.NewLine &
                       "Spara undan skapade texter på annan plats")

            ElseIf StrComp(Strings.Left(ex.Message, 40), "Column 'ID' is constrained to be unique.") = 0 Then
                MsgBox("ATR-Nummer måste vara unikt")
            End If

            DSet.RejectChanges()

            Result = 0
            MakeErrorReport("InsertTable: " & TableName & ", ", ex)
        End Try
        Return Result
    End Function

    Public Function DBconnectionExit() As Boolean
        If StrComp(con.DataSource, "") = 0 Then
            Return False
        Else
            Return True
        End If
    End Function

    Private Sub HandleRiskRowUpdated(ByVal sender As Object, ByVal e As OleDbRowUpdatedEventArgs)
        If e.Status = System.Data.UpdateStatus.Continue AndAlso e.StatementType = System.Data.StatementType.Insert Then
            e.Row("ID") = Int32.Parse(ATRDbMasterAdapter.RisksTableAdapter.ScalarQuery.ToString)
            e.Row.AcceptChanges()
            'e.tatus = UpdateStatus.SkipCurrentRow
        End If
    End Sub

    Private Sub HandleDelivieriesRowUpdated(ByVal sender As Object, ByVal e As OleDbRowUpdatedEventArgs)
        If e.Status = System.Data.UpdateStatus.Continue AndAlso e.StatementType = System.Data.StatementType.Insert Then
            e.Row("ID") = Int32.Parse(ATRDbMasterAdapter.DelivieriesTableAdapter.ScalarQuery.ToString)
            e.Row.AcceptChanges()
            'e.tatus = UpdateStatus.SkipCurrentRow
        End If
    End Sub

    Private Sub HandleInputsRowUpdated(ByVal sender As Object, ByVal e As OleDbRowUpdatedEventArgs)
        If e.Status = System.Data.UpdateStatus.Continue AndAlso e.StatementType = System.Data.StatementType.Insert Then
            e.Row("ID") = Int32.Parse(ATRDbMasterAdapter.InputsTableAdapter.ScalarQuery.ToString)
            e.Row.AcceptChanges()
            'e.tatus = UpdateStatus.SkipCurrentRow
        End If
    End Sub

    Private Sub HandleFixedCostRowUpdated(ByVal sender As Object, ByVal e As OleDbRowUpdatedEventArgs)
        If e.Status = System.Data.UpdateStatus.Continue AndAlso e.StatementType = System.Data.StatementType.Insert Then
            e.Row("ID") = Int32.Parse(ATRDbMasterAdapter.FixedCostTableAdapter.ScalarQuery.ToString)
            e.Row.AcceptChanges()
            'e.tatus = UpdateStatus.SkipCurrentRow
        End If
    End Sub

    Public Function UpdateAllTables(DSet As ATRDbDataSet) As Integer

        Dim i As Integer = 0
        Try
            ATRDbMasterAdapter.ProjectTableAdapter.Fill(DSet.Project)
            ATRDbMasterAdapter.SubProjectTableAdapter.Fill(DSet.SubProject)
            ATRDbMasterAdapter.StageTableAdapter.Fill(DSet.Stage)

            ATRDbMasterAdapter.ATRTableAdapter.Fill(DSet.ATR)

            ATRDbMasterAdapter.RisksTableAdapter.Fill(DSet.Risks)
            ATRDbMasterAdapter.DelivieriesTableAdapter.Fill(DSet.Delivieries)
            ATRDbMasterAdapter.InputsTableAdapter.Fill(DSet.Inputs)
            ATRDbMasterAdapter.ResultsTableAdapter.Fill(DSet.Results)

            ATRDbMasterAdapter.FixedCostTableAdapter.Fill(DSet.FixedCost)
            ATRDbMasterAdapter.AReCatJuncTableAdapter.Fill(DSet.AReCatJunc)

            ATRDbMasterAdapter.ResourceTableAdapter.Fill(DSet.Resource)
            ATRDbMasterAdapter.CatTableAdapter.Fill(DSet.Cat)

            ATRDbMasterAdapter.ATRMemoTableAdapter.Fill(DSet.ATRMemo)

        Catch ex As Exception
            If InStr(ex.Message, "Your network access was interrupted.") > 0 Then
                MsgBox("Probelm med nätverket, inget har sparats i databasen" & Environment.NewLine &
                       "Spara undan skapade texter på annan plats")
            End If
            MakeErrorReport("UpdateAllTables: ", ex)
        End Try
        Return i
    End Function

End Module
