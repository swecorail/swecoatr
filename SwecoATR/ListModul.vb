﻿Imports Microsoft.Office.Interop.Excel
Imports Microsoft.Office.Interop.Word
Imports Microsoft.Office.Interop
Imports System.IO
Imports System
Module ListModul
    Private oExcelApp As Excel.Application
    Private oExcelWBook As Excel.Workbook
    Private oExcelWSheet As Excel.Worksheet
    'Private oExcel As Excel.Application

    Public Sub ListInitExcel()
        Try
            oExcelApp = New Excel.Application
            oExcelApp.Visible = False
            oExcelApp.DisplayAlerts = WdAlertLevel.wdAlertsNone
        Catch ex As Exception
            MakeErrorReport("ListModul InitExcel: Kan inte skapa/hantera object:", ex)
            Throw New ApplicationException("InitExcelError:")
            oExcelApp = Nothing
        End Try
    End Sub

    Public Sub ListOpenExcelSheet(FilePath As String)
        Try
            Dim DirectoryPath As String
            'System.IO.Directory.GetCurrentDirectory
            oExcelWBook = oExcelApp.Workbooks.Add()

            DirectoryPath = System.IO.Path.GetDirectoryName(FilePath)
            If Not System.IO.Directory.Exists(DirectoryPath) Then
                System.IO.Directory.CreateDirectory(DirectoryPath)
            End If

            oExcelWBook.SaveAs(FilePath, , , , True)
            oExcelWSheet = oExcelWBook.ActiveSheet
        Catch ex As Exception
            MakeErrorReport("TRVMAllDocModul: OpenExcelSheet: ", ex)
            oExcelWBook = Nothing
            oExcelWSheet = Nothing
            Runtime.InteropServices.Marshal.FinalReleaseComObject(oExcelApp)
            oExcelApp = Nothing
            Throw New ApplicationException("OpenExcelSheetError")
        End Try
    End Sub

    Public Sub ListCreateTimeResourceExcel(ByRef ListTable As Data.DataTable)
        Dim sortFilter As String = "TimeH DESC"
        Dim ListRows() As DataRow = ListTable.Select(True, sortFilter)
        Dim i As Integer

        oExcelWSheet.Cells(1, 1).value = "Namn"
        oExcelWSheet.Cells(1, 2).value = "Timmar"
        oExcelWSheet.Cells(1, 3).value = "StartDatum"
        oExcelWSheet.Cells(1, 4).value = "SlutDatum"
        oExcelWSheet.Cells(1, 5).value = "Medverkar i följande ATR"
        For i = 0 To ListRows.Length - 1
            oExcelWSheet.Cells(i + 2, 1).value = ListRows(i).Item("Name")
            oExcelWSheet.Cells(i + 2, 2).value = ListRows(i).Item("TimeH")
            oExcelWSheet.Cells(i + 2, 3).value = ListRows(i).Item("StartDate")
            oExcelWSheet.Cells(i + 2, 4).value = ListRows(i).Item("EndDate")
            oExcelWSheet.Cells(i + 2, 5).value = ListRows(i).Item("ATRName")
        Next
    End Sub

    Public Sub ListCreatedeliveryListExcel(ByRef ListTable As Data.DataTable)
        Dim sortFilter As String = "TimeH DESC"
        'Dim ListRows() As DataRow = ListTable.Select(True, sortFilter)
        Dim i As Integer = 0

        oExcelWSheet.Cells(1, 1).value = "ATRnummer"
        oExcelWSheet.Cells(1, 2).value = "Huvuduppdrag"
        oExcelWSheet.Cells(1, 3).value = "Deluppdrag"
        oExcelWSheet.Cells(1, 4).value = "Sweconummer"
        oExcelWSheet.Cells(1, 5).value = "ATRNamn"
        oExcelWSheet.Cells(1, 6).value = "Huvuduppdragsansvarig"
        oExcelWSheet.Cells(1, 7).value = "ATR-bladsansvarig"
        oExcelWSheet.Cells(1, 8).value = "Version"
        oExcelWSheet.Cells(1, 9).value = "Totalt antal timmar"
        oExcelWSheet.Cells(1, 10).value = "TotalKostnad"
        oExcelWSheet.Cells(1, 11).value = "Startdatum"
        oExcelWSheet.Cells(1, 12).value = "Slutdatum"

        For Each Row As DataRow In ListTable.Rows
            oExcelWSheet.Cells(i + 2, 1).value = Row.Item("TRVNumber")
            oExcelWSheet.Cells(i + 2, 2).value = Row.Item("SubProject")
            oExcelWSheet.Cells(i + 2, 3).value = Row.Item("Stage")
            oExcelWSheet.Cells(i + 2, 4).value = Row.Item("ATRNumber")
            oExcelWSheet.Cells(i + 2, 5).value = Row.Item("ATRName")
            oExcelWSheet.Cells(i + 2, 6).value = Row.Item("SubprojectLeader")
            oExcelWSheet.Cells(i + 2, 7).value = Row.Item("ATRLeader")
            oExcelWSheet.Cells(i + 2, 8).value = Row.Item("Version")
            oExcelWSheet.Cells(i + 2, 9).value = Row.Item("TotalHours")
            oExcelWSheet.Cells(i + 2, 10).value = Row.Item("TotalCost")
            oExcelWSheet.Cells(i + 2, 11).value = Row.Item("StartDate")
            oExcelWSheet.Cells(i + 2, 12).value = Row.Item("EndDate")
            i = i + 1
        Next
    End Sub

    Public Sub ListCreateITListExcel(ByRef ListTable As Data.DataTable, ByRef StringList As List(Of String))
        Dim i As Integer
        oExcelWSheet.Cells(1, 1).value = "Företag"
        oExcelWSheet.Cells(1, 2).value = "Resultatenhetsnummer"
        oExcelWSheet.Cells(1, 3).value = "Huvuduppdragsnummer"
        oExcelWSheet.Cells(1, 4).value = "Deluppdragsnummer"
        oExcelWSheet.Cells(1, 5).value = "Deluppdragsnamn"
        oExcelWSheet.Cells(1, 6).value = "Deluppdragsledare"
        oExcelWSheet.Cells(1, 7).value = "Startdatum"
        oExcelWSheet.Cells(1, 8).value = "Spärrdatum "
        oExcelWSheet.Cells(1, 9).value = "Kund"
        oExcelWSheet.Cells(1, 10).value = "Kundens referens"
        oExcelWSheet.Cells(1, 11).value = "Kundens referensnr"
        oExcelWSheet.Cells(1, 12).value = "Faktureringsform"
        oExcelWSheet.Cells(1, 13).value = "Samlingsfakturering"
        oExcelWSheet.Cells(1, 14).value = "Samlingsuppdrag"
        oExcelWSheet.Cells(1, 15).value = "Prissättningmetod"
        oExcelWSheet.Cells(1, 16).value = "prislista tid"
        oExcelWSheet.Cells(1, 17).value = "prislista utlägg"
        oExcelWSheet.Cells(1, 18).value = "uppdragsstatus"

        For Each Row As DataRow In ListTable.Rows
            oExcelWSheet.Cells(i + 2, 1).value = StringList(0)
            oExcelWSheet.Cells(i + 2, 2).value = Row.Item("ProjectNumber")
            oExcelWSheet.Cells(i + 2, 3).value = Row.Item("SubProjectNumber")
            oExcelWSheet.Cells(i + 2, 4).value = Row.Item("ATRNumber")
            oExcelWSheet.Cells(i + 2, 5).value = Row.Item("ATRName")
            oExcelWSheet.Cells(i + 2, 6).value = Row.Item("SubProjectLeader")
            oExcelWSheet.Cells(i + 2, 7).value = Row.Item("StartDate")
            oExcelWSheet.Cells(i + 2, 8).value = Row.Item("EndDate")
            oExcelWSheet.Cells(i + 2, 9).value = StringList(1)
            oExcelWSheet.Cells(i + 2, 10).value = StringList(2)
            oExcelWSheet.Cells(i + 2, 11).value = StringList(3)
            oExcelWSheet.Cells(i + 2, 12).value = StringList(4)
            oExcelWSheet.Cells(i + 2, 13).value = StringList(5)
            oExcelWSheet.Cells(i + 2, 14).value = Row.Item("SubProjectNumber")
            oExcelWSheet.Cells(i + 2, 15).value = StringList(6)
            oExcelWSheet.Cells(i + 2, 16).value = StringList(7)
            oExcelWSheet.Cells(i + 2, 17).value = StringList(8)
            oExcelWSheet.Cells(i + 2, 18).value = StringList(9)

            i = i + 1
        Next
    End Sub

    Public Sub ListCreateTimeCatExcel(ByRef ListTable As Data.DataTable, ByRef CatList As Data.DataTable)
        Dim i As Integer

        oExcelWSheet.Cells(1, 1).value = "Huvuduppdragsnummer"
        oExcelWSheet.Cells(1, 2).value = "Huvuduppdragsnamn"
        For Each CatRow As DataRow In CatList.Rows
            oExcelWSheet.Cells(1, CatRow.Item("ID") + 2).value = CatRow.Item("FreeText")
        Next
        i = 0
        For Each ListRow As DataRow In ListTable.Rows
            oExcelWSheet.Cells(i + 2, 1).value = ListRow.Item("SubProjectNumber")
            oExcelWSheet.Cells(i + 2, 2).value = ListRow.Item("SubProjectName")
            For Each CatRow As DataRow In CatList.Rows
                oExcelWSheet.Cells(i + 2, CatRow.Item("ID") + 2).value = ListRow.Item(CatRow.Item("ID").ToString)
            Next
            i = i + 1
        Next
    End Sub

    Public Sub ListCreateTimeCatATRExcel(ByRef ListTable As Data.DataTable, ByRef CatList As Data.DataTable)
        Dim i As Integer

        oExcelWSheet.Cells(1, 1).value = "Huvuduppdragsnummer"
        oExcelWSheet.Cells(1, 2).value = "Huvuduppdragsnamn"
        oExcelWSheet.Cells(1, 3).value = "ATR ID"
        oExcelWSheet.Cells(1, 4).value = "ATRnamn"
        oExcelWSheet.Cells(1, 5).value = "TRVnummer"

        For Each CatRow As DataRow In CatList.Rows
            oExcelWSheet.Cells(1, CatRow.Item("ID") + 5).value = CatRow.Item("FreeText")
        Next
        i = 0
        For Each ListRow As DataRow In ListTable.Rows
            oExcelWSheet.Cells(i + 2, 1).value = ListRow.Item("SubProjectNumber")
            oExcelWSheet.Cells(i + 2, 2).value = ListRow.Item("SubProjectName")
            oExcelWSheet.Cells(i + 2, 3).value = ListRow.Item("ATRID")
            oExcelWSheet.Cells(i + 2, 4).value = ListRow.Item("ATRName")
            oExcelWSheet.Cells(i + 2, 5).value = ListRow.Item("ATR_TRV")

            For Each CatRow As DataRow In CatList.Rows
                oExcelWSheet.Cells(i + 2, CatRow.Item("ID") + 5).value = ListRow.Item(CatRow.Item("ID").ToString)
            Next
            i = i + 1
        Next
    End Sub

    Public Sub ListCreateATRResultExcel(ByRef ListTable As Data.DataTable)
        Dim i As Integer = 0
        oExcelWSheet.Cells(1, 1).value = "Huvuduppdragsnummer "
        oExcelWSheet.Cells(1, 2).value = "Huvuduppdragsnamn"
        oExcelWSheet.Cells(1, 3).value = "Huvuduppdragsansvarig"
        oExcelWSheet.Cells(1, 4).value = "TRV nummer "
        oExcelWSheet.Cells(1, 5).value = "SwecoATRnummer"
        oExcelWSheet.Cells(1, 6).value = "ATRnamn"
        oExcelWSheet.Cells(1, 7).value = "ATRansvarig"
        oExcelWSheet.Cells(1, 8).value = "SlutDatum"
        oExcelWSheet.Cells(1, 9).value = "Resultat"
        oExcelWSheet.Cells(1, 10).value = "Leveranser"

        For Each Row As DataRow In ListTable.Rows
            oExcelWSheet.Cells(i + 2, 1).value = Row.Item("MainNumber")
            oExcelWSheet.Cells(i + 2, 2).value = Row.Item("MainName")
            oExcelWSheet.Cells(i + 2, 3).value = Row.Item("MainLeader")
            oExcelWSheet.Cells(i + 2, 4).value = Row.Item("TRVNumber")
            oExcelWSheet.Cells(i + 2, 5).value = Row.Item("ATRNumber")
            oExcelWSheet.Cells(i + 2, 6).value = Row.Item("ATRName")
            oExcelWSheet.Cells(i + 2, 7).value = Row.Item("ATRLeader")
            oExcelWSheet.Cells(i + 2, 8).value = Row.Item("EndDate")
            oExcelWSheet.Cells(i + 2, 9).value = Row.Item("Result")
            oExcelWSheet.Cells(i + 2, 10).value = Row.Item("Deliviries")
            i = i + 1
        Next
    End Sub

    Public Sub ListCreateResursListExcel(ByRef ListTable As Data.DataTable)
        Dim i As Integer = 0
        oExcelWSheet.Cells(1, 1).value = "SwecoID"
        oExcelWSheet.Cells(1, 2).value = "Namn"
        oExcelWSheet.Cells(1, 3).value = "Kategori"
        For Each Row As DataRow In ListTable.Rows
            oExcelWSheet.Cells(i + 2, 1).value = Row.Item("ID")
            oExcelWSheet.Cells(i + 2, 2).value = Row.Item("FreeText")
            oExcelWSheet.Cells(i + 2, 3).value = Row.Item("CatID")
            i = i + 1
        Next
    End Sub

    Public Sub ListCreateRiskListExcel(ByRef ListTable As Data.DataTable)
        Dim i As Integer = 0
        oExcelWSheet.Cells(1, 1).value = "Huvuduppdagsnummer"
        oExcelWSheet.Cells(1, 2).value = "Huvuduppdragsnamn"
        oExcelWSheet.Cells(1, 3).value = "ATRnummer"
        oExcelWSheet.Cells(1, 4).value = "Sweconummer"
        oExcelWSheet.Cells(1, 5).value = "ATRNamn"
        oExcelWSheet.Cells(1, 6).value = "Huvuduppdragsansvarig"
        oExcelWSheet.Cells(1, 7).value = "ATR-bladsansvarig"
        oExcelWSheet.Cells(1, 8).value = "Risk"
        oExcelWSheet.Cells(1, 9).value = "Riskhantering"
        For Each Row As DataRow In ListTable.Rows
            oExcelWSheet.Cells(i + 2, 1).value = Row.Item("MainNumber")
            oExcelWSheet.Cells(i + 2, 2).value = Row.Item("MainName")
            oExcelWSheet.Cells(i + 2, 3).value = Row.Item("ATRNumber")
            oExcelWSheet.Cells(i + 2, 4).value = Row.Item("SwecoNumber")
            oExcelWSheet.Cells(i + 2, 5).value = Row.Item("ATRName")
            oExcelWSheet.Cells(i + 2, 6).value = Row.Item("Leader")
            oExcelWSheet.Cells(i + 2, 7).value = Row.Item("ATRLeader")
            oExcelWSheet.Cells(i + 2, 8).value = Row.Item("Freetext1")
            oExcelWSheet.Cells(i + 2, 9).value = Row.Item("Freetext2")
            i = i + 1
        Next
    End Sub

    Public Sub ListCreatePrimaveraListExcel(ByRef ListTable As Data.DataTable)
        Dim i As Integer = 0
        oExcelWSheet.Cells(1, 1).value = "Huvuduppdragsnamn"
        oExcelWSheet.Cells(1, 2).value = "Huvuduppdragsansvarig"
        oExcelWSheet.Cells(1, 3).value = "deluppdragsnamn"
        oExcelWSheet.Cells(1, 4).value = "ATRnummer"
        oExcelWSheet.Cells(1, 5).value = "Sweconummer"
        oExcelWSheet.Cells(1, 6).value = "ATRNamn"
        oExcelWSheet.Cells(1, 7).value = "ATR-bladsansvarig"
        oExcelWSheet.Cells(1, 8).value = "Version"
        oExcelWSheet.Cells(1, 9).value = "Totalt antal timmar"
        oExcelWSheet.Cells(1, 10).value = "Startdatum"
        oExcelWSheet.Cells(1, 11).value = "Slutdatum"

        For Each Row As DataRow In ListTable.Rows
            oExcelWSheet.Cells(i + 2, 1).value = Row.Item("SubProject")
            oExcelWSheet.Cells(i + 2, 2).value = Row.Item("SubprojectLeader")
            oExcelWSheet.Cells(i + 2, 3).value = Row.Item("Stage")
            oExcelWSheet.Cells(i + 2, 4).value = Row.Item("TRVNumber")
            oExcelWSheet.Cells(i + 2, 5).value = Row.Item("ATRNumber")
            oExcelWSheet.Cells(i + 2, 6).value = Row.Item("ATRName")
            oExcelWSheet.Cells(i + 2, 7).value = Row.Item("ATRLeader")
            oExcelWSheet.Cells(i + 2, 8).value = Row.Item("Version")
            oExcelWSheet.Cells(i + 2, 9).value = Row.Item("TotalHours")
            oExcelWSheet.Cells(i + 2, 10).value = Row.Item("StartDate")
            oExcelWSheet.Cells(i + 2, 11).value = Row.Item("EndDate")
            i = i + 1
        Next
    End Sub

    Public Sub ListCreatePrimaveraSubTableList(ByRef ListTable As Data.DataTable)
        Dim i As Integer = 0
        oExcelWSheet.Cells(1, 1).value = "ID"
        oExcelWSheet.Cells(1, 2).value = "ATR nummer"
        oExcelWSheet.Cells(1, 3).value = "Förutsättning/Leverans"
        oExcelWSheet.Cells(1, 4).value = "Levererat från/ Mottagare"

        For Each Row As DataRow In ListTable.Rows
            oExcelWSheet.Cells(i + 2, 1).value = Row.Item("ID")
            oExcelWSheet.Cells(i + 2, 2).value = Row.Item("FK_ID")
            oExcelWSheet.Cells(i + 2, 3).value = Row.Item("Freetext1")
            oExcelWSheet.Cells(i + 2, 4).value = Row.Item("Freetext2")
            i = i + 1
        Next
    End Sub

    Public Sub ListExitWorkBook()
        Try
            oExcelWBook.Save()
            oExcelWBook.Close()
            oExcelWSheet = Nothing
            oExcelWBook = Nothing
        Catch ex As Exception
            MakeErrorReport("ExitWorkBook: ", ex)
            oExcelWBook = Nothing
            oExcelWSheet = Nothing
            Runtime.InteropServices.Marshal.FinalReleaseComObject(oExcelApp)
            oExcelApp = Nothing
            Throw New ApplicationException("ExitWorkBookError")
        End Try
    End Sub

    Public Sub ListExitExcel()
        Try
            oExcelApp.Quit()
            Runtime.InteropServices.Marshal.FinalReleaseComObject(oExcelApp)
            oExcelApp = Nothing
        Catch ex As Exception
            MakeErrorReport("ExitExcel", ex)
            oExcelApp = Nothing
            Throw New ApplicationException("ExitExcelError")
        End Try
    End Sub



End Module
