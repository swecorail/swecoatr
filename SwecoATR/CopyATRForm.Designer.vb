﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class CopyATRForm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.OrginalATRNumber = New System.Windows.Forms.Label()
        Me.OrginalATRName = New System.Windows.Forms.Label()
        Me.NewATRNumber = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.NewATRName = New System.Windows.Forms.TextBox()
        Me.SaveCopyATRButton = New System.Windows.Forms.Button()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.NewProjectNr = New System.Windows.Forms.ComboBox()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.GenATRNumber = New System.Windows.Forms.TextBox()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.StageName = New System.Windows.Forms.ComboBox()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.NewSubProjectNr = New System.Windows.Forms.ComboBox()
        Me.NewSubprojectName = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox3.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(22, 23)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(89, 17)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Kopiera ATR"
        '
        'OrginalATRNumber
        '
        Me.OrginalATRNumber.AutoSize = True
        Me.OrginalATRNumber.Location = New System.Drawing.Point(22, 53)
        Me.OrginalATRNumber.Name = "OrginalATRNumber"
        Me.OrginalATRNumber.Size = New System.Drawing.Size(88, 17)
        Me.OrginalATRNumber.TabIndex = 1
        Me.OrginalATRNumber.Text = "7894561230"
        '
        'OrginalATRName
        '
        Me.OrginalATRName.AutoSize = True
        Me.OrginalATRName.Location = New System.Drawing.Point(135, 53)
        Me.OrginalATRName.Name = "OrginalATRName"
        Me.OrginalATRName.Size = New System.Drawing.Size(88, 17)
        Me.OrginalATRName.TabIndex = 2
        Me.OrginalATRName.Text = "7894561230"
        '
        'NewATRNumber
        '
        Me.NewATRNumber.Location = New System.Drawing.Point(615, 102)
        Me.NewATRNumber.MaxLength = 10
        Me.NewATRNumber.Name = "NewATRNumber"
        Me.NewATRNumber.Size = New System.Drawing.Size(100, 22)
        Me.NewATRNumber.TabIndex = 3
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(612, 82)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(148, 17)
        Me.Label2.TabIndex = 4
        Me.Label2.Text = "Nytt ATR-bladnummer"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(612, 144)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(132, 17)
        Me.Label3.TabIndex = 6
        Me.Label3.Text = "Nytt ATR-bladnamn"
        '
        'NewATRName
        '
        Me.NewATRName.Location = New System.Drawing.Point(615, 164)
        Me.NewATRName.MaxLength = 30
        Me.NewATRName.Name = "NewATRName"
        Me.NewATRName.Size = New System.Drawing.Size(226, 22)
        Me.NewATRName.TabIndex = 5
        '
        'SaveCopyATRButton
        '
        Me.SaveCopyATRButton.Location = New System.Drawing.Point(727, 205)
        Me.SaveCopyATRButton.Name = "SaveCopyATRButton"
        Me.SaveCopyATRButton.Size = New System.Drawing.Size(114, 36)
        Me.SaveCopyATRButton.TabIndex = 7
        Me.SaveCopyATRButton.Text = "Spara"
        Me.SaveCopyATRButton.UseVisualStyleBackColor = True
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.Label5)
        Me.GroupBox1.Controls.Add(Me.NewProjectNr)
        Me.GroupBox1.Controls.Add(Me.Label10)
        Me.GroupBox1.Controls.Add(Me.SaveCopyATRButton)
        Me.GroupBox1.Controls.Add(Me.GenATRNumber)
        Me.GroupBox1.Controls.Add(Me.GroupBox3)
        Me.GroupBox1.Controls.Add(Me.GroupBox2)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Controls.Add(Me.Label3)
        Me.GroupBox1.Controls.Add(Me.NewATRNumber)
        Me.GroupBox1.Controls.Add(Me.NewATRName)
        Me.GroupBox1.Location = New System.Drawing.Point(25, 91)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(882, 313)
        Me.GroupBox1.TabIndex = 8
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Nytt ATR-blad"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(13, 64)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(112, 17)
        Me.Label5.TabIndex = 14
        Me.Label5.Text = "Ny resultatenhet"
        '
        'NewProjectNr
        '
        Me.NewProjectNr.FormattingEnabled = True
        Me.NewProjectNr.Location = New System.Drawing.Point(16, 90)
        Me.NewProjectNr.Name = "NewProjectNr"
        Me.NewProjectNr.Size = New System.Drawing.Size(228, 24)
        Me.NewProjectNr.TabIndex = 13
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(612, 25)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(217, 17)
        Me.Label10.TabIndex = 12
        Me.Label10.Text = "Förslag på nytt ATR-bladnummer"
        '
        'GenATRNumber
        '
        Me.GenATRNumber.Location = New System.Drawing.Point(615, 45)
        Me.GenATRNumber.MaxLength = 10
        Me.GenATRNumber.Name = "GenATRNumber"
        Me.GenATRNumber.ReadOnly = True
        Me.GenATRNumber.Size = New System.Drawing.Size(100, 22)
        Me.GenATRNumber.TabIndex = 11
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.StageName)
        Me.GroupBox3.Controls.Add(Me.Label9)
        Me.GroupBox3.Location = New System.Drawing.Point(264, 192)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(290, 99)
        Me.GroupBox3.TabIndex = 10
        Me.GroupBox3.TabStop = False
        Me.GroupBox3.Text = "Deluppdragsinformation"
        '
        'StageName
        '
        Me.StageName.FormattingEnabled = True
        Me.StageName.Location = New System.Drawing.Point(11, 59)
        Me.StageName.Name = "StageName"
        Me.StageName.Size = New System.Drawing.Size(210, 24)
        Me.StageName.TabIndex = 7
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(6, 39)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(151, 17)
        Me.Label9.TabIndex = 5
        Me.Label9.Text = "Nytt deluppdragsnamn"
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.NewSubProjectNr)
        Me.GroupBox2.Controls.Add(Me.NewSubprojectName)
        Me.GroupBox2.Controls.Add(Me.Label7)
        Me.GroupBox2.Controls.Add(Me.Label4)
        Me.GroupBox2.Location = New System.Drawing.Point(264, 25)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(290, 161)
        Me.GroupBox2.TabIndex = 9
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Huvuduppdragsinformation"
        '
        'NewSubProjectNr
        '
        Me.NewSubProjectNr.FormattingEnabled = True
        Me.NewSubProjectNr.Location = New System.Drawing.Point(9, 67)
        Me.NewSubProjectNr.Name = "NewSubProjectNr"
        Me.NewSubProjectNr.Size = New System.Drawing.Size(212, 24)
        Me.NewSubProjectNr.TabIndex = 11
        '
        'NewSubprojectName
        '
        Me.NewSubprojectName.AutoSize = True
        Me.NewSubprojectName.Location = New System.Drawing.Point(6, 131)
        Me.NewSubprojectName.Name = "NewSubprojectName"
        Me.NewSubprojectName.Size = New System.Drawing.Size(189, 17)
        Me.NewSubprojectName.TabIndex = 8
        Me.NewSubprojectName.Text = "Nytt Huvuduppdragsnummer"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(6, 100)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(171, 17)
        Me.Label7.TabIndex = 7
        Me.Label7.Text = "Nytt huvuduppdragsnamn"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(8, 39)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(187, 17)
        Me.Label4.TabIndex = 5
        Me.Label4.Text = "Nytt huvuduppdragsnummer"
        '
        'CopyATRForm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(919, 416)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.OrginalATRName)
        Me.Controls.Add(Me.OrginalATRNumber)
        Me.Controls.Add(Me.Label1)
        Me.Name = "CopyATRForm"
        Me.Text = "Kopiera ATR"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox3.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents OrginalATRNumber As System.Windows.Forms.Label
    Friend WithEvents OrginalATRName As System.Windows.Forms.Label
    Friend WithEvents NewATRNumber As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents NewATRName As System.Windows.Forms.TextBox
    Friend WithEvents SaveCopyATRButton As System.Windows.Forms.Button
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents NewSubprojectName As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents GenATRNumber As System.Windows.Forms.TextBox
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents NewSubProjectNr As System.Windows.Forms.ComboBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents NewProjectNr As System.Windows.Forms.ComboBox
    Friend WithEvents StageName As System.Windows.Forms.ComboBox
End Class
