﻿Public Class ResourceSelectForm

    Private Sub ResourceSelectForm_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Try
            'Dim Cell As DataGridViewCell = New DataGridViewTextBoxCell()

            ResourceSelectGrid.DataSource = MainForm.ResourceSelectBindingSrc
            ResourceSelectGrid.Columns("ID").HeaderText = "SwecoID"

            ResourceSelectGrid.Columns("ID").Width = 110
            ResourceSelectGrid.Columns("FreeText").HeaderText = "Namn"
            ResourceSelectGrid.Columns("FreeText").Width = 400
            ResourceSelectGrid.Columns("CatID").HeaderText = "Kategori"
            ResourceSelectGrid.Columns("CatID").Width = 100
            ResourceSelectGrid.Columns("FK_ID").Visible = False

            MainForm.ResourceSelectView.RowFilter = "FreeText = '" & MainForm.SwecoName & "'"

            'ResourceSelectGrid.Rows.Add(MainForm.TempSet.Tables("Resource").Rows.Count)
            'For i = 0 To MainForm.TempSet.Tables("Resource").Rows.Count - 1

            'Next
            MainForm.SwecoName = ""
            MainForm.SwecoID = ""
            MainForm.ShowWindow = False
        Catch ex As Exception
            MakeErrorReport("ResourceSelectForm_Load", ex)
        End Try
        

    End Sub

    Private Sub LeaveSelect_Click(sender As Object, e As EventArgs) Handles LeaveSelect.Click
        Dim ATRResourceRow As DataGridViewRow = MainForm.ATRResourcesGrid.CurrentRow
        Dim CatRow As DataRow
        Dim SelectedRow As DataGridViewRow = ResourceSelectGrid.CurrentRow
        CatRow = MainForm.DataSet.Tables("Cat").Rows.Find(SelectedRow.Cells("CatID").Value)
        ATRResourceRow.Cells("FK_CatID").Value = SelectedRow.Cells("CatID").Value
        ATRResourceRow.Cells("FK_RID").Value = SelectedRow.Cells("ID").Value
        ATRResourceRow.Cells("Calc").Value = 0 'CInt(ATRResourceRow.Cells("TimeH").Value) * CatRow.Item("Cost")
        ATRResourceRow.Cells("CatName").Value = CatRow.Item("FreeText") & " (" & CatRow.Item("Cost") & " kr)"
        ATRResourceRow.Cells("FK_ATRID").Value = MainForm.ATRnumber.Text
        MainForm.RowIndex = ATRResourceRow.Index
        Me.Close()
    End Sub
End Class