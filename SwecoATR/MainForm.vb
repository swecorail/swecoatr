﻿'Imports SwecoRail_Trafikstyrning.TRVMallDoc
Imports System.IO
Imports System


Public Class MainForm
#Region "Variable deklarion"
    Public FilePath As String
    Public DataSet As DataSet

    Public ResourceSelectBindingSrc As New BindingSource()
    Public ResourceSelectView As New DataView
    Public SwecoID As String
    Public SwecoName As String
    Public ShowWindow As Boolean = True


    Public ResourceSelectRowIndex As Integer()


    Private ProjectBindingSrc As New BindingSource()
    Private ResourceBindingSrc As New BindingSource()
    Private CatBindingSrc As New BindingSource()
    Private SubProjectBindingSrc As New BindingSource()
    Private StageBindingSrc As New BindingSource()
    Private ATRBindingSrc As New BindingSource()

    Private ARTMemoBindinSrc As New BindingSource()

    Private FixedCostBindingSrc As New BindingSource()
    Private ResultsBindingSrc As New BindingSource()
    Private ATRReCatJuncBindinSrc As New BindingSource()

    Private ATRResourceView As New DataView
    Private ProjectTableView As New DataView
    Private SubTableMainView As New DataView ' Används för att Hitta alla huvuduppdrag på Resultatenheten
    Private SubTableSubView As New DataView ' Används för att uppdatera alla fält som tillhör huvuduppdrag
    Private StageTableMainView As New DataView
    Private StageTableSubView As New DataView
    Private CatTableView As New DataView
    Private ResourceTableView As New DataView
    Private ATRTableMainView As New DataView
    Private ATRTableSubView As New DataView
    Private InputTableView As New DataView
    Private ResultTableView As New DataView
    Private DelivieriesTableView As New DataView
    Private RisksTableView As New DataView
    Private AReCatJuncTableView As New DataView
    Private FixedCostTableView As New DataView
    Private ATRMemoTableView As New DataView

    Private StageID As Integer
    Private StageNumber As Integer

    Public RowIndex As Integer = -1 ' en bugg fix då ATRCatJunc datagrid inte funkar som den är tänkt


    Private StageTablePos As Integer = 0

    Private StageSubNr As Integer = 0
    Public SuperUser As Boolean = False
    Private StageNumberList As New List(Of Integer)
    Private WBSLink As String = "C:\"
    Private HelpLink As String = "C:\"
    Private ITInfoFromFile As New List(Of String)
    Private FileNameType As String = ""
#End Region

    Private Sub Form1_Load(sender As Object, e As EventArgs) Handles MyBase.Load

    End Sub

    Private Sub NewProjectToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles NewProjectToolStripMenuItem.Click
        Try
            CreateProject.Show()
        Catch ex As Exception
            MakeErrorReport("NewprojektToolStripMenuItem: ", ex)
        End Try
    End Sub

    Private Sub OpenToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles OpenToolStripMenuItem.Click
        Try
            If DBconnectionExit() = True Then
                MsgBox("Projekt är redan öppnat")
                'CloseDB()
            Else
                'OpenFileDialog1.Filter = "Database files (*.accdb)|*.accdb"
                OpenFileDialog1.ShowDialog()
                If StrComp(OpenFileDialog1.FileName, "") = 0 Then
                    Throw New ApplicationException("Filen finns inte")
                End If
                OpenDb(OpenFileDialog1.FileName)
                Basicfunctions.MainFolder = System.IO.Path.GetDirectoryName(OpenFileDialog1.FileName)
                WordFolderLabel.Text = Basicfunctions.MainFolder
                ReadSetupFile(Basicfunctions.MainFolder)
                SuperUserSetup()
                DataSet = ConnectDataSet()
                BindControls()
                ProjectBindingSrc.MoveFirst()
                SubProjectBindingSrc.MoveFirst()
                SubprojectNumber.SelectedIndex = 0


            End If

        Catch ex As Exception
            MakeErrorReport("OpenToolStripMenuItem_Click: ", ex)
            MsgBox("Fel vid öppnadet av projektet")
        End Try
    End Sub

    Public Sub BindControls()
        Try
            Dim Cell As DataGridViewCell = New DataGridViewTextBoxCell()

            ATRResourceView.Table = DataSet.Tables("AReCatJunc")
            '*********  Project tabellen *********'
            ProjectTableView.Table = DataSet.Tables("Project")

            ProjectBindingSrc.DataSource = ProjectTableView


            ProjectName.DataBindings.Add(New Binding("Text",
                                                     ProjectBindingSrc, "ProjectName", True))

            ProjectNumber.DataSource = DataSet.Tables("Project")
            ProjectNumber.DisplayMember = "ID"
            ProjectNumber.FormattingEnabled = True

            ProjectOwner.DataBindings.Add(New Binding("Text",
                                                      ProjectBindingSrc, "ProjectOwner", True))
            CustomerNumber.DataBindings.Add(New Binding("Text",
                                                        ProjectBindingSrc, "CustomerNr", True))
            Projectleader.DataBindings.Add(New Binding("Text",
                                                       ProjectBindingSrc, "Leader", True))
            '*********  SubProject tabellen *********'

            SubTableMainView.Table = DataSet.Tables("SubProject")
            SubTableSubView.Table = DataSet.Tables("SubProject")
            SubProjectBindingSrc.DataSource = SubTableSubView
            SubprojectNumber.DataSource = SubTableMainView
            SubprojectNumber.DisplayMember = "ID"
            SubprojectNumber.FormattingEnabled = True

            SubProjectName.DataBindings.Add(New Binding("Text",
                                                        SubProjectBindingSrc, "TableName", True))
            SubprojectFK.DataBindings.Add(New Binding("Text",
                                                      SubProjectBindingSrc, "FK_ID", True))
            ' Man kan inte databinda en textbox som inte syns, men man kan ändra visible efter bindning
            SubprojectFK.Visible = False
            SubProjectLeader.DataBindings.Add(New Binding("Text",
                                                          SubProjectBindingSrc, "Leader", True))
            '*********  Stage Tabellen *********'
            StageTableMainView.Table = DataSet.Tables("Stage")
            StageTableSubView.Table = DataSet.Tables("Stage")
            StageBindingSrc.DataSource = StageTableSubView
            StageComboBox.DataSource = StageTableMainView
            StageComboBox.DisplayMember = "IndexName"
            StageComboBox.FormattingEnabled = True
            'StageListBox.DataBindings.Add(New Binding("SelectedIndex", _
            '                                       StageBindingSrc, "TableName", True))
            StageLeader.DataBindings.Add(New Binding("Text",
                                                     StageBindingSrc, "Leader", True))
            StageFK.DataBindings.Add(New Binding("Text",
                                                     StageBindingSrc, "FK_ID", True))
            StageFK.Visible = False

            '*********  Resource Tabellen *********'
            ResourceTableView.Table = DataSet.Tables("Resource")
            ResourceBindingSrc.DataSource = ResourceTableView
            ResourceGrid.DataSource = ResourceBindingSrc
            ResourceGrid.Columns("ID").HeaderText = "SwecoID"
            ResourceGrid.Columns("ID").Width = 110
            ResourceGrid.Columns("FreeText").HeaderText = "Namn"
            ResourceGrid.Columns("FreeText").Width = 200
            ResourceGrid.Columns("CatID").HeaderText = "Kategori"
            ResourceGrid.Columns("CatID").Width = 80
            ResourceGrid.Columns("RoleText").HeaderText = "RollBeskrivning"
            ResourceGrid.Columns("RoleText").Width = 200
            ResourceGrid.Columns("FK_ID").Visible = False
            ResourceGrid.Columns("FK_ID").ValueType = GetType(Integer)

            '*********  Cat Tabellen *********'
            CatTableView.Table = DataSet.Tables("Cat")
            CatBindingSrc.DataSource = CatTableView
            CatGrid.DataSource = CatBindingSrc
            CatGrid.Columns("ID").HeaderText = "Kategori"
            CatGrid.Columns("ID").Width = 50
            CatGrid.Columns("FK_ID").Visible = False
            CatGrid.Columns("FK_ID").ValueType = GetType(Integer)
            CatGrid.Columns("FreeText").HeaderText = "Priskategori"
            CatGrid.Columns("FreeText").Width = 300
            CatGrid.Columns("Cost").HeaderText = "Fakturerat pris"
            CatGrid.Columns("Cost").Width = 125


            '*********  ATR Tabellen *********'
            ATRTableMainView.Table = DataSet.Tables("ATR")
            ATRTableSubView.Table = DataSet.Tables("ATR")
            ATRBindingSrc.DataSource = ATRTableSubView
            ATRnumber.DataSource = ATRTableMainView
            ATRnumber.DisplayMember = "ID"
            ATRnumber.FormattingEnabled = True

            ATRName.DataBindings.Add(New Binding("Text",
                                                    ATRBindingSrc, "ATRName", True))
            ATRLeader.DataBindings.Add(New Binding("Text",
                                                    ATRBindingSrc, "Leader", True))
            ATRScope.DataBindings.Add(New Binding("Text",
                                                    ATRBindingSrc, "Scope", True))
            StartDate.DataBindings.Add(New Binding("Value",
                                                    ATRBindingSrc, "StartDate", True))
            EndDate.DataBindings.Add(New Binding("Value",
                                                    ATRBindingSrc, "EndDate", True))
            ATRVersion.DataBindings.Add(New Binding("Text",
                                                    ATRBindingSrc, "Version", True))
            ATRrevDate.DataBindings.Add(New Binding("Value",
                                                    ATRBindingSrc, "RevDate", True))
            ATRRevSign.DataBindings.Add(New Binding("Text",
                                                    ATRBindingSrc, "RevSign", True))
            TRVNameBox.DataBindings.Add(New Binding("Text",
                                                    ATRBindingSrc, "TRVNumber", True))

            ATR_FK.DataBindings.Add(New Binding("Text",
                                                    ATRBindingSrc, "FK_ID", True))
            ATR_FK.Visible = False

            '*********  Inputs Tabellen *********'
            InputTableView.Table = DataSet.Tables("Inputs")
            ATRInputGrid.DataSource = InputTableView
            ATRInputGrid.Columns("ID").Width = 0

            ATRInputGrid.Columns("ID").DisplayIndex = 3
            ATRInputGrid.Columns("FreeText1").DisplayIndex = 1
            ATRInputGrid.Columns("FreeText2").DisplayIndex = 2
            ATRInputGrid.Columns("FK_ID").DisplayIndex = 4
            ATRInputGrid.Columns("FK_ID").Visible = False

            ATRInputGrid.Columns("FreeText1").Width = 510
            ATRInputGrid.Columns("FreeText1").HeaderText = "Förutsättning"
            ATRInputGrid.Columns("FreeText2").Width = 510
            ATRInputGrid.Columns("FreeText2").HeaderText = "Leverat från"
            ATRInputGrid.Columns("FreeText3").Visible = False

            '*********  Results Tabellen *********'
            ResultTableView.Table = DataSet.Tables("Results")
            ResultsBindingSrc.DataSource = ResultTableView
            ResultText.DataBindings.Add(New Binding("Text",
                                                  ResultsBindingSrc, "FreeText1", True))

            '*********  Delivieries Tabellen *********'
            DelivieriesTableView.Table = DataSet.Tables("Delivieries")
            ATRDeliveriesGrid.DataSource = DelivieriesTableView
            ATRDeliveriesGrid.Columns("ID").Width = 0

            ATRDeliveriesGrid.Columns("ID").DisplayIndex = 3
            ATRDeliveriesGrid.Columns("FreeText1").DisplayIndex = 1
            ATRDeliveriesGrid.Columns("FreeText2").DisplayIndex = 2
            ATRDeliveriesGrid.Columns("FK_ID").DisplayIndex = 4
            ATRDeliveriesGrid.Columns("FK_ID").Visible = False

            ATRDeliveriesGrid.Columns("FreeText1").Width = 510
            ATRDeliveriesGrid.Columns("FreeText1").HeaderText = "Leverans"
            ATRDeliveriesGrid.Columns("FreeText2").Width = 510
            ATRDeliveriesGrid.Columns("FreeText2").HeaderText = "Mottagare"
            ATRDeliveriesGrid.Columns("FreeText3").Visible = False

            '*********  Risks Tabellen *********'
            RisksTableView.Table = DataSet.Tables("Risks")
            ATRRisksGrid.DataSource = RisksTableView
            ATRRisksGrid.Columns("ID").Width = 0

            ATRRisksGrid.Columns("ID").DisplayIndex = 3
            ATRRisksGrid.Columns("FreeText1").DisplayIndex = 1
            ATRRisksGrid.Columns("FreeText2").DisplayIndex = 2
            ATRRisksGrid.Columns("FK_ID").DisplayIndex = 4
            ATRRisksGrid.Columns("FK_ID").Visible = False

            ATRRisksGrid.Columns("FreeText1").Width = 510
            ATRRisksGrid.Columns("FreeText1").HeaderText = "Risk"
            ATRRisksGrid.Columns("FreeText2").Width = 510
            ATRRisksGrid.Columns("FreeText2").HeaderText = "Riskhantering"
            ATRRisksGrid.Columns("FreeText3").Visible = False

            '*********  AReCatJunc Tabellen *********'
            'Dim ParentColumn1 As DataColumn
            'Dim ParentColumn2 As DataColumn
            'Dim ParentColumns As DataColumn()

            'Dim ChildColumn1 As DataColumn
            'Dim ChildColumn2 As DataColumn
            'Dim ChildColumns As DataColumn()


            'ParentColumn1 = DataSet.Tables("Resource").Columns("ID")
            'ParentColumn2 = DataSet.Tables("Resource").Columns("CatID")

            'ChildColumn1 = DataSet.Tables("AReCatJunc").Columns("FK_RID")
            'ChildColumn2 = DataSet.Tables("AReCatJunc").Columns("FK_CatID")

            'ParentColumns = New DataColumn() {ParentColumn1, ParentColumn2}
            'ChildColumns = New DataColumn() {ChildColumn1, ChildColumn2}

            'Dim DataRelation = New DataRelation("ATRResourceResource", ParentColumns, ChildColumns)
            'DataSet.Relations.Add(DataRelation)

            AReCatJuncTableView.Table = DataSet.Tables("AReCatJunc")
            ATRReCatJuncBindinSrc.DataSource = AReCatJuncTableView

            'ATRResourcesGrid.DataSource = DataSet.DefaultViewManager
            'ATRResourcesGrid.DataMember = "Resource; AReCatJunc"
            'ATRResourcesGrid.AutoGenerateColumns = True
            ATRResourcesGrid.DataSource = AReCatJuncTableView

            Dim ATRResourceGridNameColumn As DataGridViewColumn = New DataGridViewColumn()
            ATRResourceGridNameColumn.HeaderText = "Namn"
            ATRResourceGridNameColumn.CellTemplate = Cell
            ATRResourceGridNameColumn.Width = 250
            ATRResourceGridNameColumn.Name = "Name"
            ATRResourcesGrid.Columns.Add(ATRResourceGridNameColumn)
            ATRResourcesGrid.Columns("Name").DisplayIndex = 0

            ATRResourcesGrid.Columns("TimeH").DisplayIndex = 1
            ATRResourcesGrid.Columns("TimeH").HeaderText = "Timmar"
            ATRResourcesGrid.Columns("TimeH").Width = 100

            Dim ATRResourceGridCatNameColumn As DataGridViewColumn = New DataGridViewColumn()
            ATRResourceGridCatNameColumn.HeaderText = "Rollbeskrivning /Timkostnad"
            ATRResourceGridCatNameColumn.CellTemplate = Cell
            ATRResourceGridCatNameColumn.Width = 400
            ATRResourceGridCatNameColumn.Name = "CatName"
            ATRResourcesGrid.Columns.Add(ATRResourceGridCatNameColumn)
            ATRResourcesGrid.Columns("CatName").DisplayIndex = 2

            Dim NewColumn As DataGridViewColumn = New DataGridViewColumn()
            NewColumn.HeaderText = "Kostnad för konsult"
            NewColumn.CellTemplate = Cell
            NewColumn.Width = 100
            NewColumn.Name = "Calc"
            ATRResourcesGrid.Columns.Add(NewColumn)
            ATRResourcesGrid.Columns("Calc").DisplayIndex = 3

            ATRResourcesGrid.Columns("FK_RID").DisplayIndex = 4
            ATRResourcesGrid.Columns("FK_RID").HeaderText = "SwecoID"
            ATRResourcesGrid.Columns("FK_RID").Width = 80

            ATRResourcesGrid.Columns("FK_ATRID").DisplayIndex = 5
            ATRResourcesGrid.Columns("FK_ATRID").HeaderText = "ATR_ID"
            ATRResourcesGrid.Columns("FK_ATRID").Width = 80
            ATRResourcesGrid.Columns("FK_ATRID").Visible = False

            ATRResourcesGrid.Columns("FK_CatID").DisplayIndex = 6
            ATRResourcesGrid.Columns("FK_CatID").HeaderText = "Kategori"
            ATRResourcesGrid.Columns("FK_CatID").Width = 80
            ATRResourcesGrid.Columns("FK_CatID").Visible = True

            ATRResourcesGrid.Columns("Roletext").DisplayIndex = 7
            ATRResourcesGrid.Columns("Roletext").HeaderText = "ATR specifik roll"
            ATRResourcesGrid.Columns("Roletext").Width = 80
            ATRResourcesGrid.Columns("Roletext").Visible = False

            '*********  FixedCost Tabellen *********'
            FixedCostTableView.Table = DataSet.Tables("FixedCost")
            FixedCostsGrid.DataSource = FixedCostTableView
            FixedCostsGrid.Columns("FK_ID").DisplayIndex = 2
            FixedCostsGrid.Columns("FK_ID").Visible = False
            FixedCostsGrid.Columns("ID").DisplayIndex = 3
            FixedCostsGrid.Columns("ID").Visible = False
            FixedCostsGrid.Columns("FreeText").DisplayIndex = 0
            FixedCostsGrid.Columns("FreeText").Width = 750
            FixedCostsGrid.Columns("FreeText").HeaderText = "Kostnadstext"
            FixedCostsGrid.Columns("Cost").DisplayIndex = 1
            FixedCostsGrid.Columns("Cost").Width = 200
            FixedCostsGrid.Columns("Cost").HeaderText = "Kostnad"

            '*********  ATRMemo Tabellen *********'
            ATRMemoTableView.Table = DataSet.Tables("ATRMemo")
            ARTMemoBindinSrc.DataSource = ATRMemoTableView
            MemoText.DataBindings.Add(New Binding("Text",
                                                     ARTMemoBindinSrc, "FreeText", True))

            '********* ResourceSelect   *********'
            ResourceSelectView.Table = DataSet.Tables("Resource")
            ResourceSelectBindingSrc.DataSource = ResourceSelectView
        Catch ex As Exception
            MakeErrorReport("BindControls: ", ex)
        End Try
    End Sub

    Private Sub SaveProjectInfo_Click(sender As Object, e As EventArgs)
        Try
            Dim i As Integer = 0
            Me.Validate()
            ProjectBindingSrc.EndEdit()
            i = InsertTable(DataSet, "Project")
        Catch ex As Exception
            MsgBox("Fel vid insättning i huvuduppdragstabell")
            MakeErrorReport("SaveProjectInfo :", ex)
        End Try

    End Sub

    Private Sub UpdateResourceButton_Click(sender As Object, e As EventArgs) Handles UpdateResourceButton.Click
        Try

            Dim i As Integer = 0
            i = InsertTable(DataSet, "Resource")
            If i <> 0 Then
                MsgBox("Alla tabeller uppdaterade")
            Else
                MsgBox("Fel vid insättning i Resurstabell")
            End If


        Catch ex As Exception


        End Try
    End Sub

    Private Sub CatGrid_RowLeave(sender As Object, e As DataGridViewCellEventArgs) Handles CatGrid.RowLeave

    End Sub

    Private Sub SaveATRButton_Click(sender As Object, e As EventArgs) Handles SaveATRButton.Click
        Try
            Dim status As Integer = 0

            status = InsertTable(DataSet, "Risks")

            If status <> 0 Then
                MakeLogUpdate("Input-, Output- och Risk-Tabell till ATR: " & ATRnumber.SelectedItem.item("ID") & " har skapats/uppdaterats av:  " & Environment.UserName & ": " & System.DateTime.Today)
                MsgBox("Tabeller skapade/uppdaterade")
            Else
                MsgBox("Fel vid insättning i ATR undertabeller ")
            End If
        Catch ex As Exception

            MsgBox("Fel vid insättning i ATR undertabeller ")
            MakeErrorReport("SaveATRButton :", ex)
        End Try
    End Sub

    Private Sub tabControl_Selecting(sender As Object, e As TabControlCancelEventArgs) Handles tabControl.Selecting
        Try
            If DBconnectionExit() = True Then
                ATRPageNr1.Text = ATRnumber.SelectedItem.item("ID")
                ATRPageNr2.Text = ATRnumber.SelectedItem.item("ID")
                ATRNrMemo.Text = ATRnumber.SelectedItem.item("ID")
                UpdateATR_ConstTab(ATRnumber.SelectedItem.item("ID"))
            End If
        Catch ex As Exception

        End Try
    End Sub

    Private Sub CreateATR_Click(sender As Object, e As EventArgs) Handles CreateATR.Click
        Try
            'UpdateAllTables(DataSet)
            InitWord()
            Dim StageFilterString As String
            Dim StageResultRows() As DataRow
            Dim ATRFilterString As String
            Dim ATRResultRows() As DataRow
            If FullATRListRadioButton.Checked = True Then ' alla på resursnivå, Finns ju bara rad så vi tar alla ATR blad
                For Each ATRRow As DataRow In DataSet.Tables("ATR").Rows
                    CreateWordFile(ATRRow)
                Next
                MakeLogUpdate("*********   Alla ATR-blad utskrivna av: " & Environment.UserName & ": " & System.DateTime.Today & "   ********* ")
            ElseIf SubprojectATRListRadioButton.Checked = True Then  ' på aktivt Huvuduppdrag
                StageFilterString = "FK_ID ='" & SubprojectNumber.Text & "'"
                StageResultRows = DataSet.Tables("Stage").Select(StageFilterString)
                For Each StageRow As DataRow In StageResultRows
                    ATRFilterString = "FK_ID='" & StageRow.Item("ID") & "'"
                    ATRResultRows = DataSet.Tables("ATR").Select(ATRFilterString)
                    For Each ATRRow As DataRow In ATRResultRows
                        CreateWordFile(ATRRow)
                    Next
                Next
                MakeLogUpdate("*********   Alla ATR-blad på Huvuduppdragsnr: " & SubprojectNumber.Text & "  utskrivna av: " &
                              Environment.UserName & ": " & System.DateTime.Today & "   ********* ")

            ElseIf StageATRListRadioButton.Checked = True Then ' aktivt deluppdrag
                ATRFilterString = "FK_ID='" & StageID & "'"
                ATRResultRows = DataSet.Tables("ATR").Select(ATRFilterString)
                For Each ATRRow As DataRow In ATRResultRows
                    CreateWordFile(ATRRow)
                Next

                MakeLogUpdate("*********  Alla ATR-blad på deluppdragsnummer: " & DataSet.Tables("Stage").Rows(StageTablePos).Item("SubNr") &
                              "på huvuduppdragnummer :" & DataSet.Tables("Stage").Rows(StageTablePos).Item("FK_ID") & " av: " &
                              Environment.UserName & ": " & System.DateTime.Today & "    *********")
            ElseIf ATRRadioButton.Checked = True Then ' på aktivt ATRblad.
                ATRFilterString = "ID='" & ATRnumber.SelectedItem.item("ID") & "'"
                ATRResultRows = DataSet.Tables("ATR").Select(ATRFilterString)
                If ATRResultRows.Length > 0 Then
                    CreateWordFile(ATRResultRows(0))
                    MakeLogUpdate("*********  ATR-blad : " & ATRResultRows(0).Item("ID") & " av: " &
                              Environment.UserName & ": " & System.DateTime.Today & "    *********")
                Else
                    MsgBox("Aktivt ATR nummer är inte sparat i databas")
                End If


            ElseIf ATRDateRadioButton.Checked = True Then

                For Each ATRRow As DataRow In DataSet.Tables("ATR").Rows
                    If ATRStartDateRadioButton.Checked = True Then
                        If ATRRow.Item("StartDate") = ATRPrintDatePicker.Value Then
                            CreateWordFile(ATRRow)
                        End If
                    End If
                    If ATREndDateRadioButton.Checked = True Then
                        If ATRRow.Item("EndDate") = ATRPrintDatePicker.Value Then
                            CreateWordFile(ATRRow)
                        End If
                    End If
                    If ATRRevDateRadioButton.Checked = True Then
                        If ATRRow.Item("RevDate") = ATRPrintDatePicker.Value Then
                            CreateWordFile(ATRRow)
                        End If
                    End If
                Next
                If ATRStartDateRadioButton.Checked = True Then
                    MakeLogUpdate("*********   Alla ATR-blad med startdatum: " & ATRPrintDatePicker.Value & "  utskrivna av: " &
                                  Environment.UserName & ": " & System.DateTime.Today & "   ********* ")
                ElseIf ATREndDateRadioButton.Checked = True Then
                    MakeLogUpdate("*********   Alla ATR-blad med slutdatum: " & ATRPrintDatePicker.Value & "  utskrivna av: " &
                                  Environment.UserName & ": " & System.DateTime.Today & "   ********* ")
                ElseIf ATRRevDateRadioButton.Checked = True Then
                    MakeLogUpdate("*********   Alla ATR-blad med Ändringsdatum: " & ATRPrintDatePicker.Value & "  utskrivna av: " &
                                  Environment.UserName & ": " & System.DateTime.Today & "   ********* ")
                End If
            ElseIf ATRVersionRadioButton.Checked = True Then
                For Each ATRRow As DataRow In DataSet.Tables("ATR").Rows
                    If ATRRow.Item("Version") = ATRPrintVersionBox.Text Then
                        CreateWordFile(ATRRow)
                    End If
                Next
                MakeLogUpdate("*********   Alla ATR-blad med Version: " & ATRPrintVersionBox.Text & "  utskrivna av: " &
                              Environment.UserName & ": " & System.DateTime.Today & "   ********* ")
            ElseIf ATRRevSignRadioButton.Checked = True Then
                For Each ATRRow As DataRow In DataSet.Tables("ATR").Rows
                    If ATRRow.Item("RevSign") = ATRPrintRevSignBox.Text Then
                        CreateWordFile(ATRRow)
                    End If
                Next
                MakeLogUpdate("*********   Alla ATR-blad skapade/ ändrade: " & ATRPrintRevSignBox.Text & "  utskrivna av: " &
                              Environment.UserName & ": " & System.DateTime.Today & "   ********* ")

            End If
            ExitWord()
            MsgBox("ATR-blad är klara ")
        Catch ex As Exception
            MakeErrorReport("CreateATR_Click", ex)
        End Try
    End Sub

    Private Sub Project_NewButton_Click(sender As Object, e As EventArgs) Handles Project_NewButton.Click
        Try
            Dim R As Data.DataRow = DataSet.Tables("Project").NewRow()
            R.Item("ID") = ProjectNumber.Text
            R.Item("ProjectName") = ProjectName.Text
            R.Item("Leader") = Projectleader.Text
            R.Item("ProjectOwner") = ProjectOwner.Text
            If StrComp(CustomerNumber.Text, "") <> 0 Then
                R.Item("CustomerNr") = CInt(CustomerNumber.Text)
            End If

            DataSet.Tables("Project").Rows.Add(R)
            InsertTable(DataSet, "Project")
            ProjectNumber.Items.Add(ProjectNumber.Text)

            MakeLogUpdate("Ny rad resultatenhetsinfomation: Uppdragsnr: " & ProjectNumber.Text & " av:  " & Environment.UserName & ": " & System.DateTime.Today)
            MsgBox("Ny Resultatenhet skapad")
        Catch ex As Exception
            If InStr(ex.Message, "Your network access was interrupted.") > 0 Then
                MsgBox("Probelm med nätverket, inget har sparats i databasen" & Environment.NewLine &
                       "Spara undan skapade texter på annan plats")
            Else
                MsgBox("Fel vid skapande av nytt projekt")

            End If
            MakeErrorReport("Project_NewButton_Click", ex)
        End Try
    End Sub

    Private Sub Project_DeleteButton_Click(sender As Object, e As EventArgs) Handles Project_DeleteButton.Click
        Dim R As DataRow = DataSet.Tables("Project").Rows.Find(CInt(ProjectNumber.Text))
        R.Delete()
        InsertTable(DataSet, "Project")
        MakeLogUpdate("Rad raderad ur resultatenhetsinfomation: Uppdragsnr: " & ProjectNumber.Text & " av:  " & Environment.UserName & ": " & System.DateTime.Today)
    End Sub

    Private Sub Project_UpdateButton_Click(sender As Object, e As EventArgs) Handles Project_UpdateButton.Click
        Try
            Dim FilterString As String
            Dim ResultRows() As DataRow

            FilterString = "ID= '" & ProjectNumber.Text & "'"
            ResultRows = DataSet.Tables("Project").Select(FilterString)

            If ResultRows.Length > 0 Then
                ResultRows(0).BeginEdit()
                ResultRows(0).Item("ProjectName") = ProjectName.Text
                ResultRows(0).Item("Leader") = Projectleader.Text
                ResultRows(0).Item("ProjectOwner") = ProjectOwner.Text

                If StrComp(CustomerNumber.Text, "") <> 0 Then
                    ResultRows(0).Item("CustomerNr") = CInt(CustomerNumber.Text)
                Else
                    ResultRows(0).Item("CustomerNr") = 0
                End If
                ResultRows(0).EndEdit()
                InsertTable(DataSet, "Project")
            End If
        Catch ex As Exception
            If InStr(ex.Message, "Your network access was interrupted.") > 0 Then
                MsgBox("Probelm med nätverket, inget har sparats i databasen" & Environment.NewLine &
                       "Spara undan skapade texter på annan plats")
            Else
                MsgBox("Fel vid uppdatering av Resultatenhet")
            End If
            MakeErrorReport("Project_UpdateButton_Click: ", ex)
        End Try

    End Sub

    Private Sub Sub_NewButton_Click(sender As Object, e As EventArgs) Handles Sub_NewButton.Click
        Try
            Dim i As Integer = 0
            Dim R As Data.DataRow = DataSet.Tables("SubProject").NewRow()
            R.Item("ID") = SubprojectNumber.Text
            R.Item("FK_ID") = ProjectNumber.Text
            R.Item("TableName") = SubProjectName.Text
            R.Item("Leader") = SubProjectLeader.Text
            DataSet.Tables("SubProject").Rows.Add(R)
            i = InsertTable(DataSet, "SubProject")
            MakeLogUpdate("Ny rad i Uppdragsinfomation: Uppdragsnr: " & SubprojectNumber.Text & " av:  " & Environment.UserName & ": " & System.DateTime.Today)
            MsgBox("Nytt huvuduppdrag skapat")
        Catch ex As Exception
            If InStr(ex.Message, "Your network access was interrupted.") > 0 Then
                MsgBox("Probelm med nätverket, inget har sparats i databasen" & Environment.NewLine &
                       "Spara undan skapade texter på annan plats")
            Else
                MsgBox("Deluppdragsnummer måste vara unikt")
            End If
            MakeErrorReport("Sub_NewButton_Click", ex)
        End Try

    End Sub

    Private Sub Sub_DeleteButton_Click(sender As Object, e As EventArgs) Handles Sub_DeleteButton.Click
        Try
            Dim R As DataRow = DataSet.Tables("SubProject").Rows.Find(CInt(SubprojectNumber.Text))
            R.Delete()
            InsertTable(DataSet, "SubProject")
        Catch ex As Exception
            MakeErrorReport("Sub_DeleteButton_Click", ex)
        End Try
    End Sub

    Private Sub Sub_UpdateButton_Click(sender As Object, e As EventArgs) Handles Sub_UpdateButton.Click
        Try
            Dim R As DataRow = DataSet.Tables("SubProject").Rows.Find(SubprojectNumber.Text)
            'SubTableView.Table.Rows(SubprojectTablePos).Item("ID") = SubprojectNumber.Text
            R.BeginEdit()
            ' eftersom att fälten är kopplande via binding kontrolls behöver jag bara säga att någon har uppdaterat något 
            ' och sedan slutat editera 
            R.EndEdit()
            InsertTable(DataSet, "SubProject")
            MakeLogUpdate("Rad uppdaterad i Uppdragsinfomation: Uppdragsnr: " & SubprojectNumber.Text & " av:  " & Environment.UserName & ": " & System.DateTime.Today)
            MsgBox("Huvuduppdrag uppdaterat")
        Catch ex As Exception
            If InStr(ex.Message, "Your network access was interrupted.") > 0 Then
                MsgBox("Probelm med nätverket, inget har sparats i databasen" & Environment.NewLine &
                       "Spara undan skapade texter på annan plats")
            Else
                MsgBox("Fel vid uppdatering av huvudupdprag")
            End If
            MakeErrorReport("Sub_UpdateButtonClick", ex)
        End Try

    End Sub

    Private Sub Stage_NewButton_Click(sender As Object, e As EventArgs) Handles Stage_NewButton.Click
        Try
            If StageListBox.SelectedIndex <> -1 Then

                Dim R As Data.DataRow = DataSet.Tables("Stage").NewRow()
                R.Item("SubNr") = GetStageSubNr(StageListBox.SelectedIndex)
                R.Item("FK_ID") = SubprojectNumber.Text
                R.Item("TableName") = StageListBox.SelectedIndex
                R.Item("Leader") = StageLeader.Text
                R.Item("IndexName") = StageListBox.SelectedItem
                StageSubNr = GetStageSubNr(StageListBox.SelectedIndex)

                DataSet.Tables("Stage").Rows.Add(R)

                InsertTable(DataSet, "Stage")


                ATRLink.Text = ProjectName.Text & " - " & SubProjectName.Text & " - " & StageComboBox.Text
                GenATRnumber.Text = Strings.Left(SubprojectNumber.Text, 7) & StageNumber & "XX"

                MakeLogUpdate("Ny rad i Deluppdragsinfomation: deluppdragsnamn: " & StageListBox.SelectedItem & " i huvuduppdragsnr: " & SubprojectNumber.Text & " av:  " & Environment.UserName & ": " & System.DateTime.Today)
                MsgBox("Nytt deluppdrag skapat")
            Else
                MsgBox("Klicka på texten i Namn på skedet för att välja skede")
            End If

        Catch ex As Exception
            If InStr(ex.Message, "Your network access was interrupted.") > 0 Then
                MsgBox("Probelm med nätverket, inget har sparats i databasen" & Environment.NewLine &
                       "Spara undan skapade texter på annan plats")
            Else
                MsgBox("Fel vid skapade av deluppdrag")
            End If
            MakeErrorReport("Stage_NewButton_Click", ex)
        End Try
    End Sub

    Private Sub Stage_UpdateButton_Click(sender As Object, e As EventArgs) Handles Stage_UpdateButton.Click
        Try
            Dim R As DataRow = DataSet.Tables("Stage").Rows.Find(StageID)
            R.BeginEdit()
            R.Item("IndexName") = StageListBox.SelectedItem
            R.Item("SubNr") = GetStageSubNr(StageListBox.SelectedIndex)
            'R.Item("FK_ID") = SubprojectNumber.Text
            R.Item("TableName") = StageListBox.SelectedIndex
            'R.Item("Leader") = StageLeader.Text
            R.EndEdit()
            InsertTable(DataSet, "Stage")

            MakeLogUpdate("Rad uppdaterad i Deluppdragsinfomation: deluppdragsnr: " & StageNumber & " i huvuduppdragsnr: " & SubprojectNumber.Text & " av:  " & Environment.UserName & ": " & System.DateTime.Today)
            MsgBox("deluppdrag uppdaterat")
        Catch ex As Exception
            If InStr(ex.Message, "Your network access was interrupted.") > 0 Then
                MsgBox("Probelm med nätverket, inget har sparats i databasen" & Environment.NewLine &
                       "Spara undan skapade texter på annan plats")
            Else
                MsgBox("Fel vid uppdatering av deluppdrag")
            End If
            MakeErrorReport("Stage_UpdateButton_Click", ex)
        End Try
    End Sub

    Private Sub Stage_DeleteButton_Click(sender As Object, e As EventArgs) Handles Stage_DeleteButton.Click
        Try
            ' Räcker inte med att ta delete på bara denna rad måste se om det finns barn till raden oxå
            Dim R As DataRow = DataSet.Tables("Stage").Rows.Find(StageID)
            R.Delete()
            InsertTable(DataSet, "Stage")
            MakeLogUpdate("Rad Raderad ur Deluppdragsinfomation: deluppdragsnr: " & StageNumber & " i huvuduppdragsnr: " & SubprojectNumber.Text & " av:  " & Environment.UserName & ": " & System.DateTime.Today)

        Catch ex As Exception
            MakeErrorReport("Sub_DeleteButton_Click", ex)
        End Try
    End Sub

    Private Sub ATR_NewButton_Click(sender As Object, e As EventArgs) Handles ATR_NewButton.Click
        Try
            If Strings.Len(ATRnumber.Text) = 10 Then
                If StrComp(Strings.Left(ATRnumber.Text, 7), Strings.Left(GenATRnumber.Text, 7)) = 0 Then
                    If InStr(ATRName.Text, ":") = 0 And InStr(ATRName.Text, ";") = 0 Then ' : och ; är otilåtna tecken i filnamn och etersom vi använder namnet som filnamn skall dessa tas bort redan här
                        Dim R As Data.DataRow = DataSet.Tables("ATR").NewRow()
                        R.Item("ID") = ATRnumber.Text
                        R.Item("FK_ID") = StageID
                        R.Item("ATRName") = ATRName.Text
                        R.Item("Leader") = ATRLeader.Text
                        R.Item("Scope") = ATRScope.Text
                        R.Item("StartDate") = StartDate.Value
                        R.Item("EndDate") = EndDate.Value
                        R.Item("Version") = ATRVersion.Text
                        R.Item("TRVNumber") = TRVNameBox.Text
                        ATRrevDate.Value = System.DateTime.Today
                        R.Item("RevDate") = ATRrevDate.Value
                        ATRRevSign.Text = Environment.UserName
                        R.Item("RevSign") = ATRRevSign.Text
                        R.EndEdit()
                        DataSet.Tables("ATR").RejectChanges() ' Denna rad lägger jag här för att få bort att det ser ut som jag har gjort ändingar i en annan ATR
                        DataSet.Tables("ATR").Rows.Add(R)
                        Dim i As Integer = 0
                        i = InsertTable(DataSet, "ATR")

                        If i <> 0 Then
                            FilterOnATRNumber(ATRnumber.Text)

                            MakeLogUpdate("Nytt ATRblad: " & ATRnumber.Text & " skapat av:  " & Environment.UserName & ": " & System.DateTime.Today)

                            MsgBox("Nytt ATR skapat")
                        Else
                            MsgBox("Fel vid insättning i Db")
                        End If

                    Else
                        MsgBox("ATR-namnet innehåller något av tecknerna : eller ; som är otillåtna tecken ")
                    End If
                Else
                    MsgBox("ATR-bladsnummer stämmer inte med föreslaget ATR-bladnummer" & Environment.NewLine &
                           "Aktivt huvuduppdrag och deluppdrag är: " & Environment.NewLine &
                           ATRLink.Text & Environment.NewLine & "Detta ger ATR-bladnummer: " & GenATRnumber.Text)
                End If
            Else
                MsgBox("ATR nummret skall vara 10 siffor")
            End If

        Catch ex As Exception
            MsgBox("Kunde inte skapa ett nytt ATR-blad")
            MakeErrorReport("ATR_NewButton_Click", ex)
        End Try
    End Sub

    Private Sub ATR_UpdateButton_Click(sender As Object, e As EventArgs) Handles ATR_UpdateButton.Click
        Try
            If StrComp(Strings.Left(ATRnumber.Text, 7), Strings.Left(GenATRnumber.Text, 7)) = 0 Then
                If InStr(ATRName.Text, ":") = 0 And InStr(ATRName.Text, ";") = 0 Then ' : och ; är otilåtna tecken i filnamn och etersom vi använder namnet som filnamn skall dessa tas bort redan här
                    Dim R() As DataRow
                    Dim FilterString As String = "ID = '" & ATRnumber.Text & "'"

                    R = DataSet.Tables("ATR").Select(FilterString)
                    If R.Length > 0 Then
                        R(0).BeginEdit()
                        R(0).Item("FK_ID") = StageID
                        R(0).Item("ATRName") = ATRName.Text
                        R(0).Item("Leader") = ATRLeader.Text
                        R(0).Item("Scope") = ATRScope.Text
                        R(0).Item("StartDate") = StartDate.Value
                        R(0).Item("EndDate") = EndDate.Value
                        R(0).Item("Version") = ATRVersion.Text
                        ATRrevDate.Value = System.DateTime.Today
                        R(0).Item("RevDate") = ATRrevDate.Value
                        ATRRevSign.Text = Environment.UserName
                        R(0).Item("RevSign") = ATRRevSign.Text
                        R(0).Item("TRVNumber") = TRVNameBox.Text
                        R(0).EndEdit()
                        Dim i As Integer = 0

                        i = InsertTable(DataSet, "ATR")
                        If i <> 0 Then
                            MakeLogUpdate("ATRblad: " & ATRnumber.Text & " uppdaterat av:  " & Environment.UserName & ": " & System.DateTime.Today)
                            MsgBox("ATR-blad uppdaterat")
                        Else
                            MsgBox("Fel vid uppdatering av databas")
                        End If

                    Else
                        MsgBox("Fel vid uppdatering av ATRblad" & Environment.NewLine)
                    End If
                Else
                    MsgBox("ATR-namnet innehåller något av tecknerna : eller ; som är otillåtna tecken ")
                End If
            Else
                MsgBox("ATR-bladsnummer stämmer inte med föreslaget ATR-bladnummer" & Environment.NewLine &
                       "Aktivt huvuduppdrag och deluppdrag är: " & Environment.NewLine &
                       ATRLink.Text & Environment.NewLine & "Detta ger ATR-bladnummer: " & GenATRnumber.Text)
            End If
        Catch ex As Exception
            MsgBox("Fel vid uppdatering av ATR-blad")
            MakeErrorReport("ATR_UpdateButton_Click", ex)
        End Try
    End Sub

    Private Sub CreateWordFile(ATRRow As DataRow)
        Try
            Dim sStrings As New List(Of String)
            Dim TmpString As String = ""
            Dim ResourceList As New List(Of String)
            Dim ExpensList As New List(Of String)
            Dim ResourceString As String = ""
            Dim ExpensString As String = ""
            Dim InputString As New List(Of String)
            Dim DeliveryString As New List(Of String)
            Dim RiskString As New List(Of String)
            Dim i As Integer = 0
            Dim StageTmpString As String = ""
            Dim StageTmpString2 As String = ""
            Dim SpecialFileName As String = ""
            Dim SubATRCatFilter As String
            Dim ATRCatJuncFilter As String
            Dim InPutResultRows() As DataRow
            Dim ResultResultRows() As DataRow
            Dim RisksResultRows() As DataRow
            Dim DelivieriesResultRows() As DataRow
            Dim FixedCostResultRows() As DataRow
            Dim AReCatJuncResultRows() As DataRow


            SubATRCatFilter = "FK_ID='" & ATRRow.Item("ID") & "'"
            ATRCatJuncFilter = "FK_ATRID='" & ATRRow.Item("ID") & "'"
            InPutResultRows = DataSet.Tables("Inputs").Select(SubATRCatFilter)
            ResultResultRows = DataSet.Tables("Results").Select(SubATRCatFilter)
            RisksResultRows = DataSet.Tables("Risks").Select(SubATRCatFilter)
            DelivieriesResultRows = DataSet.Tables("Delivieries").Select(SubATRCatFilter)
            FixedCostResultRows = DataSet.Tables("FixedCost").Select(SubATRCatFilter)
            AReCatJuncResultRows = DataSet.Tables("AReCatJunc").Select(ATRCatJuncFilter)

            Dim StageRow As DataRow = DataSet.Tables("Stage").Rows.Find(ATRRow.Item("FK_ID"))
            Dim SubProjectRow As DataRow = DataSet.Tables("SubProject").Rows.Find(StageRow.Item("FK_ID"))
            Dim ProjectRow As DataRow = DataSet.Tables("Project").Rows.Find(SubProjectRow.Item("FK_ID"))

            If System.IO.Directory.Exists(WordFolderLabel.Text & "\" & SubProjectRow.Item("TableName")) = False Then
                System.IO.Directory.CreateDirectory(WordFolderLabel.Text & "\" & SubProjectRow.Item("TableName"))
            End If
            StageTmpString = StageRow.Item("IndexName")
            StageTmpString2 = FindInString(StageTmpString, "(")
            If System.IO.Directory.Exists(WordFolderLabel.Text & "\" & SubProjectRow.Item("TableName") &
                                          "\" & StageTmpString2) = False Then
                System.IO.Directory.CreateDirectory(WordFolderLabel.Text & "\" & SubProjectRow.Item("TableName") &
                                                    "\" & StageTmpString2)
            End If

            ' alla som inte använder det interna numret skall få special
            ' vi hoppas att TRV kommer köra samma namnstadard och bara byta första delen.
            If StrComp(FileNameType, "SwecoInterStadard") <> 0 Then
                SpecialFileName = FileNameType & "-ATR-" & ATRRow.Item("ID") & "-" & ATRRow.Item("ATRName")

                OpenWordDoc(WordFolderLabel.Text & "\" & SubProjectRow.Item("TableName") & "\" & StageTmpString2 &
                            "\" & SpecialFileName & ".docx")
                sStrings.Add("ATR: " & SpecialFileName)

            Else
                SpecialFileName = ATRRow.Item("ID") & "-" & ATRRow.Item("ATRName")
                OpenWordDoc(WordFolderLabel.Text & "\" & SubProjectRow.Item("TableName") & "\" & StageTmpString2 &
                            "\" & SpecialFileName & ".docx")
                sStrings.Add("ATR: " & ATRRow.Item("ID"))
            End If

            sStrings.Add(ATRRow.Item("Leader"))
            sStrings.Add(ATRRow.Item("RevDate"))
            sStrings.Add(" ") ' Ärende nummer i Mallen används inte 
            If StrComp(FileNameType, "SwecoInterStadard") <> 0 Then
                sStrings.Add(ProjectRow.Item("ProjectName")) ' ProjectName
            Else
                sStrings.Add(SubProjectRow.Item("TableName")) ' ProjectName
            End If

            sStrings.Add(ATRRow.Item("ID")) ' Project.CustomerNr
            sStrings.Add(ATRRow.Item("Version"))
            sStrings.Add(ATRRow.Item("ATRName"))
            TextToHeader(sStrings)

            sStrings.Clear()
            If StrComp(FileNameType, "SwecoInterStadard") <> 0 Then
                sStrings.Add(SpecialFileName)
            Else
                sStrings.Add(ATRRow.Item("ID"))
            End If


            sStrings.Add(ATRRow.Item("ATRName")) ' tror att det är samma som dokumenttitel 
            sStrings.Add(ProjectRow.Item("ProjectName") & ", " & SubProjectRow("TableName") & ", " & StageRow("IndexName"))
            sStrings.Add(ATRRow.Item("Leader")) ' resurs som är ansvarig för ATR bladet.

            TmpString = ""
            sStrings.Add(ATRRow.Item("Scope")) ' Omfattning

            If ResultResultRows.Length > 0 Then
                For Each Row As DataRow In ResultResultRows
                    TmpString = TmpString & Row.Item("FreeText1")
                Next
            Else
                TmpString = " "
            End If

            sStrings.Add(TmpString) ' Resultat
            TmpString = ""

            sStrings.Add(ATRRow.Item("StartDate"))
            sStrings.Add(ATRRow.Item("EndDate"))


            TmpString = ""

            sStrings.Add(WordFolderLabel.Text & "\Excel\" & ATRRow.Item("ID") & ".xlsx") ' sökväg till den unika filen av TRV mallen
            'sStrings.Add("C:\Programering\SwecoATR\SwecoATR\bin\Debug\ATR-mall.xlsx") ' sökväg till den unika filen av TRV mallen

            TextToBody(sStrings, InPutResultRows, DelivieriesResultRows, RisksResultRows, AReCatJuncResultRows, FixedCostResultRows, DataSet.Tables("Cat"), DataSet.Tables("Resource"))

            ExitDoc(ATRInWordFormat.Checked)
        Catch ex As Exception
            ExitDoc(False)
            MakeErrorReport("CreateWordFile: " & ATRRow.Item("ID") & " :", ex)
            MsgBox("Fel vid skapande av ATR-blad: " & ATRRow.Item("ID"))

        End Try
    End Sub

    Private Sub WordFilepath_Click(sender As Object, e As EventArgs) Handles WordFilepath.Click
        WordFolderDialog.ShowDialog()
        WordFolderLabel.Text = WordFolderDialog.SelectedPath
    End Sub

    Private Sub LinkLabel1_LinkClicked(sender As Object, e As LinkLabelLinkClickedEventArgs) Handles HelpLinklabel.LinkClicked
        System.Diagnostics.Process.Start(HelpLink)
    End Sub

    Private Sub ATRDateRadioButton_CheckedChanged(sender As Object, e As EventArgs) Handles ATRDateRadioButton.CheckedChanged
        If ATRDateRadioButton.Checked = False Then
            ATRSelectGroupeBox.Enabled = False
        Else
            ATRSelectGroupeBox.Enabled = True
        End If
    End Sub

    Private Sub ATRVersionRadioButton_CheckedChanged(sender As Object, e As EventArgs) Handles ATRVersionRadioButton.CheckedChanged
        If ATRVersionRadioButton.Checked = False Then
            ATRPrintVersionBox.Enabled = False
        Else
            ATRPrintVersionBox.Enabled = True
        End If
    End Sub

    Private Sub ATRRevSignRadioButton_CheckedChanged(sender As Object, e As EventArgs) Handles ATRRevSignRadioButton.CheckedChanged
        If ATRRevSignRadioButton.Checked = False Then
            ATRPrintRevSignBox.Enabled = False
        Else
            ATRPrintRevSignBox.Enabled = True
        End If
    End Sub

    Private Sub SaveATRResCostButton_Click(sender As Object, e As EventArgs) Handles SaveATRResCostButton.Click
        Try
            Dim TotalCost As Integer = 0
            Dim i As Integer = 0
            i = InsertTable(DataSet, "AReCatJunc")

            If i <> 0 Then
                MakeLogUpdate("Resurs-, kostTabell till ATR: " & ATRnumber.SelectedItem.item("ID") & " har skapats/uppdaterats av:  " & Environment.UserName & ": " & System.DateTime.Now)
                MsgBox("Tabeller skapade/uppdaterade")
            Else
                MsgBox("Fel vid insättning i ATR undertabeller ")
            End If
            '''''************* FIEXED COST *************''''''

            UpdateATR_ConstTab(ATRnumber.SelectedItem.item("ID"))
        Catch ex As Exception
            MakeErrorReport("SaveATRResCostButton :", ex)
        End Try
    End Sub

    Private Function GetStageSubNr(SelectBoxIndex As Integer)
        If SelectBoxIndex > 0 Then
            Return StageNumberList(SelectBoxIndex)
        Else
            Return StageNumberList(0)
        End If

    End Function

    Private Sub UpdateATR_ConstTab(ATRNumberString As String)
        Try
            Dim TotalCost As Integer = 0
            UpdateATRResourceGridView(TotalCost)

            '''''********** FIXED COST **********'''''
            If FixedCostsGrid.Rows.Count > 0 Then
                For Each Row As DataGridViewRow In FixedCostsGrid.Rows
                    If StrComp(Row.Cells("FK_ID").Value, "") <> 0 Then
                        If IsDBNull(Row.Cells("Cost").Value) = False Then
                            TotalCost = TotalCost + CInt(Row.Cells("Cost").Value)
                        End If
                    End If
                Next
                FixedCostsGrid.Update()
            End If
            TotalCostATR.Text = TotalCost
        Catch ex As Exception
            ' MakeErrorReport("UpdateATR_ConstTab", ex)
        End Try

    End Sub

    Private Sub FixedCostsGrid_UserDeletingRow(sender As Object, e As DataGridViewRowCancelEventArgs) Handles FixedCostsGrid.UserDeletingRow


    End Sub

    Private Sub ATRInputGrid_UserDeletingRow(sender As Object, e As DataGridViewRowCancelEventArgs)

    End Sub

    Private Sub ATRDeliveriesGrid_UserDeletingRow(sender As Object, e As DataGridViewRowCancelEventArgs) Handles ATRDeliveriesGrid.UserDeletingRow
        Try


        Catch ex As Exception
            MsgBox("Fel vid radering av rad i leveranser")
            MakeErrorReport("ATRDeliveriesGrid_UserDeletingRow", ex)
        End Try
    End Sub

    Private Sub ATRRisksGrid_UserDeletingRow(sender As Object, e As DataGridViewRowCancelEventArgs) Handles ATRRisksGrid.UserDeletingRow
        Try

        Catch ex As Exception
            MsgBox("Fel vid radering av rad i Risker")
            MakeErrorReport("ATRRisksGrid_UserDeletingRow", ex)
        End Try
    End Sub

    Private Sub ATRDeliveriesGrid_CellEnter(sender As Object, e As DataGridViewCellEventArgs) Handles ATRDeliveriesGrid.CellEnter
        Dim Row As DataGridViewRow = ATRDeliveriesGrid.CurrentRow
        Row.Cells("FK_ID").Value = ATRnumber.SelectedItem.item("ID")
    End Sub

    Private Sub ATRInputGrid_RowValidating(sender As Object, e As DataGridViewCellCancelEventArgs) Handles ATRInputGrid.RowValidating
        'Dim Row As DataGridViewRow = ATRDeliveriesGrid.CurrentRow
        'Row.Cells("FK_ID").Value = ATRnumber.SelectedItem.item("ID")
    End Sub

    Private Sub ATRInputGrid_CellEnter(sender As Object, e As DataGridViewCellEventArgs) Handles ATRInputGrid.CellEnter
        Try
            Dim Row As DataGridViewRow = ATRInputGrid.CurrentRow
            Row.Cells("FK_ID").Value = ATRnumber.SelectedItem.item("ID")
        Catch ex As Exception
            'MakeErrorReport("ATRInputLeaveCell: ", ex)
        End Try
    End Sub

    Private Sub ATRRisksGrid_CellEnter(sender As Object, e As DataGridViewCellEventArgs) Handles ATRRisksGrid.CellEnter
        Dim Row As DataGridViewRow = ATRRisksGrid.CurrentRow
        Row.Cells("FK_ID").Value = ATRnumber.SelectedItem.item("ID")
    End Sub

    Private Sub FixedCostsGrid_CellEnter(sender As Object, e As DataGridViewCellEventArgs) Handles FixedCostsGrid.CellEnter
        Dim Row As DataGridViewRow = FixedCostsGrid.CurrentRow
        Row.Cells("FK_ID").Value = ATRnumber.SelectedItem.item("ID")
    End Sub

    Private Sub ATRResourcesGrid_CellEnter(sender As Object, e As DataGridViewCellEventArgs) Handles ATRResourcesGrid.CellEnter
        Try
            Dim GridRow As DataGridViewRow = ATRResourcesGrid.CurrentRow
            Dim FilterString As String
            Dim ResultRows() As DataRow
            Dim CatRow As DataRow
            Dim CurrentCell As DataGridViewCell = ATRResourcesGrid.CurrentCell
            If IsDBNull(GridRow.Cells("FK_RID").Value) = True Then
                If StrComp(GridRow.Cells("Name").Value, "") <> 0 Then
                    FilterString = "FreeText='" & GridRow.Cells("Name").Value & "'"
                    ResultRows = DataSet.Tables("Resource").Select(FilterString)
                    If ResultRows.Length > 0 Then
                        If ResultRows.Length = 1 Then ' Resursen finns bara en gång i Tabellen
                            CatRow = DataSet.Tables("Cat").Rows.Find(ResultRows(0).Item("CatID"))
                            GridRow.Cells("FK_CatID").Value = ResultRows(0).Item("CatID")
                            GridRow.Cells("FK_RID").Value = ResultRows(0).Item("ID")
                            If IsDBNull(GridRow.Cells("TimeH").Value) = True Then
                                GridRow.Cells("TimeH").Value = 0
                            End If
                            GridRow.Cells("Calc").Value = CInt(GridRow.Cells("TimeH").Value) * CatRow.Item("Cost")
                            GridRow.Cells("CatName").Value = ResultRows(0).Item("RoleText") & " (" & CatRow.Item("Cost") & " kr)"
                            GridRow.Cells("FK_ATRID").Value = ATRnumber.SelectedItem.item("ID")
                            RowIndex = GridRow.Index
                        Else  ' Resursen finns med flera kategorier
                            If ShowWindow = True Then ' Den vill visa fönstret två gånger per rad
                                SwecoName = GridRow.Cells("Name").Value
                                ResourceSelectForm.Show()
                            End If
                            SwecoID = ""
                        End If
                    Else
                        MsgBox("Namnet finns inte i Resurslistan")

                    End If

                    ATRResourcesGrid.Update()
                    SwecoID = ""
                End If

            End If

        Catch ex As Exception
            MakeErrorReport("ATRResourcesGrid_CellEnter:", ex)
        End Try

    End Sub

    Private Sub OpenPDFCatalog_Click(sender As Object, e As EventArgs) Handles OpenPDFCatalog.Click
        System.Diagnostics.Process.Start(WordFolderLabel.Text)
    End Sub

    Private Sub WBSLinkLabel_LinkClicked(sender As Object, e As LinkLabelLinkClickedEventArgs) Handles WBSLinkLabel.LinkClicked
        System.Diagnostics.Process.Start(WBSLink)
    End Sub

    Private Sub ATRMemoSave_Click(sender As Object, e As EventArgs) Handles ATRMemoSave.Click
        Try
            Dim FilterString As String
            Dim ResultRows() As DataRow
            FilterString = "ID='" & ATRnumber.SelectedItem.item("ID") & "'"
            ResultRows = DataSet.Tables("ATRMemo").Select(FilterString)
            If ResultRows.Length > 0 Then
                ResultRows(0).BeginEdit()
                ResultRows(0).Item("FreeText") = MemoText.Text
                ResultRows(0).EndEdit()
            Else
                Dim Row As DataRow = DataSet.Tables("ATRMemo").NewRow
                Row.Item("ID") = ATRnumber.SelectedItem.item("ID")
                Row.Item("FreeText") = MemoText.Text
                DataSet.Tables("ATRMemo").Rows.Add(Row)
            End If
            Dim i As Integer = 0
            i = InsertTable(DataSet, "ATRMemo")
            If i <> 0 Then
                MsgBox("Minnesanteckningar sparade")
                MakeLogUpdate("Minnesanteckningar till ATR: " & ATRnumber.SelectedItem.item("ID") & " har skapats/uppdaterats av:  " & Environment.UserName & ": " & System.DateTime.Now)
            Else
                MsgBox("Fel vid sparande av minnesanteckninar")
            End If

        Catch ex As Exception
            MakeErrorReport("ATRMemoSave_Click: ", ex)
        End Try
    End Sub

    Private Sub CopyATRButton_Click(sender As Object, e As EventArgs) Handles CopyATRButton.Click
        CopyATRForm.Show()
    End Sub

    Private Sub ListSelectBox_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ListSelectBox.SelectedIndexChanged
        Select Case ListSelectBox.SelectedItem
            Case "ATRLista för granskning"
                ListSelectSubProjectRButton.Enabled = True
                ListSelectStageRButton.Enabled = True
                ListSelectSubProjectRButton.Checked = True
            Case "Timmar/Kategori"
                ListSelectProjectRButton.Enabled = True
                ListSelectSubProjectRButton.Enabled = True
                ListSelectProjectRButton.Checked = True

            Case Else
                ListSelectProjectRButton.Enabled = False
                ListSelectSubProjectRButton.Enabled = False
                ListSelectStageRButton.Enabled = False
        End Select
    End Sub

    Private Sub CreateList_Click(sender As Object, e As EventArgs) Handles CreateList.Click
        Try
            Dim StageRows() As DataRow
            Dim ATRRows() As DataRow
            Dim ATRResourceRows() As DataRow
            Dim ATRFixedCostRows() As DataRow
            Dim ResourceTableRow() As DataRow
            Dim ResultRows() As DataRow
            Dim DeliveryRows() As DataRow
            Dim CatRows As DataRow
            Dim ListTableRow As DataRow

            Dim i As Integer = 0
            Dim j As Integer = 0
            Dim k As Integer = 0

            Dim CatFilter As String = ""
            Dim StageFilter As String = ""
            Dim ATRFilter As String = ""
            Dim ATRSort As String = ""
            Dim ATRSubFilter As String = ""
            Dim ATRResourceFilter As String = ""
            Dim ATRResourceSort As String = ""
            Dim TotalReCost As Integer = 0
            Dim TotalFixedCost As Integer = 0
            Dim ResourceFilter As String = ""

            Dim ResultFilter As String
            Dim DeliveryFilter As String

            Select Case ListSelectBox.SelectedItem
                Case "Timmar/Resurslista"
                    Dim NameColumn As New DataColumn
                    Dim ATRColumn As New DataColumn
                    Dim SDColumn As New DataColumn
                    Dim EDColumn As New DataColumn
                    Dim TColumn As New DataColumn
                    Dim ListTable As New DataTable
                    NameColumn.DataType = System.Type.GetType("System.String")
                    NameColumn.ColumnName = "Name"
                    ListTable.Columns.Add(NameColumn) ' namn
                    TColumn.ColumnName = "TimeH"
                    TColumn.DataType = System.Type.GetType("System.Int32")
                    ListTable.Columns.Add(TColumn) ' timmar
                    SDColumn.ColumnName = "StartDate"
                    SDColumn.DataType = System.Type.GetType("System.DateTime")
                    ListTable.Columns.Add(SDColumn) ' startdatum
                    EDColumn.ColumnName = "EndDate"
                    EDColumn.DataType = System.Type.GetType("System.DateTime")
                    ListTable.Columns.Add(EDColumn) ' slutdatum
                    ATRColumn.DataType = System.Type.GetType("System.String")
                    ATRColumn.ColumnName = "ATRName"
                    ListTable.Columns.Add(ATRColumn) ' ATRnamn



                    For Each ResourceRow As DataRow In DataSet.Tables("Resource").Rows
                        Dim StartDate As DateTime = New DateTime(2200, 12, 31)

                        Dim EndDate As DateTime = New DateTime(1900, 1, 1)
                        Dim TotalHours As Integer = 0

                        ListTableRow = ListTable.NewRow
                        ListTableRow.Item("Name") = ResourceRow.Item("FreeText")
                        ATRResourceFilter = "FK_RID = '" & ResourceRow.Item("ID") & "' AND FK_CatID = " & ResourceRow.Item("CatID")
                        ATRResourceRows = DataSet.Tables("AReCatJunc").Select(ATRResourceFilter)
                        For i = 0 To ATRResourceRows.Length - 1
                            ATRFilter = "ID = '" & ATRResourceRows(i).Item("FK_ATRID") & "'"
                            ATRRows = DataSet.Tables("ATR").Select(ATRFilter)
                            ' En forsats som bara kommer att köras en gång 
                            For j = 0 To ATRRows.Length - 1
                                If DateSort.Checked = True Then
                                    If DateTime.Compare(SDate.Value, ATRRows(j).Item("StartDate")) < 1 And DateTime.Compare(ATRRows(j).Item("EndDate"), EDate.Value) < 1 Then
                                        If ATRRows(j).Item("StartDate") < StartDate Then
                                            StartDate = ATRRows(j).Item("StartDate")
                                        End If
                                        If ATRRows(j).Item("EndDate") > EndDate Then
                                            EndDate = ATRRows(j).Item("EndDate")
                                        End If

                                        If IsDBNull(ATRResourceRows(i).Item("TimeH")) = False Then
                                            ListTableRow.Item("ATRName") = ListTableRow.Item("ATRName") & Environment.NewLine & ATRRows(j).Item("ID") &
                                            " - " & ATRRows(j).Item("ATRName") & " (" & ATRResourceRows(i).Item("TimeH") & ")"
                                        Else
                                            ListTableRow.Item("ATRName") = ListTableRow.Item("ATRName") & Environment.NewLine & ATRRows(j).Item("ID") &
                                            " - " & ATRRows(j).Item("ATRName") & " (0)"
                                        End If

                                        If IsDBNull(ATRResourceRows(i).Item("TimeH")) = False Then
                                            TotalHours = TotalHours + ATRResourceRows(i).Item("TimeH")
                                        End If
                                    End If
                                Else
                                    If ATRRows(j).Item("StartDate") < StartDate Then
                                        StartDate = ATRRows(j).Item("StartDate")
                                    End If
                                    If ATRRows(j).Item("EndDate") > EndDate Then
                                        EndDate = ATRRows(j).Item("EndDate")
                                    End If

                                    If IsDBNull(ATRResourceRows(i).Item("TimeH")) = False Then
                                        ListTableRow.Item("ATRName") = ListTableRow.Item("ATRName") & Environment.NewLine & ATRRows(j).Item("ID") &
                                        " - " & ATRRows(j).Item("ATRName") & " (" & ATRResourceRows(i).Item("TimeH") & ")"
                                    Else
                                        ListTableRow.Item("ATRName") = ListTableRow.Item("ATRName") & Environment.NewLine & ATRRows(j).Item("ID") &
                                        " - " & ATRRows(j).Item("ATRName") & " (0)"
                                    End If

                                    If IsDBNull(ATRResourceRows(i).Item("TimeH")) = False Then
                                        TotalHours = TotalHours + ATRResourceRows(i).Item("TimeH")
                                    End If
                                End If
                            Next
                        Next
                        ListTableRow.Item("TimeH") = TotalHours
                        ListTableRow.Item("StartDate") = StartDate
                        ListTableRow.Item("EndDate") = EndDate
                        ListTable.Rows.Add(ListTableRow)

                    Next
                    ListInitExcel()
                    ListOpenExcelSheet(MainFolder & "\TimmarResurs.xlsx")
                    ListCreateTimeResourceExcel(ListTable)
                    ListExitWorkBook()
                    ListExitExcel()
                Case "ATRLista leverans", "Primavera Baslista"
                    Dim SubprojectLeader As New DataColumn
                    Dim ATRLeader As New DataColumn
                    Dim ATRName As New DataColumn
                    Dim ATRNumber As New DataColumn
                    Dim TotalCost As New DataColumn
                    Dim TotalHours As New DataColumn
                    Dim StartDate As New DataColumn
                    Dim EndDate As New DataColumn
                    Dim Version As New DataColumn
                    Dim TRVNumber As New DataColumn
                    Dim StageColumn As New DataColumn
                    Dim SubProjectColumn As New DataColumn
                    Dim ListTable As New DataTable



                    Dim TotalHour As Integer = 0

                    ATRNumber.DataType = System.Type.GetType("System.String")
                    ATRNumber.ColumnName = "ATRNumber"
                    ListTable.Columns.Add(ATRNumber)

                    ATRName.DataType = System.Type.GetType("System.String")
                    ATRName.ColumnName = "ATRName"
                    ListTable.Columns.Add(ATRName)

                    StageColumn.DataType = System.Type.GetType("System.String")
                    StageColumn.ColumnName = "Stage"
                    ListTable.Columns.Add(StageColumn)

                    SubProjectColumn.DataType = System.Type.GetType("System.String")
                    SubProjectColumn.ColumnName = "SubProject"
                    ListTable.Columns.Add(SubProjectColumn)

                    SubprojectLeader.DataType = System.Type.GetType("System.String")
                    SubprojectLeader.ColumnName = "SubProjectleader"
                    ListTable.Columns.Add(SubprojectLeader)

                    ATRLeader.DataType = System.Type.GetType("System.String")
                    ATRLeader.ColumnName = "ATRLeader"
                    ListTable.Columns.Add(ATRLeader)

                    TotalCost.DataType = System.Type.GetType("System.Int32")
                    TotalCost.ColumnName = "TotalCost"
                    ListTable.Columns.Add(TotalCost)

                    TotalHours.DataType = System.Type.GetType("System.Int32")
                    TotalHours.ColumnName = "TotalHours"
                    ListTable.Columns.Add(TotalHours)

                    StartDate.DataType = System.Type.GetType("System.DateTime")
                    StartDate.ColumnName = "StartDate"
                    ListTable.Columns.Add(StartDate)

                    EndDate.DataType = System.Type.GetType("System.DateTime")
                    EndDate.ColumnName = "EndDate"
                    ListTable.Columns.Add(EndDate)

                    Version.DataType = System.Type.GetType("System.String")
                    Version.ColumnName = "Version"
                    ListTable.Columns.Add(Version)

                    TRVNumber.DataType = System.Type.GetType("System.String")
                    TRVNumber.ColumnName = "TRVNumber"
                    ListTable.Columns.Add(TRVNumber)


                    For Each SubProjectRow As DataRow In DataSet.Tables("SubProject").Rows
                        StageFilter = "FK_ID = '" & SubProjectRow.Item("ID") & "'"
                        StageRows = DataSet.Tables("Stage").Select(StageFilter)
                        For i = 0 To StageRows.Length - 1
                            ATRFilter = "FK_ID = " & StageRows(i).Item("ID")
                            ATRRows = DataSet.Tables("ATR").Select(ATRFilter, "ID ASC")
                            For j = 0 To ATRRows.Length - 1
                                TotalFixedCost = 0
                                TotalReCost = 0
                                TotalHour = 0
                                ListTableRow = ListTable.NewRow
                                ListTableRow.Item("ATRNumber") = ATRRows(j).Item("ID")
                                ListTableRow.Item("SubProject") = SubProjectRow.Item("TableName")
                                ListTableRow.Item("Stage") = StageRows(i).Item("IndexName")
                                ListTableRow.Item("ATRName") = ATRRows(j).Item("ATRName")
                                ListTableRow.Item("ATRLeader") = ATRRows(j).Item("Leader")
                                ListTableRow.Item("SubProjectleader") = SubProjectRow.Item("Leader")
                                ListTableRow.Item("StartDate") = ATRRows(j).Item("StartDate")
                                ListTableRow.Item("EndDate") = ATRRows(j).Item("EndDate")
                                ListTableRow.Item("Version") = ATRRows(j).Item("Version")
                                ListTableRow.Item("TRVNumber") = ATRRows(j).Item("TRVNumber")

                                ATRResourceFilter = "FK_ATRID = '" & ATRRows(j).Item("ID") & "'"
                                ATRResourceRows = DataSet.Tables("AReCatJunc").Select(ATRResourceFilter)
                                For k = 0 To ATRResourceRows.Length - 1
                                    CatRows = DataSet.Tables("Cat").Rows.Find(ATRResourceRows(k).Item("FK_CatID"))
                                    If IsDBNull(ATRResourceRows(k).Item("TimeH")) = False Then
                                        TotalReCost = TotalReCost + (CatRows.Item("Cost") * ATRResourceRows(k).Item("TimeH"))
                                        TotalHour = TotalHour + ATRResourceRows(k).Item("TimeH")
                                    End If


                                Next

                                ATRSubFilter = "FK_ID = '" & ATRRows(j).Item("ID") & "'"
                                ATRFixedCostRows = DataSet.Tables("FixedCost").Select(ATRSubFilter)
                                For k = 0 To ATRFixedCostRows.Length - 1
                                    If IsDBNull(ATRFixedCostRows(k).Item("Cost")) = False Then
                                        TotalFixedCost = TotalFixedCost + ATRFixedCostRows(k).Item("Cost")
                                    End If

                                Next

                                ListTableRow.Item("TotalHours") = TotalHour
                                ListTableRow.Item("TotalCost") = TotalReCost + TotalFixedCost

                                ListTableRow.Item("Stage") = StageRows(i).Item("IndexName")
                                ListTableRow.Item("SubProject") = SubProjectRow.Item("TableName")

                                ListTable.Rows.Add(ListTableRow)
                            Next
                        Next
                    Next
                    ListInitExcel()
                    If StrComp(ListSelectBox.SelectedItem, "ATRLista leverans") = 0 Then
                        ListOpenExcelSheet(MainFolder & "\LeveransLista.xlsx")
                        ListCreatedeliveryListExcel(ListTable)
                    Else
                        ListOpenExcelSheet(MainFolder & "\PrimaveraLista.xlsx")
                        ListCreatePrimaveraListExcel(ListTable)
                    End If


                    ListExitWorkBook()
                    ListExitExcel()


                Case "ATRLista för granskning"
                    If ListSelectSubProjectRButton.Checked = True Then
                        Dim SubProjectFilter As String = "ID = '" & ListGenSubProjectNumber.Text & "'"
                        StageFilter = "FK_ID = '" & ListGenSubProjectNumber.Text & "'"
                        ATRFilter = "FK_ID = "

                    ElseIf ListSelectStageRButton.Checked = True Then


                    End If
                Case "ATRLista för IT"
                    Dim ProjectNumber As New DataColumn
                    Dim ProjectName As New DataColumn
                    Dim SubProjectNumber As New DataColumn
                    Dim SubProjectLeader As New DataColumn
                    Dim ATRNumber As New DataColumn
                    Dim ATRName As New DataColumn
                    Dim StartDate As New DataColumn
                    Dim EndDate As New DataColumn

                    Dim SwecoID As String = ""


                    Dim ListTable As New DataTable

                    ProjectNumber.DataType = System.Type.GetType("System.String")
                    ProjectNumber.ColumnName = "ProjectNumber"
                    ListTable.Columns.Add(ProjectNumber)

                    ProjectName.DataType = System.Type.GetType("System.String")
                    ProjectName.ColumnName = "ProjectName"
                    ListTable.Columns.Add(ProjectName)

                    SubProjectNumber.DataType = System.Type.GetType("System.String")
                    SubProjectNumber.ColumnName = "SubProjectNumber"
                    ListTable.Columns.Add(SubProjectNumber)

                    SubProjectLeader.DataType = System.Type.GetType("System.String")
                    SubProjectLeader.ColumnName = "SubProjectLeader"
                    ListTable.Columns.Add(SubProjectLeader)

                    ATRNumber.DataType = System.Type.GetType("System.String")
                    ATRNumber.ColumnName = "ATRNumber"
                    ListTable.Columns.Add(ATRNumber)

                    ATRName.DataType = System.Type.GetType("System.String")
                    ATRName.ColumnName = "ATRName"
                    ListTable.Columns.Add(ATRName)

                    StartDate.DataType = System.Type.GetType("System.DateTime")
                    StartDate.ColumnName = "StartDate"
                    ListTable.Columns.Add(StartDate)

                    EndDate.DataType = System.Type.GetType("System.DateTime")
                    EndDate.ColumnName = "EndDate"
                    ListTable.Columns.Add(EndDate)

                    For Each SubProjectRow As DataRow In DataSet.Tables("SubProject").Rows
                        StageFilter = "FK_ID = '" & SubProjectRow.Item("ID") & "'"
                        StageRows = DataSet.Tables("Stage").Select(StageFilter)
                        ResourceFilter = "FreeText = '" & SubProjectRow.Item("Leader") & "'"
                        ResourceTableRow = DataSet.Tables("Resource").Select(ResourceFilter)
                        If ResourceTableRow.Length > 0 Then
                            SwecoID = ResourceTableRow(0).Item("ID")
                        Else
                            MsgBox("Kan inte hitta " & SubProjectRow.Item("Leader") & " i resurslistan")
                        End If
                        i = 0
                        j = 0
                        For i = 0 To StageRows.Length - 1
                            ATRFilter = "FK_ID = " & StageRows(i).Item("ID")
                            ATRRows = DataSet.Tables("ATR").Select(ATRFilter)
                            For j = 0 To ATRRows.Length - 1
                                ListTableRow = ListTable.NewRow
                                ' En Lite fuling här antar att varje projekt bara innehåller en Resursenhet
                                ListTableRow.Item("ProjectNumber") = DataSet.Tables("Project").Rows(0).Item("ID")
                                ListTableRow.Item("SubProjectNumber") = SubProjectRow.Item("ID")
                                ListTableRow.Item("SubProjectLeader") = SwecoID
                                ListTableRow.Item("ATRNumber") = ATRRows(j).Item("ID")
                                ListTableRow.Item("ATRName") = ATRRows(j).Item("ATRName")
                                ListTableRow.Item("StartDate") = ATRRows(j).Item("StartDate")
                                ListTableRow.Item("EndDate") = ATRRows(j).Item("EndDate")
                                ListTable.Rows.Add(ListTableRow)
                            Next
                            j = 0
                        Next
                    Next
                    ListInitExcel()
                    ListOpenExcelSheet(MainFolder & "\SwecoIT.xlsx")
                    ListCreateITListExcel(ListTable, ITInfoFromFile)
                    ListExitWorkBook()
                    ListExitExcel()
                Case "Timmar/Kategori/Delprojekt "
                    CreateTimeCatSubProjectList()
                Case "Timmar/Kategori/ATR"
                    CreateTimeCatATRList()

                Case "ATRLista med resultaten"
                    Dim Tmpstring As String = ""
                    Dim ATRNumberColumn As New DataColumn
                    Dim TRVNumberColumn As New DataColumn
                    Dim ATRNameColumn As New DataColumn
                    Dim MainLeaderColumn As New DataColumn
                    Dim ATRLeaderColumn As New DataColumn
                    Dim EndDateColumn As New DataColumn
                    Dim ResultColumn As New DataColumn
                    Dim DeliviriesColumn As New DataColumn
                    Dim MainNumberColumn As New DataColumn
                    Dim MainNameColumn As New DataColumn

                    Dim ListTable As New DataTable

                    ATRNumberColumn.DataType = System.Type.GetType("System.String")
                    ATRNumberColumn.ColumnName = "ATRNumber"
                    ListTable.Columns.Add(ATRNumberColumn)

                    TRVNumberColumn.DataType = System.Type.GetType("System.String")
                    TRVNumberColumn.ColumnName = "TRVNumber"
                    ListTable.Columns.Add(TRVNumberColumn)

                    ATRNameColumn.DataType = System.Type.GetType("System.String")
                    ATRNameColumn.ColumnName = "ATRName"
                    ListTable.Columns.Add(ATRNameColumn)

                    MainLeaderColumn.DataType = System.Type.GetType("System.String")
                    MainLeaderColumn.ColumnName = "MainLeader"
                    ListTable.Columns.Add(MainLeaderColumn)

                    ATRLeaderColumn.DataType = System.Type.GetType("System.String")
                    ATRLeaderColumn.ColumnName = "ATRLeader"
                    ListTable.Columns.Add(ATRLeaderColumn)

                    EndDateColumn.DataType = System.Type.GetType("System.String")
                    EndDateColumn.ColumnName = "EndDate"
                    ListTable.Columns.Add(EndDateColumn)

                    ResultColumn.DataType = System.Type.GetType("System.String")
                    ResultColumn.ColumnName = "Result"
                    ListTable.Columns.Add(ResultColumn)

                    DeliviriesColumn.DataType = System.Type.GetType("System.String")
                    DeliviriesColumn.ColumnName = "Deliviries"
                    ListTable.Columns.Add(DeliviriesColumn)

                    MainNumberColumn.DataType = System.Type.GetType("System.String")
                    MainNumberColumn.ColumnName = "MainNumber"
                    ListTable.Columns.Add(MainNumberColumn)

                    MainNameColumn.DataType = System.Type.GetType("System.String")
                    MainNameColumn.ColumnName = "MainName"
                    ListTable.Columns.Add(MainNameColumn)

                    For Each SubProjectRow As DataRow In DataSet.Tables("SubProject").Rows
                        Tmpstring = ""
                        StageFilter = "FK_ID = '" & SubProjectRow.Item("ID") & "'"
                        StageRows = DataSet.Tables("Stage").Select(StageFilter)
                        StageRows = DataSet.Tables("Stage").Select(StageFilter)
                        For i = 0 To StageRows.Length - 1
                            ATRFilter = "FK_ID =" & StageRows(i).Item("ID")
                            ATRRows = DataSet.Tables("ATR").Select(ATRFilter)
                            For j = 0 To ATRRows.Length - 1
                                Dim TableRow As DataRow = ListTable.NewRow
                                TableRow.Item("MainNumber") = SubProjectRow.Item("ID")
                                TableRow.Item("MainName") = SubProjectRow.Item("TableName")
                                TableRow.Item("MainLeader") = SubProjectRow.Item("Leader")
                                TableRow.Item("TRVNumber") = ATRRows(j).Item("TRVNumber")
                                TableRow.Item("ATRNumber") = ATRRows(j).Item("ID")
                                TableRow.Item("ATRName") = ATRRows(j).Item("ATRName")
                                TableRow.Item("ATRLeader") = ATRRows(j).Item("Leader")
                                TableRow.Item("EndDate") = ATRRows(j).Item("EndDate")

                                ResultFilter = "FK_ID = '" & ATRRows(j).Item("ID") & "'"
                                ResultRows = DataSet.Tables("Results").Select(ResultFilter)
                                DeliveryFilter = "FK_ID = '" & ATRRows(j).Item("ID") & "'"
                                DeliveryRows = DataSet.Tables("Delivieries").Select(DeliveryFilter)

                                If ResultRows.Length > 0 Then
                                    TableRow.Item("Result") = ResultRows(0).Item("FreeText1")
                                End If

                                For k = 0 To DeliveryRows.Length - 1
                                    Tmpstring = Tmpstring & DeliveryRows(k).Item("FreeText1") & Environment.NewLine
                                Next

                                TableRow.Item("Deliviries") = Tmpstring

                                ListTable.Rows.Add(TableRow)
                            Next
                        Next
                    Next

                    ListInitExcel()
                    ListOpenExcelSheet(MainFolder & "\Leverabler.xlsx")
                    ListCreateATRResultExcel(ListTable)
                    ListExitWorkBook()
                    ListExitExcel()

                Case "Resurslista"

                    ListInitExcel()
                    ListOpenExcelSheet(MainFolder & "\Resurslista.xlsx")
                    ListCreateResursListExcel(DataSet.Tables("Resource"))
                    ListExitWorkBook()
                    ListExitExcel()

                Case "Risklista"
                    CreateRiskList()
                Case "Primavera Inputslista"
                    CreatePrimaveraSubTableList("Inputs")
                Case "Primavera leveranserLista"
                    CreatePrimaveraSubTableList("Delivieries")
                Case Else

            End Select
            MsgBox("Lista skapad/uppdaterad")
        Catch ex As Exception
            MakeErrorReport("CreateList_Click: ", ex)
            MsgBox("Fel vid skapandet av lista")
        End Try
    End Sub

    Public Sub SuperUserSetup()
        If SuperUser = True Then
            Project_NewButton.Visible = True
            Project_UpdateButton.Visible = True
            Sub_NewButton.Visible = True
            Sub_UpdateButton.Visible = True
            StageListBox.Enabled = True
            Stage_NewButton.Visible = True
            Stage_UpdateButton.Visible = True
            UpdateResourceButton.Enabled = True
            DeleteATR.Visible = True
            ImportExcelToolStripMenuItem.Enabled = True
            TRVNameBox.ReadOnly = False
            ChangeCatOnResourseButton.Enabled = True
            ATRInWordFormat.Enabled = True
            StageListBox.Visible = True
            MakeStagelabel.Visible = True
        Else
            Project_NewButton.Visible = False
            Project_UpdateButton.Visible = False
            Sub_NewButton.Visible = False
            Sub_UpdateButton.Visible = False
            StageListBox.Enabled = False
            Stage_NewButton.Visible = False
            Stage_UpdateButton.Visible = False
            UpdateResourceButton.Enabled = False
            DeleteATR.Visible = False
            ImportExcelToolStripMenuItem.Enabled = False
            TRVNameBox.ReadOnly = True
            ChangeCatOnResourseButton.Enabled = False
            ATRInWordFormat.Enabled = False
            StageListBox.Visible = False
            MakeStagelabel.Visible = False
        End If
    End Sub

    Public Sub ReadSetupFile(FolderPath As String)
        Try
            Using InputFile As New StreamReader(FolderPath & "\Databasinställningar.txt")
                Dim line As String
                While Not (InputFile.EndOfStream)
                    line = InputFile.ReadLine
                    If StrComp(line, "Superuser") = 0 Then
                        While StrComp(line, "Slut superuser") <> 0
                            line = InputFile.ReadLine
                            If StrComp(line.ToUpper, Environment.UserName.ToUpper) = 0 Then
                                SuperUser = True
                            End If
                        End While
                    End If

                    line = InputFile.ReadLine
                    If StrComp(line, "Deluppdragsnamn och deluppdragssiffra") = 0 Then
                        line = InputFile.ReadLine
                        While StrComp(line, "Slut deluppdrag") <> 0
                            Dim StageText As String = ""
                            Dim StageNr As String = ""
                            StageText = FindInString(line, "$")
                            StageNr = line
                            StageListBox.Items.Add(StageText)
                            StageNumberList.Add(StageNr)
                            line = InputFile.ReadLine
                        End While
                    End If

                    line = InputFile.ReadLine
                    If StrComp(line, "WBS I IDA") = 0 Then
                        While StrComp(line, "Slut WBS") <> 0
                            WBSLink = line
                            line = InputFile.ReadLine
                        End While
                    End If

                    line = InputFile.ReadLine
                    If StrComp(line, "Latund i IDA") = 0 Then
                        While StrComp(line, "Slut Lathund") <> 0
                            HelpLink = line
                            line = InputFile.ReadLine
                        End While
                    End If

                    line = InputFile.ReadLine
                    If StrComp(line, "ITLeverans") = 0 Then
                        line = InputFile.ReadLine
                        While StrComp(line, "Slut ITLeverans") <> 0
                            ITInfoFromFile.Add(line)
                            line = InputFile.ReadLine
                        End While
                    End If

                    line = InputFile.ReadLine
                    If StrComp(line, "WordMall") = 0 Then
                        line = InputFile.ReadLine
                        While StrComp(line, "Slut WordMall") <> 0
                            TRVMallDoc.TemplatePath = line
                            line = InputFile.ReadLine
                        End While
                    End If

                    line = InputFile.ReadLine
                    If StrComp(line, "FilnamnsKonvention") = 0 Then
                        line = InputFile.ReadLine
                        While StrComp(line, "Slut FilnamnsKonvention") <> 0
                            FileNameType = line
                            line = InputFile.ReadLine
                        End While
                    End If
                End While
            End Using
        Catch ex As Exception
            MakeErrorReport("ReadSetupFile", ex)
        End Try
    End Sub

    Private Sub UpdateATRResourceGridView(ByRef TotalCost As Integer)
        UpdateATRResourecView(TotalCost)
    End Sub

    Private Sub UpdateATRResourecView(ByRef totalcost As Integer)
        Try
            Dim ResoucreRow As DataRow
            Dim CatRow As DataRow
            Dim FindResourceKey(1) As Object
            If ATRResourcesGrid.Rows.Count > 0 Then
                For Each Row As DataGridViewRow In ATRResourcesGrid.Rows
                    If StrComp(Row.Cells("FK_RID").Value, "") <> 0 Then
                        FindResourceKey(0) = Row.Cells("FK_RID").Value
                        FindResourceKey(1) = Row.Cells("FK_CatID").Value
                        ResoucreRow = DataSet.Tables("Resource").Rows.Find(FindResourceKey)

                        CatRow = DataSet.Tables("Cat").Rows.Find(Row.Cells("FK_CatID").Value)
                        Row.Cells("CatName").Value = ResoucreRow.Item("RoleText") & " (" & CatRow.Item("Cost") & " kr)"
                        Row.Cells("Name").Value = ResoucreRow.Item("FreeText")
                        Row.Cells("Calc").Value = Row.Cells("TimeH").Value * CatRow.Item("Cost")
                        totalcost = totalcost + CInt(Row.Cells("Calc").Value)

                    End If
                Next
            End If
        Catch ex As Exception

        End Try
    End Sub

    Private Sub ATRResourcesGrid_Sorted(sender As Object, e As EventArgs) Handles ATRResourcesGrid.Sorted
        Dim i As Integer = 0
        i = 1
        UpdateATRResourceGridView(i)
    End Sub

    Private Sub ImporteraWBSToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles ImporteraWBSToolStripMenuItem.Click
        Try
            WBSFileDialog.ShowDialog()
            ImportInitExcel()
            ImportOpenExcelSheet(WBSFileDialog.FileName)
            ImportWBSData(DataSet)
            ImportCloseExcel()
            MsgBox("Klar med import av WBS")
        Catch ex As Exception
            MakeErrorReport("ImporteraWBSToolStripMenuItem_Click: ", ex)
            MsgBox("Fel vid inläsning av WBS-data")
        End Try
    End Sub

    Private Sub DeleteATR_Click(sender As Object, e As EventArgs) Handles DeleteATR.Click
        Try
            Dim ATRNr As String = ""
            Dim MsgBoxResult As MsgBoxResult = MsgBox("Skall aktivt ATR-blad verkligen raderas?", MsgBoxStyle.OkCancel)
            If MsgBoxResult = Microsoft.VisualBasic.MsgBoxResult.Ok Then
                Dim i As Integer = 0
                Dim FilterString As String = "FK_ID ='" & ATRnumber.SelectedItem.item("ID") & "'"
                Dim DataRows() As DataRow = DataSet.Tables("Risks").Select(FilterString)

                For i = 0 To DataRows.Length - 1
                    DataRows(i).Delete()
                Next
                InsertTable(DataSet, "Risks")

                DataRows = DataSet.Tables("Delivieries").Select(FilterString)
                For i = 0 To DataRows.Length - 1
                    DataRows(i).Delete()
                Next
                InsertTable(DataSet, "Delivieries")

                DataRows = DataSet.Tables("Results").Select(FilterString)
                For i = 0 To DataRows.Length - 1
                    DataRows(i).Delete()
                Next
                InsertTable(DataSet, "Results")

                DataRows = DataSet.Tables("Inputs").Select(FilterString)
                For i = 0 To DataRows.Length - 1
                    DataRows(i).Delete()
                Next
                InsertTable(DataSet, "Inputs")

                DataRows = DataSet.Tables("FixedCost").Select(FilterString)
                For i = 0 To DataRows.Length - 1
                    DataRows(i).Delete()
                Next
                InsertTable(DataSet, "FixedCost")

                FilterString = "ID ='" & ATRnumber.SelectedItem.item("ID") & "'"
                DataRows = DataSet.Tables("ATRMemo").Select(FilterString)
                For i = 0 To DataRows.Length - 1
                    DataRows(i).Delete()
                Next
                InsertTable(DataSet, "ATRMemo")

                FilterString = "FK_ATRID ='" & ATRnumber.SelectedItem.item("ID") & "'"
                DataRows = DataSet.Tables("AReCatJunc").Select(FilterString)
                For i = 0 To DataRows.Length - 1
                    DataRows(i).Delete()
                Next
                InsertTable(DataSet, "AReCatJunc")

                ATRNr = ATRnumber.SelectedItem.item("ID")
                FilterString = "ID ='" & ATRnumber.SelectedItem.item("ID") & "'"
                DataRows = DataSet.Tables("ATR").Select(FilterString)
                For i = 0 To DataRows.Length - 1
                    DataRows(i).Delete()
                Next

                InsertTable(DataSet, "ATR")
            End If
            MsgBox("ATRblad raderat")
            MakeLogUpdate("ATRblad: " & ATRNr & "har raderats av:" & Environment.UserName & ", Tid: " & System.DateTime.Now)
        Catch ex As Exception
            MakeErrorReport("DeleteATR: ", ex)
            MsgBox("Fel vid raderin gav ATRblad")
        End Try

    End Sub

    Private Sub CreatePrimaveraSubTableList(ListName As String)
        Try
            Dim DV As New DataView

            Dim ListTable As New DataTable

            Dim SortString As String = "FK_ID ASC"

            DV.Table = DataSet.Tables(ListName)
            DV.Sort = SortString
            ListTable = DV.ToTable

            ListInitExcel()
            Select Case ListName
                Case "Inputs"
                    ListOpenExcelSheet(MainFolder & "\PrimaveraInputs.xlsx")
                Case "Delivieries"
                    ListOpenExcelSheet(MainFolder & "\PrimaveraOutput.xlsx")
            End Select
            ListCreatePrimaveraSubTableList(ListTable)
            ListExitWorkBook()
            ListExitExcel()
        Catch ex As Exception
            MakeErrorReport("CreatePrimaveraSubTableList: ", ex)
            Throw New ApplicationException("CreatePrimaveraSubTableList: " & ListName & ": ")
        End Try

    End Sub

    Private Sub ChangeCatOnResourseButton_Click(sender As Object, e As EventArgs) Handles ChangeCatOnResourseButton.Click
        Try
            Dim Row As DataGridViewRow = ResourceGrid.CurrentRow
            Dim MsgBoxResult As MsgBoxResult = MsgBox("Skall aktivt resurs verkligen få ny kategori?" &
                                                      Environment.NewLine &
                                                      Row.Cells("FreeText").Value, MsgBoxStyle.OkCancel)
            If MsgBoxResult = Microsoft.VisualBasic.MsgBoxResult.Ok Then
                Dim NewCat As String
                Dim i As Integer = 0
                Dim FilterString As String
                Dim ResultRows() As DataRow

                FilterString = "FK_CatID='" & Row.Cells("CatID").Value & "' AND FK_RID='" & Row.Cells("ID").Value & "'"
                ResultRows = DataSet.Tables("AReCatJunc").Select(FilterString)
                NewCat = InputBox("Ny Kategorinummer", , Row.Cells("CatID").Value)

                Dim NewDataGridRow As DataRow = DataSet.Tables("Resource").NewRow
                NewDataGridRow.Item("ID") = Row.Cells("ID").Value
                NewDataGridRow.Item("FreeText") = Row.Cells("Freetext").Value
                NewDataGridRow.Item("FK_ID") = Row.Cells("FK_ID").Value
                NewDataGridRow.Item("RoleText") = Row.Cells("RoleText").Value
                NewDataGridRow.Item("CatID") = NewCat
                DataSet.Tables("Resource").Rows.Add(NewDataGridRow)
                InsertTable(DataSet, "Resource")
                For Each ATRRow As DataRow In ResultRows
                    Dim NewDataRow As DataRow = DataSet.Tables("AReCatJunc").NewRow
                    NewDataRow.Item("FK_RID") = ATRRow.Item("FK_RID")
                    NewDataRow.Item("FK_ATRID") = ATRRow.Item("FK_ATRID")
                    NewDataRow.Item("FK_CatID") = NewCat
                    NewDataRow.Item("TimeH") = ATRRow.Item("TimeH")

                    DataSet.Tables("AReCatJunc").Rows.Add(NewDataRow)
                    ATRRow.Delete()
                Next
                InsertTable(DataSet, "AReCatJunc")
                FilterString = "CatID='" & Row.Cells("CatID").Value & "' AND ID='" & Row.Cells("ID").Value & "'"
                ResultRows = DataSet.Tables("Resource").Select(FilterString)


                For Each ReRow As DataRow In ResultRows
                    ReRow.Delete()
                Next
                InsertTable(DataSet, "Resource")
                'ResourceGrid.Update()
                MsgBox("Byte av kategori är klart")
            End If
        Catch ex As Exception
            MakeErrorReport("Kategoribyte: ", ex)
            MsgBox("Fel vid byte av kategori")
        End Try
    End Sub

    Private Sub CreateTimeCatSubProjectList()
        Dim TmpString As String = ""
        Dim SubProjectNumber As New DataColumn
        Dim SubProjectName As New DataColumn
        Dim SubProjectLeader As New DataColumn
        Dim ListTable As New DataTable
        Dim ListTableRow As DataRow

        Dim StageRows() As DataRow
        Dim ATRRows() As DataRow
        Dim ATRResourceRows() As DataRow

        Dim i As Integer = 0
        Dim j As Integer = 0
        Dim k As Integer = 0


        Dim StageFilter As String = ""
        Dim ATRFilter As String = ""
        Dim ATRResourceFilter As String = ""
        Dim ATRResourceSort As String = ""

        SubProjectNumber.DataType = System.Type.GetType("System.String")
        SubProjectNumber.ColumnName = "SubProjectNumber"
        ListTable.Columns.Add(SubProjectNumber)

        SubProjectName.DataType = System.Type.GetType("System.String")
        SubProjectName.ColumnName = "SubProjectName"
        ListTable.Columns.Add(SubProjectName)



        Dim CatTotalResultList As New List(Of Integer)
        For Each CatRow As DataRow In DataSet.Tables("Cat").Rows
            CatTotalResultList.Add(0)
            Dim NewColumn As New DataColumn
            NewColumn.DataType = System.Type.GetType("System.Int32")
            NewColumn.ColumnName = CatRow.Item("ID")
            ListTable.Columns.Add(NewColumn)
        Next

        For Each SubProjectRow As DataRow In DataSet.Tables("SubProject").Rows

            ListTableRow = ListTable.NewRow
            For Each Row As DataRow In DataSet.Tables("Cat").Rows
                ListTableRow.Item(Row.Item("ID").ToString) = 0
            Next

            ListTableRow.Item("SubProjectNumber") = SubProjectRow.Item("ID")
            ListTableRow.Item("SubprojectName") = SubProjectRow.Item("TableName")
            StageFilter = "FK_ID = '" & SubProjectRow.Item("ID") & "'"
            StageRows = DataSet.Tables("Stage").Select(StageFilter)
            For i = 0 To StageRows.Length - 1
                ATRFilter = "FK_ID =" & StageRows(i).Item("ID")
                ATRRows = DataSet.Tables("ATR").Select(ATRFilter)
                For j = 0 To ATRRows.Length - 1
                    ATRResourceFilter = "FK_ATRID = '" & ATRRows(j).Item("ID") & "'"
                    ATRResourceSort = "FK_CatID ASC"
                    ATRResourceRows = DataSet.Tables("AReCatJunc").Select(ATRResourceFilter, ATRResourceSort)
                    For k = 0 To ATRResourceRows.Length - 1
                        If IsDBNull(ATRResourceRows(k).Item("TimeH")) = False Then
                            CatTotalResultList(ATRResourceRows(k).Item("FK_CatID") - 1) = CatTotalResultList(ATRResourceRows(k).Item("FK_CatID") - 1) + ATRResourceRows(k).Item("TimeH")
                            ListTableRow.Item(ATRResourceRows(k).Item("FK_CatID").ToString) = ListTableRow.Item(ATRResourceRows(k).Item("FK_CatID").ToString) + ATRResourceRows(k).Item("TimeH")
                        End If

                    Next
                    k = 0
                Next
                j = 0
            Next
            ListTable.Rows.Add(ListTableRow)
            i = 0
        Next
        ListTableRow = ListTable.NewRow
        ListTableRow.Item("SubProjectNumber") = " "
        ListTableRow.Item("SubprojectName") = "Totalt alla huvuduppdrag"
        For i = 0 To CatTotalResultList.Count - 1
            TmpString = i + 1
            ListTableRow.Item(TmpString) = CatTotalResultList(i)
        Next
        ListTable.Rows.Add(ListTableRow)

        ListInitExcel()
        ListOpenExcelSheet(MainFolder & "\Timmar-Kategori-Delprojekt.xlsx")
        ListCreateTimeCatExcel(ListTable, DataSet.Tables("Cat"))
        ListExitWorkBook()
        ListExitExcel()
    End Sub

    Private Sub CreateTimeCatATRList()
        Dim TmpString As String = ""
        Dim SubProjectNumber As New DataColumn
        Dim SubProjectName As New DataColumn
        Dim SubProjectLeader As New DataColumn
        Dim ATRID As New DataColumn
        Dim ATRName As New DataColumn
        Dim ATR_TRVNumber As New DataColumn
        Dim ListTable As New DataTable
        Dim ListTableRow As DataRow

        Dim StageRows() As DataRow
        Dim ATRRows() As DataRow
        Dim ATRResourceRows() As DataRow

        Dim i As Integer = 0
        Dim j As Integer = 0
        Dim k As Integer = 0


        Dim StageFilter As String = ""
        Dim ATRFilter As String = ""
        Dim ATRResourceFilter As String = ""
        Dim ATRResourceSort As String = ""

        SubProjectNumber.DataType = System.Type.GetType("System.String")
        SubProjectNumber.ColumnName = "SubProjectNumber"
        ListTable.Columns.Add(SubProjectNumber)

        SubProjectName.DataType = System.Type.GetType("System.String")
        SubProjectName.ColumnName = "SubProjectName"
        ListTable.Columns.Add(SubProjectName)

        ATRID.DataType = System.Type.GetType("System.String")
        ATRID.ColumnName = "ATRID"
        ListTable.Columns.Add(ATRID)

        ATRName.DataType = System.Type.GetType("System.String")
        ATRName.ColumnName = "ATRName"
        ListTable.Columns.Add(ATRName)

        ATR_TRVNumber.DataType = System.Type.GetType("System.String")
        ATR_TRVNumber.ColumnName = "ATR_TRV"
        ListTable.Columns.Add(ATR_TRVNumber)

        For Each CatRow As DataRow In DataSet.Tables("Cat").Rows
            Dim NewColumn As New DataColumn
            NewColumn.DataType = System.Type.GetType("System.Int32")
            NewColumn.ColumnName = CatRow.Item("ID")
            ListTable.Columns.Add(NewColumn)
        Next

        For Each SubProjectRow As DataRow In DataSet.Tables("SubProject").Rows
            StageFilter = "FK_ID = '" & SubProjectRow.Item("ID") & "'"
            StageRows = DataSet.Tables("Stage").Select(StageFilter)
            For i = 0 To StageRows.Length - 1

                ATRFilter = "FK_ID =" & StageRows(i).Item("ID")
                ATRRows = DataSet.Tables("ATR").Select(ATRFilter)

                For j = 0 To ATRRows.Length - 1
                    ListTableRow = ListTable.NewRow
                    For Each Row As DataRow In DataSet.Tables("Cat").Rows
                        ListTableRow.Item(Row.Item("ID").ToString) = 0
                    Next

                    ListTableRow.Item("SubProjectNumber") = SubProjectRow.Item("ID")
                    ListTableRow.Item("SubprojectName") = SubProjectRow.Item("TableName")
                    ListTableRow.Item("ATRID") = ATRRows(j).Item("ID")
                    ListTableRow.Item("ATRName") = ATRRows(j).Item("ATRName")
                    ListTableRow.Item("ATR_TRV") = ATRRows(j).Item("TRVNumber")


                    ATRResourceFilter = "FK_ATRID = '" & ATRRows(j).Item("ID") & "'"
                    ATRResourceSort = "FK_CatID ASC"
                    ATRResourceRows = DataSet.Tables("AReCatJunc").Select(ATRResourceFilter, ATRResourceSort)
                    For k = 0 To ATRResourceRows.Length - 1
                        If IsDBNull(ATRResourceRows(k).Item("TimeH")) = False Then
                            ListTableRow.Item(ATRResourceRows(k).Item("FK_CatID").ToString) = ListTableRow.Item(ATRResourceRows(k).Item("FK_CatID").ToString) + ATRResourceRows(k).Item("TimeH")
                        End If

                    Next
                    k = 0
                    ListTable.Rows.Add(ListTableRow)
                Next
                j = 0
            Next
            i = 0
        Next

        ListInitExcel()
        ListOpenExcelSheet(MainFolder & "\Timmar-Kategori-ATR.xlsx")
        ListCreateTimeCatATRExcel(ListTable, DataSet.Tables("Cat"))
        ListExitWorkBook()
        ListExitExcel()
    End Sub

    Private Sub CreateRiskList()
        Dim StageRows() As DataRow
        Dim ATRRows() As DataRow
        Dim DeliveryRows() As DataRow
        Dim StageFilter As String = ""
        Dim ATRFilter As String = ""
        Dim DeliveryFilter As String

        Dim ListTable As New DataTable
        Dim MainNumberColumn As New DataColumn
        Dim MainNameColumn As New DataColumn
        Dim ATRNumberColumn As New DataColumn
        Dim SwecoNumberColumn As New DataColumn
        Dim ATRNameColumn As New DataColumn
        Dim LeaderColumn As New DataColumn
        Dim ATRLeaderColumn As New DataColumn
        Dim FreeText1Column As New DataColumn
        Dim FreeText2Column As New DataColumn

        MainNumberColumn.DataType = System.Type.GetType("System.String")
        MainNumberColumn.ColumnName = "MainNumber"
        ListTable.Columns.Add(MainNumberColumn)

        MainNameColumn.DataType = System.Type.GetType("System.String")
        MainNameColumn.ColumnName = "MainName"
        ListTable.Columns.Add(MainNameColumn)

        ATRNumberColumn.DataType = System.Type.GetType("System.String")
        ATRNumberColumn.ColumnName = "ATRNumber"
        ListTable.Columns.Add(ATRNumberColumn)

        SwecoNumberColumn.DataType = System.Type.GetType("System.String")
        SwecoNumberColumn.ColumnName = "SwecoNumber"
        ListTable.Columns.Add(SwecoNumberColumn)

        ATRNameColumn.DataType = System.Type.GetType("System.String")
        ATRNameColumn.ColumnName = "ATRName"
        ListTable.Columns.Add(ATRNameColumn)

        LeaderColumn.DataType = System.Type.GetType("System.String")
        LeaderColumn.ColumnName = "Leader"
        ListTable.Columns.Add(LeaderColumn)

        ATRLeaderColumn.DataType = System.Type.GetType("System.String")
        ATRLeaderColumn.ColumnName = "ATRLeader"
        ListTable.Columns.Add(ATRLeaderColumn)

        FreeText1Column.DataType = System.Type.GetType("System.String")
        FreeText1Column.ColumnName = "FreeText1"
        ListTable.Columns.Add(FreeText1Column)

        FreeText2Column.DataType = System.Type.GetType("System.String")
        FreeText2Column.ColumnName = "FreeText2"
        ListTable.Columns.Add(FreeText2Column)


        For Each SubProjectRow As DataRow In DataSet.Tables("SubProject").Rows
            StageFilter = "FK_ID = '" & SubProjectRow.Item("ID") & "'"
            StageRows = DataSet.Tables("Stage").Select(StageFilter)
            StageRows = DataSet.Tables("Stage").Select(StageFilter)
            For i = 0 To StageRows.Length - 1
                ATRFilter = "FK_ID =" & StageRows(i).Item("ID")
                ATRRows = DataSet.Tables("ATR").Select(ATRFilter)
                For j = 0 To ATRRows.Length - 1
                    DeliveryFilter = "FK_ID = '" & ATRRows(j).Item("ID") & "'"
                    DeliveryRows = DataSet.Tables("Risks").Select(DeliveryFilter)


                    For k = 0 To DeliveryRows.Length - 1
                        Dim TableRow As DataRow = ListTable.NewRow
                        TableRow.Item("MainNumber") = SubProjectRow.Item("ID")
                        TableRow.Item("MainName") = SubProjectRow.Item("TableName")
                        TableRow.Item("Leader") = SubProjectRow.Item("Leader")
                        TableRow.Item("SwecoNumber") = ATRRows(j).Item("TRVNumber")
                        TableRow.Item("ATRNumber") = ATRRows(j).Item("ID")
                        TableRow.Item("ATRName") = ATRRows(j).Item("ATRName")
                        TableRow.Item("ATRLeader") = ATRRows(j).Item("Leader")
                        TableRow.Item("Freetext1") = DeliveryRows(k).Item("FreeText1")
                        TableRow.Item("Freetext2") = DeliveryRows(k).Item("FreeText2")
                        ListTable.Rows.Add(TableRow)
                    Next
                Next
            Next
        Next


        ListInitExcel()
        ListOpenExcelSheet(MainFolder & "\Risklista.xlsx")
        ListCreateRiskListExcel(ListTable)
        ListExitWorkBook()
        ListExitExcel()
    End Sub

    Private Sub ProjectNumber_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ProjectNumber.SelectedIndexChanged
        Try
            Dim FilterString As String
            Dim ResultRows() As DataRow
            FilterString = "ID= '" & CInt(ProjectNumber.SelectedItem.item("ID")) & "'"
            ProjectTableView.RowFilter = FilterString
            FilterString = "FK_ID='" & ProjectNumber.SelectedItem.item("ID") & "'"
            ResourceTableView.RowFilter = FilterString

            CatTableView.RowFilter = FilterString

            '
            SubTableMainView.RowFilter = FilterString
            SubprojectNumber.Refresh()
            FilterString = "ID='" & SubprojectNumber.SelectedItem.item("ID") & "'"
            SubTableSubView.RowFilter = FilterString


            FilterString = "FK_ID='" & SubprojectNumber.SelectedItem.item("ID") & "'"
            StageTableMainView.RowFilter = FilterString
            StageComboBox.Refresh()
            StageTableSubView.RowFilter = "IndexName = '" & StageComboBox.Text & "' AND FK_ID='" & SubprojectNumber.Text & "'"

            FilterString = "IndexName = '" & StageComboBox.Text & "' AND FK_ID='" & SubprojectNumber.Text & "'"
            ResultRows = DataSet.Tables("Stage").Select(FilterString)
            StageID = ResultRows(0).Item("ID")
            StageNumber = ResultRows(0).Item("SubNr")
            ATRTableMainView.RowFilter = "FK_ID ='" & StageID & "'"
            ATRTableMainView.Sort = "ID ASC"
            ATRnumber.Refresh()
            ATRTableSubView.RowFilter = "ID = '" & ATRnumber.SelectedItem.item("ID") & "'"
            FilterOnATRNumber(ATRnumber.SelectedItem.item("ID"))
            ATRLink.Text = ProjectName.Text & " - " & SubProjectName.Text & " - " & StageComboBox.Text
            GenATRnumber.Text = Strings.Left(SubprojectNumber.Text, 7) & StageNumber & "XX"
        Catch ex As Exception
            'MakeErrorReport("ProjectNumber_SelectedIndexChanged:", ex)
        End Try
    End Sub

    Private Sub SubprojectNumber_SelectedIndexChanged(sender As Object, e As EventArgs) Handles SubprojectNumber.SelectedIndexChanged
        Try
            'UpdateAllTables(DataSet)
            Dim FilterString As String
            FilterString = "ID='" & SubprojectNumber.SelectedItem.item("ID") & "'"
            SubTableSubView.RowFilter = FilterString

            FilterString = "FK_ID='" & SubprojectNumber.SelectedItem.item("ID") & "'"
            StageTableMainView.RowFilter = FilterString

            FilterString = "IndexName = '" & StageComboBox.SelectedItem.item("IndexName") & "' AND FK_ID='" & SubprojectNumber.SelectedItem.item("ID") & "'"
            StageTableSubView.RowFilter = FilterString

            StageID = StageComboBox.SelectedItem.Item("ID")
            StageNumber = StageComboBox.SelectedItem.Item("SubNr")
            ATRTableMainView.RowFilter = "FK_ID ='" & StageID & "'"
            ATRTableMainView.Sort = "ID ASC"

            FilterOnATRNumber(ATRnumber.SelectedItem.item("ID"))
            ATRTableSubView.RowFilter = "ID = '" & ATRnumber.SelectedItem.item("ID") & "'"
            FilterOnATRNumber(ATRnumber.SelectedItem.item("ID"))
            ATRLink.Text = ProjectNumber.SelectedItem.item("ProjectName") & " - " & SubprojectNumber.SelectedItem.item("TableName") & " - " & StageComboBox.SelectedItem.item("IndexName")
            GenATRnumber.Text = Strings.Left(SubprojectNumber.SelectedItem.item("ID"), 7) & StageNumber & "XX"

        Catch ex As Exception
            'MakeErrorReport("SubprojectNumber_SelectedIndexChanged:", ex)
        End Try
    End Sub

    Private Sub StageComboBox_SelectedIndexChanged(sender As Object, e As EventArgs) Handles StageComboBox.SelectedIndexChanged
        Try
            Dim FilterString As String
            FilterString = "IndexName = '" & StageComboBox.SelectedItem.item("IndexName") & "' AND FK_ID='" & SubprojectNumber.SelectedItem.item("ID") & "'"
            StageTableSubView.RowFilter = FilterString


            StageID = StageComboBox.SelectedItem.Item("ID")
            StageNumber = StageComboBox.SelectedItem.Item("SubNr")

            ATRTableMainView.RowFilter = "FK_ID ='" & StageID & "'"
            ATRTableMainView.Sort = "ID ASC"

            If ATRTableMainView.Count > 0 Then
                ATRTableSubView.RowFilter = "ID = '" & ATRnumber.SelectedItem.item("ID") & "'"
                FilterOnATRNumber(ATRnumber.SelectedItem.item("ID"))
            Else
                ATRTableSubView.RowFilter = "ID = '-1'"
                FilterOnATRNumber("-1")
            End If



            ATRLink.Text = ProjectNumber.SelectedItem.item("ProjectName") & " - " & SubprojectNumber.SelectedItem.item("TableName") & " - " & StageComboBox.SelectedItem.item("IndexName")
            GenATRnumber.Text = Strings.Left(SubprojectNumber.SelectedItem.item("ID"), 7) & StageNumber & "XX"

        Catch ex As Exception
            'MakeErrorReport("StageComboBox_SelectedIndexChanged:", ex)
        End Try

    End Sub

    Private Sub ATRnumber_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ATRnumber.SelectedIndexChanged
        Try
            If IsDBNull(ATRnumber.SelectedItem.item("ID")) = False Then
                FilterOnATRNumber(ATRnumber.SelectedItem.item("ID"))
            End If
        Catch ex As Exception

        End Try

    End Sub

    Private Sub FilterOnATRNumber(ATRString As String)
        Try
            ATRTableSubView.RowFilter = "ID = '" & ATRString & "'"
            InputTableView.RowFilter = "FK_ID='" & ATRString & "'"
            ResultTableView.RowFilter = "FK_ID='" & ATRString & "'"
            DelivieriesTableView.RowFilter = "FK_ID='" & ATRString & "'"
            RisksTableView.RowFilter = "FK_ID='" & ATRString & "'"
            AReCatJuncTableView.RowFilter = "FK_ATRID='" & ATRString & "'"
            ATRResourcesGrid.Refresh()
            FixedCostTableView.RowFilter = "FK_ID='" & ATRString & "'"
            ATRMemoTableView.RowFilter = "ID='" & ATRString & "'"
        Catch ex As Exception
            MakeErrorReport("FilterOnATRNumber:", ex)
        End Try
    End Sub

    Private Sub WBSIIDAToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles WBSIIDAToolStripMenuItem.Click
        System.Diagnostics.Process.Start(WBSLink)
    End Sub

    Private Sub ATRResourcesGrid_CellLeave(sender As Object, e As DataGridViewCellEventArgs)
        Try
            ShowWindow = True
        Catch ex As Exception
            MakeErrorReport("ATRResourcesGrid_CellLeave", ex)
        End Try
    End Sub

    Private Sub ATRResourcesGrid_RowLeave(sender As Object, e As DataGridViewCellEventArgs) Handles ATRResourcesGrid.RowLeave
        Try
            ShowWindow = True
        Catch ex As Exception
            MakeErrorReport("", ex)
        End Try
    End Sub

    Private Sub BoldText_Click(sender As Object, e As EventArgs) Handles BoldTextScope.Click
        Dim StartPos As Integer
        Dim EndPos As Integer
        Dim TmpString As String
        TmpString = ATRScope.SelectedText
        StartPos = ATRScope.Text.IndexOf(ATRScope.SelectedText)
        EndPos = StartPos + TmpString.Length

        ATRScope.Text = ATRScope.Text.Insert(EndPos, "</b>") ' skriv sista texten först för att inte skjuta på indexet
        ATRScope.Text = ATRScope.Text.Insert(StartPos, "<b>")
    End Sub

    Private Sub UnderLineText_Click(sender As Object, e As EventArgs) Handles UnderLineTextScope.Click
        Dim StartPos As Integer
        Dim EndPos As Integer
        Dim TmpString As String
        TmpString = ATRScope.SelectedText
        StartPos = ATRScope.Text.IndexOf(ATRScope.SelectedText)
        EndPos = StartPos + TmpString.Length

        ATRScope.Text = ATRScope.Text.Insert(EndPos, "</u>") ' skriv sista texten först för att inte skjuta på indexet
        ATRScope.Text = ATRScope.Text.Insert(StartPos, "<u>")
    End Sub

    Private Sub BoldTextResult_Click(sender As Object, e As EventArgs) Handles BoldTextResult.Click
        Dim StartPos As Integer
        Dim EndPos As Integer
        Dim TmpString As String
        TmpString = ResultText.SelectedText
        StartPos = ResultText.Text.IndexOf(ATRScope.SelectedText)
        EndPos = StartPos + TmpString.Length

        ResultText.Text = ResultText.Text.Insert(EndPos, "</b>") ' skriv sista texten först för att inte skjuta på indexet
        ResultText.Text = ResultText.Text.Insert(StartPos, "<b>")
    End Sub

    Private Sub UnderlineTextResult_Click(sender As Object, e As EventArgs) Handles UnderlineTextResult.Click
        Dim StartPos As Integer
        Dim EndPos As Integer
        Dim TmpString As String
        TmpString = ResultText.SelectedText
        StartPos = ResultText.Text.IndexOf(ATRScope.SelectedText)
        EndPos = StartPos + TmpString.Length

        ResultText.Text = ResultText.Text.Insert(EndPos, "</u>") ' skriv sista texten först för att inte skjuta på indexet
        ResultText.Text = ResultText.Text.Insert(StartPos, "<u>")
    End Sub

    Private Sub ATRResourcesGrid_DataBindingComplete(sender As Object, e As DataGridViewBindingCompleteEventArgs) Handles ATRResourcesGrid.DataBindingComplete
        Dim i As Integer = 0
        i = 0
        UpdateATRResourecView(i)
    End Sub

End Class
