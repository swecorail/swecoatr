﻿Imports Microsoft.Office.Interop.Excel
Imports Microsoft.Office.Interop.Word
Imports Microsoft.Office.Interop
Imports System.IO
Module ImportDataModul

    Private oExcelApp As Excel.Application
    Private oExcelWBook As Excel.Workbook
    Private oExcelWSheet As Excel.Worksheet
    Private ReadOnly ImportLog As String = "ImportLog.txt"
    'Private oExcel As Excel.Application
    Public Sub ImportWBSData(ByRef DSet As DataSet)
        Try
            Dim i As Integer = 3
            Dim ATRRows() As DataRow
            While StrComp(oExcelWSheet.Cells(i, 3).value, "") <> 0
                Dim Filterstring As String = "ID = '" & oExcelWSheet.Cells(i, 3).value & "'"
                ATRRows = DSet.Tables("ATR").Select(Filterstring)
                If ATRRows.Length = 1 Then
                    ATRRows(0).Item("TRVNumber") = oExcelWSheet.Cells(i, 4).value
                    ATRRows(0).EndEdit()
                ElseIf ATRRows.Length > 1 Then
                    ImportMakeLogUpdate(oExcelWSheet.Cells(i, 3).value & " Gav Flera rader")
                Else
                    ImportMakeLogUpdate(oExcelWSheet.Cells(i, 3).value & " Gav inga rader")
                End If
                i = i + 1
            End While
            InsertTable(DSet, "ATR")
        Catch ex As Exception
            MakeErrorReport("ImportWBSData: ", ex)
            oExcelWBook = Nothing
            oExcelWSheet = Nothing
            Runtime.InteropServices.Marshal.FinalReleaseComObject(oExcelApp)
            oExcelApp = Nothing
            Throw New ApplicationException("ImportDataError:")
        End Try
    End Sub
    Public Sub ImportInitExcel()
        Try
            oExcelApp = New Excel.Application
            oExcelApp.Visible = False
            oExcelApp.DisplayAlerts = WdAlertLevel.wdAlertsNone
        Catch ex As Exception
            MakeErrorReport("ImportDataModul InitExcel: Kan inte skapa/hantera object:", ex)
            Throw New ApplicationException("InitExcelError:")
            oExcelApp = Nothing
        End Try
    End Sub
    Public Sub ImportOpenExcelSheet(FilePath As String)
        Try
            oExcelWBook = oExcelApp.Workbooks.Open(FilePath)
            oExcelWSheet = oExcelWBook.ActiveSheet
        Catch ex As Exception
            MakeErrorReport("ImportOpenExcelSheet: ", ex)
            oExcelWBook = Nothing
            oExcelWSheet = Nothing
            Runtime.InteropServices.Marshal.FinalReleaseComObject(oExcelApp)
            oExcelApp = Nothing
            Throw New ApplicationException("OpenExcelSheetError")
        End Try
    End Sub

    Public Sub ImportCloseExcel()
        oExcelWBook.Close()
        oExcelApp.Quit()
        Runtime.InteropServices.Marshal.FinalReleaseComObject(oExcelApp)
    End Sub
    Public Sub ImportMakeLogUpdate(Action As String)
        Dim TxtString As String
        TxtString = Action & System.DateTime.Today & Environment.NewLine
        File.AppendAllText(Basicfunctions.MainFolder & "\" & ImportLog, TxtString)
    End Sub
End Module
