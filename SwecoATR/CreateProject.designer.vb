﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class CreateProject
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.ProjName = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Folder = New System.Windows.Forms.Label()
        Me.BrowseFolder = New System.Windows.Forms.Button()
        Me.SaveButton = New System.Windows.Forms.Button()
        Me.CreateProjectFolder = New System.Windows.Forms.FolderBrowserDialog()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(13, 30)
        Me.Label1.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(102, 20)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Projektnamn"
        '
        'ProjName
        '
        Me.ProjName.Location = New System.Drawing.Point(143, 26)
        Me.ProjName.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.ProjName.Name = "ProjName"
        Me.ProjName.Size = New System.Drawing.Size(370, 27)
        Me.ProjName.TabIndex = 1
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(13, 70)
        Me.Label3.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(63, 20)
        Me.Label3.TabIndex = 4
        Me.Label3.Text = "Sökväg"
        '
        'Folder
        '
        Me.Folder.AutoSize = True
        Me.Folder.Location = New System.Drawing.Point(139, 70)
        Me.Folder.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Folder.Name = "Folder"
        Me.Folder.Size = New System.Drawing.Size(14, 20)
        Me.Folder.TabIndex = 5
        Me.Folder.Text = " "
        '
        'BrowseFolder
        '
        Me.BrowseFolder.Location = New System.Drawing.Point(523, 66)
        Me.BrowseFolder.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.BrowseFolder.Name = "BrowseFolder"
        Me.BrowseFolder.Size = New System.Drawing.Size(94, 29)
        Me.BrowseFolder.TabIndex = 2
        Me.BrowseFolder.Text = "Sök"
        Me.BrowseFolder.UseVisualStyleBackColor = True
        '
        'SaveButton
        '
        Me.SaveButton.Location = New System.Drawing.Point(523, 120)
        Me.SaveButton.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.SaveButton.Name = "SaveButton"
        Me.SaveButton.Size = New System.Drawing.Size(94, 29)
        Me.SaveButton.TabIndex = 3
        Me.SaveButton.Text = "Spara"
        Me.SaveButton.UseVisualStyleBackColor = True
        '
        'CreateProject
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(10.0!, 20.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(639, 162)
        Me.Controls.Add(Me.SaveButton)
        Me.Controls.Add(Me.BrowseFolder)
        Me.Controls.Add(Me.Folder)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.ProjName)
        Me.Controls.Add(Me.Label1)
        Me.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Name = "CreateProject"
        Me.Text = "Nytt projekt"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents ProjName As System.Windows.Forms.TextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Folder As System.Windows.Forms.Label
    Friend WithEvents BrowseFolder As System.Windows.Forms.Button
    Friend WithEvents SaveButton As System.Windows.Forms.Button
    Friend WithEvents CreateProjectFolder As System.Windows.Forms.FolderBrowserDialog
End Class
