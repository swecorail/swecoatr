﻿
Imports System.Data.OleDb
Module ProjectTableModule
    ReadOnly Sql As String = "SELECT Project.* FROM Project;"
    Private adapter As OleDbDataAdapter
    Private InsertParamList As OleDbParameterCollection
    Private UpdateParamList As OleDbParameterCollection
    Private DeleteParamList As OleDbParameterCollection
    ''' <summary>
    ''' 
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub CreateProjectTable()
        Try
            Dim SQL As String
            SQL = "CREATE TABLE Project (" & _
                  "[ID] INTEGER," & _
                  "[ProjectName] TEXT(250)," & _
                  "[Leader] TEXT(250)," & _
                  "[ProjectOwner] TEXT(250)," & _
                  "[CustomerNr] INTEGER," & _
                  "PRIMARY KEY(ID))"
            DbCommand(SQL)
        Catch ex As Exception
            MakeErrorReport("CreateProjectTable", ex)
            Throw New ApplicationException("CreateTabelError")
        End Try
    End Sub
    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="con"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function CreateprojectDataAdaper(ByRef con As OleDbConnection) As OleDbDataAdapter

        adapter = New OleDbDataAdapter(Sql, con)
        adapter.MissingSchemaAction = MissingSchemaAction.AddWithKey

        adapter.InsertCommand = New OleDbCommand( _
            "INSERT INTO project (ID, ProjectName, Leader, ProjectOwner, CustomerNr)" & _
            "VALUES (?, ?, ?, ?, ?)")

        adapter.UpdateCommand = New OleDbCommand( _
            "UPDATE Project SET ProjectName = ? , Leader = ?, ProjectOwner = ?, CustomerNr = ?" & _
            "WHERE ID = ?")

        adapter.DeleteCommand = New OleDbCommand( _
            "DELETE From Project WHERE ID = ? ")

        adapter.InsertCommand.Parameters.Add( _
            "@ID", OleDbType.Integer, Integer.MaxValue, "ID")
        adapter.InsertCommand.Parameters.Add( _
            "@ProjectName", OleDbType.Char, 250, "ProjectName")
        adapter.InsertCommand.Parameters.Add( _
            "@Leader", OleDbType.Char, 250, "Leader")
        adapter.InsertCommand.Parameters.Add( _
            "@ProjectOwner", OleDbType.Char, 250, "ProjectOwner")
        adapter.InsertCommand.Parameters.Add( _
            "@CustomerNr", OleDbType.Integer, Integer.MaxValue, "CustomerNr")

        adapter.UpdateCommand.Parameters.Add( _
            "@ID", OleDbType.Integer, Integer.MaxValue, "ID")
        adapter.UpdateCommand.Parameters.Add( _
            "@ProjectName", OleDbType.Char, 250, "ProjectName")
        adapter.UpdateCommand.Parameters.Add( _
            "@Leader", OleDbType.Char, 250, "Leader")
        adapter.UpdateCommand.Parameters.Add( _
            "@ProjectOwner", OleDbType.Char, 250, "ProjectOwner")
        adapter.UpdateCommand.Parameters.Add( _
            "@CustomerNr", OleDbType.Integer, Integer.MaxValue, "CustomerNr")
        adapter.UpdateCommand.Parameters.Add( _
            "@ID", OleDbType.Integer, Integer.MaxValue, "ID").SourceVersion = _
        DataRowVersion.Original

        adapter.DeleteCommand.Parameters.Add( _
            "@ID", OleDbType.Integer, Integer.MaxValue, "ID").SourceVersion = _
        DataRowVersion.Original
        InsertParamList = adapter.InsertCommand.Parameters
        UpdateParamList = adapter.UpdateCommand.Parameters
        DeleteParamList = adapter.DeleteCommand.Parameters

        Return adapter
    End Function
    Public Function GetInsertParmeters() As OleDbParameterCollection
        Return InsertParamList
    End Function

    Public Sub SetInsertParmeters(ByVal InPut As OleDbParameterCollection)
        Dim i As Integer = 0
        For i = 0 To InPut.Count - 1
            InsertParamList.Item(i) = InPut.Item(i)
        Next
    End Sub

    Public Function GetUpdateParmeters() As OleDbParameterCollection
        Return UpdateParamList
    End Function

    Public Sub SetUpdateParmeters(ByVal InPut As OleDbParameterCollection)
        Dim i As Integer = 0
        For i = 0 To InPut.Count - 1
            UpdateParamList.Item(i) = InPut.Item(i)
        Next
    End Sub

    Public Function GetDeleteParmeters() As OleDbParameterCollection
        Return DeleteParamList
    End Function

    Public Sub SetDeleteParmeters(ByVal InPut As OleDbParameterCollection)
        Dim i As Integer = 0
        For i = 0 To InPut.Count - 1
            DeleteParamList.Item(i) = InPut.Item(i)
        Next
    End Sub
End Module
