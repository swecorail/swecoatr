﻿Imports System.IO

Public Class CreateProject
    Private Sub BrowseFolder_Click(sender As Object, e As EventArgs) Handles BrowseFolder.Click
        CreateProjectFolder.ShowDialog()
        Folder.Text = CreateProjectFolder.SelectedPath
    End Sub

    Private Sub SaveButton_Click(sender As Object, e As EventArgs) Handles SaveButton.Click
        Try
            If DBconnectionExit() = True Then
                'CloseDb()
            End If

            If StrComp(ProjName.Text, "") <> 0 And StrComp(Folder.Text, " ") <> 0 Then
                If File.Exists(Folder.Text & " \ " & ProjName.Text & ".accdb") = True Then
                    MsgBox("Projektet finns redan byt namn")
                    Throw New ApplicationException("Filen Finns redan")
                Else
                    MainForm.FilePath = Folder.Text & "\" & ProjName.Text & ".accdb"
                    Basicfunctions.MainFolder = System.IO.Path.GetDirectoryName(MainForm.FilePath)
                    MainForm.WordFolderLabel.Text = Basicfunctions.MainFolder
                    CreateDb(MainForm.FilePath)
                    MainForm.DataSet = ConnectDataSet()
                    MainForm.BindControls()
                    MainForm.ReadSetupFile(Basicfunctions.MainFolder)
                    MainForm.SuperUserSetup()
                End If
            Else
                MsgBox("projektet måste ha ett namn och/eller sökväg")
                Throw New ApplicationException("Projektet måste ha ett namn")
            End If
            Me.Close()
        Catch ex As Exception
            MakeErrorReport("Createproject Error", ex)
        End Try
    End Sub
End Class