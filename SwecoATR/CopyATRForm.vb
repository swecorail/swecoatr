﻿
Public Class CopyATRForm

    Private SubProjectMainView As New DataView
    Private SubProjectSubView As New DataView
    Private SubProjectDBinder As New BindingSource
    Private StageView As New DataView
    Private StageMainview As New DataView
    Private StageSubView As New DataView


    Private Sub SaveCopyATRButton_Click(sender As Object, e As EventArgs) Handles SaveCopyATRButton.Click
        Try
            Dim FilterString As String
            Dim ResultRows() As DataRow

            If Strings.Len(NewATRNumber.Text) = 10 Then
                If StrComp(Strings.Left(NewATRNumber.Text, 7), Strings.Left(GenATRNumber.Text, 7)) = 0 Then
                    FilterString = "FK_ID='" & NewSubProjectNr.Text & "' AND IndexName='" & StageName.Text & "'"
                    ResultRows = MainForm.DataSet.Tables("Stage").Select(FilterString)

                    Dim ATRRow As DataRow = MainForm.DataSet.Tables("ATR").NewRow
                    Dim i As Integer = 0
                    ATRRow.Item("ID") = NewATRNumber.Text
                    ATRRow.Item("ATRName") = NewATRName.Text
                    ATRRow.Item("FK_ID") = ResultRows(0).Item("ID")
                    ATRRow.Item("Leader") = MainForm.ATRLeader.Text
                    ATRRow.Item("Scope") = MainForm.ATRScope.Text
                    ATRRow.Item("StartDate") = MainForm.StartDate.Value
                    ATRRow.Item("EndDate") = MainForm.EndDate.Value
                    ATRRow.Item("Version") = "00.01"
                    ATRRow.Item("RevDate") = System.DateTime.Today
                    ATRRow.Item("RevSign") = Environment.UserName
                    MainForm.DataSet.Tables("ATR").Rows.Add(ATRRow)
                    InsertTable(MainForm.DataSet, "ATR")

                    ''''''************************  INPUTS *********************'''''''''
                    FilterString = "FK_ID='" & OrginalATRNumber.Text & "'"
                    ResultRows = MainForm.DataSet.Tables("Inputs").Select(FilterString)
                    For Each Row As DataRow In ResultRows
                        'For Each InputRow As DataRow In MainForm.MiniSet.Tables("Inputs").Rows
                        Dim NewInputRow As DataRow = MainForm.DataSet.Tables("Inputs").NewRow
                        NewInputRow.Item("FreeText1") = Row.Item("FreeText1")
                        NewInputRow.Item("FreeText2") = Row.Item("FreeText2")
                        NewInputRow.Item("FreeText3") = Row.Item("FreeText3")
                        NewInputRow.Item("FK_ID") = NewATRNumber.Text
                        MainForm.DataSet.Tables("Inputs").Rows.Add(NewInputRow)
                    Next
                    InsertTable(MainForm.DataSet, "Inputs")

                    ''''''************************  Delivieries *********************'''''''''
                    FilterString = "FK_ID='" & OrginalATRNumber.Text & "'"
                    ResultRows = MainForm.DataSet.Tables("Delivieries").Select(FilterString)
                    For Each Row As DataRow In ResultRows
                        Dim NewDelivieryRow As DataRow = MainForm.DataSet.Tables("Delivieries").NewRow
                        NewDelivieryRow.Item("FreeText1") = Row.Item("FreeText1")
                        NewDelivieryRow.Item("FreeText2") = Row.Item("FreeText2")
                        NewDelivieryRow.Item("FreeText3") = Row.Item("FreeText3")
                        NewDelivieryRow.Item("FK_ID") = NewATRNumber.Text
                        MainForm.DataSet.Tables("Delivieries").Rows.Add(NewDelivieryRow)
                    Next
                    InsertTable(MainForm.DataSet, "Delivieries")

                    ''''''************************  Results *********************'''''''''
                    FilterString = "FK_ID='" & OrginalATRNumber.Text & "'"
                    ResultRows = MainForm.DataSet.Tables("Results").Select(FilterString)
                    For Each Row As DataRow In ResultRows
                        Dim NewResultsRow As DataRow = MainForm.DataSet.Tables("Results").NewRow
                        NewResultsRow.Item("FreeText1") = Row.Item("FreeText1")
                        NewResultsRow.Item("FreeText2") = Row.Item("FreeText2")
                        NewResultsRow.Item("FreeText3") = Row.Item("FreeText3")
                        NewResultsRow.Item("FK_ID") = NewATRNumber.Text
                        MainForm.DataSet.Tables("Results").Rows.Add(NewResultsRow)
                    Next
                    InsertTable(MainForm.DataSet, "Results")

                    ''''''************************  Risks *********************'''''''''
                    FilterString = "FK_ID='" & OrginalATRNumber.Text & "'"
                    ResultRows = MainForm.DataSet.Tables("Risks").Select(FilterString)
                    For Each Row As DataRow In ResultRows
                        Dim NewRiskRow As DataRow = MainForm.DataSet.Tables("Risks").NewRow
                        NewRiskRow.Item("FreeText1") = Row.Item("FreeText1")
                        NewRiskRow.Item("FreeText2") = Row.Item("FreeText2")
                        NewRiskRow.Item("FreeText3") = Row.Item("FreeText3")
                        NewRiskRow.Item("FK_ID") = NewATRNumber.Text
                        MainForm.DataSet.Tables("Risks").Rows.Add(NewRiskRow)
                    Next
                    InsertTable(MainForm.DataSet, "Risks")

                    FilterString = "ID='" & OrginalATRNumber.Text & "'"
                    ResultRows = MainForm.DataSet.Tables("ATRMemo").Select(FilterString)
                    For Each Row As DataRow In ResultRows
                        Dim NewMemoRow As DataRow = MainForm.DataSet.Tables("ATRMemo").NewRow
                        NewMemoRow.Item("FreeText") = Row.Item("FreeText")
                        NewMemoRow.Item("ID") = NewATRNumber.Text
                        MainForm.DataSet.Tables("ATRMemo").Rows.Add(NewMemoRow)
                    Next
                    InsertTable(MainForm.DataSet, "ATRMemo")


                    ''''''************************  ATRResources *********************'''''''''
                    FilterString = "FK_ATRID='" & OrginalATRNumber.Text & "'"
                    ResultRows = MainForm.DataSet.Tables("AReCatJunc").Select(FilterString)
                    For Each Row As DataRow In ResultRows
                        Dim NewInputRow As DataRow = MainForm.DataSet.Tables("AReCatJunc").NewRow
                        NewInputRow.Item("FK_RID") = Row.Item("FK_RID")
                        NewInputRow.Item("FK_CatID") = Row.Item("FK_CatID")
                        NewInputRow.Item("TimeH") = Row.Item("TimeH")
                        NewInputRow.Item("FK_ATRID") = NewATRNumber.Text
                        MainForm.DataSet.Tables("AReCatJunc").Rows.Add(NewInputRow)
                    Next
                    InsertTable(MainForm.DataSet, "AReCatJunc")

                    ''''''************************  Fixed Cost *********************'''''''''
                    FilterString = "FK_ID='" & OrginalATRNumber.Text & "'"
                    ResultRows = MainForm.DataSet.Tables("FixedCost").Select(FilterString)
                    For Each Row As DataRow In ResultRows
                        Dim NewInputRow As DataRow = MainForm.DataSet.Tables("FixedCost").NewRow
                        NewInputRow.Item("FreeText") = Row.Item("FreeText")
                        NewInputRow.Item("Cost") = Row.Item("Cost")
                        NewInputRow.Item("FK_ID") = NewATRNumber.Text
                        MainForm.DataSet.Tables("FixedCost").Rows.Add(NewInputRow)
                    Next
                    InsertTable(MainForm.DataSet, "FixedCost")

                    MsgBox("ATR är kopierad")
                    Me.Close()
                Else
                    MsgBox("ATR-bladsnummer stämmer inte med föreslaget ATR-bladnummer" & Environment.NewLine & _
                            GenATRNumber.Text)
                End If
            Else
                MsgBox("ATR nummret skall vara 10 siffor")
            End If
        Catch ex As Exception
            MakeErrorReport("SaveCopyATRButton_Click: ", ex)
            MsgBox("Fel vid kopiering av ATR" & Environment.NewLine & _
                   "Det nya ATRnumret får inte finnas sedan tidigare i databasen")
        End Try

    End Sub

    Private Sub CopyATRForm_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        NewProjectNr.DataSource = MainForm.DataSet.Tables("Project")
        NewProjectNr.DisplayMember = "ID"
        NewProjectNr.FormattingEnabled = True

        SubProjectMainView.Table = MainForm.DataSet.Tables("SubProject")
        SubProjectMainView.RowFilter = "FK_ID='" & NewProjectNr.Text & "'"

        NewSubProjectNr.DataSource = SubProjectMainView
        NewSubProjectNr.DisplayMember = "ID"
        NewSubProjectNr.FormattingEnabled = True

        SubProjectSubView.Table = MainForm.DataSet.Tables("SubProject")
        SubProjectSubView.RowFilter = "ID='" & NewSubProjectNr.Text & "'"

        SubProjectDBinder.DataSource = SubProjectSubView
        NewSubprojectName.DataBindings.Add(New Binding("Text", _
                                           SubProjectDBinder, "TableName", True))

        StageView.Table = MainForm.DataSet.Tables("Stage")
        StageView.RowFilter = "FK_ID='" & NewSubProjectNr.Text & "'"
        StageName.DataSource = StageView
        StageName.DisplayMember = "IndexName"
        StageName.FormattingEnabled = True

        Dim FilterString As String
        Dim ResultRows() As DataRow

        FilterString = "FK_ID='" & NewSubProjectNr.Text & "' AND IndexName='" & StageName.Text & "'"
        ResultRows = MainForm.DataSet.Tables("Stage").Select(FilterString)


        OrginalATRNumber.Text = MainForm.ATRnumber.Text
        OrginalATRName.Text = MainForm.ATRName.Text

        GenATRNumber.Text = Strings.Left(NewSubProjectNr.Text, 7) & ResultRows(0).Item("SubNr") & "XX"
    End Sub

    Private Sub NewProjectNr_SelectedIndexChanged(sender As Object, e As EventArgs) Handles NewProjectNr.SelectedIndexChanged
        Try
            Dim FilterString As String
            Dim ResultRows() As DataRow

            SubProjectMainView.RowFilter = "FK_ID='" & NewProjectNr.Text & "'"
            SubProjectSubView.RowFilter = "ID='" & NewSubProjectNr.Text & "'"
            StageView.RowFilter = "FK_ID='" & NewSubProjectNr.Text & "'"


            FilterString = "FK_ID='" & NewSubProjectNr.Text & "' AND IndexName='" & StageName.Text & "'"
            ResultRows = MainForm.DataSet.Tables("Stage").Select(FilterString)
            GenATRNumber.Text = Strings.Left(NewSubProjectNr.Text, 7) & ResultRows(0).Item("SubNr") & "XX"
        Catch ex As Exception
            Dim i = 0
        End Try

    End Sub

    Private Sub NewSubProjectNr_SelectedIndexChanged(sender As Object, e As EventArgs) Handles NewSubProjectNr.SelectedIndexChanged
        Try
            Dim FilterString As String
            Dim ResultRows() As DataRow

            SubProjectSubView.RowFilter = "ID='" & NewSubProjectNr.Text & "'"
            StageView.RowFilter = "FK_ID='" & NewSubProjectNr.Text & "'"

            FilterString = "FK_ID='" & NewSubProjectNr.Text & "' AND IndexName='" & StageName.Text & "'"
            ResultRows = MainForm.DataSet.Tables("Stage").Select(FilterString)
            GenATRNumber.Text = Strings.Left(NewSubProjectNr.Text, 7) & ResultRows(0).Item("SubNr") & "XX"
        Catch ex As Exception

        End Try

    End Sub

    Private Sub StageName_SelectedIndexChanged(sender As Object, e As EventArgs) Handles StageName.SelectedIndexChanged
        Try
            Dim FilterString As String
            Dim ResultRows() As DataRow

            FilterString = "FK_ID='" & NewSubProjectNr.Text & "' AND IndexName='" & StageName.Text & "'"
            ResultRows = MainForm.DataSet.Tables("Stage").Select(FilterString)
            GenATRNumber.Text = Strings.Left(NewSubProjectNr.Text, 7) & ResultRows(0).Item("SubNr") & "XX"
        Catch ex As Exception

        End Try
    End Sub
End Class