﻿Imports Microsoft.Office.Interop.Word
Imports Microsoft.Office.Interop
Imports System.IO
Public Module TRVMallDoc
    Private oWordApp As Word.Application
    Private oDoc As Word.Document
    Public TemplatePath As String = "C:\"
    ''' <summary>
    ''' Skapar ett Wordobjekt
    ''' stänger av att programmet visas och alla varningar
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub InitWord()
        Try
            oWordApp = New Word.Application
            oWordApp.Visible = True
            oWordApp.DisplayAlerts = WdAlertLevel.wdAlertsNone
        Catch ex As Exception
            MakeErrorReport("TRVMAllDocModul InitWord: Kan inte skapa/hantera object:", ex)
            oWordApp = Nothing
            Throw New ApplicationException("InitWordError:")
        End Try
    End Sub
    ''' <summary>
    ''' öppnar ATR-mall som ligger i samma mapp som exe filen dokument 
    ''' sparar undan dokumentet under rätt namn
    ''' </summary>
    ''' <param name="FilePath">
    ''' FilePath är sökväg + filnamn på den fil som fil som skall skapas
    ''' </param>
    ''' <remarks></remarks>
    Public Sub OpenWordDoc(FilePath As String)
        Try
            Dim DirectoryPath As String
            DirectoryPath = System.IO.Path.GetDirectoryName(FilePath)
            If Not System.IO.Directory.Exists(DirectoryPath) Then
                System.IO.Directory.CreateDirectory(DirectoryPath)
            End If
            ' & "\ATR-mall.xlsx"
            'System.IO.Directory.GetCurrentDirectory
            oDoc = oWordApp.Documents.Open(TemplatePath & "\ATR-mall.docx") ' öppnar malllen skrivskyddad
            'oDoc = oWordApp.ActiveDocument
            oDoc.SaveAs2(FilePath)

        Catch ex As Exception
            MsgBox("kan inte öppna fil: " & MainForm.WordTemplatePath.Text & "\ATR-mall.docx")
            MakeErrorReport("OpenWordDoc: kan inte öppna eller spara fil: ", ex)
            oDoc = Nothing
            Runtime.InteropServices.Marshal.FinalReleaseComObject(oWordApp)
            oWordApp = Nothing
            Throw New ApplicationException("OpenWordDoc:")
        End Try
    End Sub
    ''' <summary>
    ''' Funktionen fyller i textheader 
    ''' </summary>
    ''' <param name="BasicStrings">
    ''' Lista med strings skall fyllas enligt följande
    ''' 0. ATR Nummer (Filnamn)  
    ''' 1. [Skapat av ]
    ''' 2. [Dokumentdatum]
    ''' 3. Ev. ärendenummer
    ''' 4. Uppdragsnamn
    ''' 5. Uppdragsnummer </param>
    ''' 6. version 
    ''' 7. Dokumenttitel
    ''' <remarks>
    ''' Eftersom Funktionen bara jobbar mot en känd .doc mall görs fasta antaganden
    ''' </remarks>

    Public Sub TextToHeader(BasicStrings As List(Of String))
        Try
            With oDoc
                With .Sections(1)
                    Dim WrdRng As Word.Range = .Headers(WdHeaderFooterIndex.wdHeaderFooterPrimary).Range
                    Dim oHeaderTable As Word.Table = WrdRng.Tables(1)
                    oHeaderTable.Cell(2, 2).Range.Text = BasicStrings(0) ' ATR NUMMER: XX
                    oHeaderTable.Cell(5, 1).Range.Text = BasicStrings(1) ' [Skapat av]
                    oHeaderTable.Cell(5, 2).Range.Text = BasicStrings(2) ' [Dokumentdatum]
                    oHeaderTable.Cell(5, 3).Range.Text = BasicStrings(3) ' Ev. ärendenummer
                    oHeaderTable.Cell(7, 1).Range.Text = BasicStrings(4) ' Uppdragsnamn
                    oHeaderTable.Cell(7, 2).Range.Text = BasicStrings(5) ' Uppdragsnummer
                    oHeaderTable.Cell(7, 3).Range.Text = BasicStrings(6) ' Version
                    oHeaderTable.Cell(9, 1).Range.Text = BasicStrings(7) ' Dokumenttitel
                End With
            End With
            oDoc.Save()
        Catch ex As Exception
            MsgBox("Fel vid skrivning till Header")
            MakeErrorReport("TextToHeader:", ex)
            oDoc = Nothing
            Runtime.InteropServices.Marshal.FinalReleaseComObject(oWordApp)
            oWordApp = Nothing
            Throw New ApplicationException("TextToHeader:")
        End Try
    End Sub
    Public Sub TextToBody(BasicStrings As List(Of String), ByRef InTable As DataRow(), ByRef Deltable As DataRow(), ByRef RiTable As DataRow(), ByRef ATRTable As DataRow(), _
                          ByRef CTable As DataRow(), ByRef CatTable As Data.DataTable, ByRef ResTable As Data.DataTable)
        Try
            Dim sTmp As String
            Dim i As Integer
            Dim j As Integer = 0
            Dim BoldScopeStartPoint As Integer
            Dim InputTable As Word.Table
            Dim Deliverytable As Word.Table
            Dim RiskTable As Word.Table
            Dim ResourceTable As Word.Table
            Dim CostTable As Word.Table
            Dim Lenght As Integer = 0
            Dim TotalResourceCost As Integer = 0
            Dim TotalHours As Integer = 0
            Dim TotalFixedCost As Integer = 0
            Dim FoundFirstPara As Boolean = False
            Dim ParaTextList As New List(Of String)
            Dim ParaFormatList As New List(Of String)

            With oDoc
                With .Sections(1)
                    Dim WrdRng As Word.Range = .Range

                    sTmp = WrdRng.Tables(1).Range.Text ' DEBUG RAD TA BORT NÄR JAG ÄR KLAR
                    WrdRng.Tables(1).Cell(1, 2).Range.Text = BasicStrings(0) ' ATR Bl ID
                    WrdRng.Tables(1).Cell(2, 2).Range.Text = BasicStrings(1) ' DokumentNamn
                    WrdRng.Tables(1).Cell(3, 2).Range.Text = BasicStrings(2) ' Huvud Aktivitet

                    ' kolla om man måste flytta alla celler efter det här för att få det att funka
                    sTmp = WrdRng.Tables(1).Range.Text ' DEBUG RAD TA BORT NÄR JAG ÄR KLAR
                    WrdRng.Tables(1).Cell(4, 2).Range.Text = BasicStrings(3) ' Ansvarig för ATRblad
                    '''''********************  INPUT TABLE *****************'''''
                    If InTable.Length > 0 Then
                        InputTable = oDoc.Sections(1).Range.Tables.Add(WrdRng.Tables(1).Cell(5, 2).Range, 1, 2)
                        InputTable.Borders.Enable = True
                        InputTable.Borders(WdBorderType.wdBorderVertical).LineStyle = WdLineStyle.wdLineStyleSingle
                        InputTable.Borders.InsideLineStyle = WdLineStyle.wdLineStyleSingle
                        InputTable.Borders.OutsideLineStyle = WdLineStyle.wdLineStyleNone
                        InputTable.Cell(1, 1).Range.Text = "Förutsättning"
                        InputTable.Cell(1, 2).Range.Text = "Levererat från"
                        i = 0
                        For Each Row As DataRow In InTable
                            InputTable.Rows.Add()
                            If IsDBNull(Row.Item("FreeText1")) = False Then
                                InputTable.Cell(i + 2, 1).Range.Text = Row.Item("FreeText1")
                            End If
                            If IsDBNull(Row.Item("FreeText2")) = True Then
                                InputTable.Cell(i + 2, 2).Range.Text = " "
                            Else
                                InputTable.Cell(i + 2, 2).Range.Text = Row.Item("FreeText2")
                            End If
                            i = i + 1
                        Next
                        InputTable.Rows(1).Range.Font.Bold = True
                    End If

                    '''''******************** Scope Cell ********************'''''
                    BoldScopeStartPoint = -1
                    BoldScopeStartPoint = InStr(BasicStrings(4), "<b>") ' Instr visar 0 om str 2 inte finns i 

                    ' eftersom det blir extra newline när jag skapar paragrafer på annat sätt en med
                    ' Dim oPara2 As Word.Paragraph = oDoc.Content.Paragraphs.Add(WrdRng.Tables(1).Cell(6, 2).Range)
                    ' används denna rad, men med denna raden så läggs alltid nyast pragrafen först i cellen.
                    ' därför delas texten upp på paragrafer och sedan matas de in bakifrån och framåt

                    If BoldScopeStartPoint = 0 Then
                        ParaFormatList.Add("No")
                        ParaTextList.Add(BasicStrings(4))
                    End If

                    While BoldScopeStartPoint > 0
                        Dim EndPos As Integer = 0
                        BasicStrings(4) = BasicStrings(4).Remove(BoldScopeStartPoint - 1, 3)

                        If BoldScopeStartPoint > 1 And FoundFirstPara = False Then ' om det inte börjar med en Fet rubrik
                            Dim oPara2 As Word.Paragraph = oDoc.Content.Paragraphs.Add(WrdRng.Tables(1).Cell(6, 2).Range)
                            'Dim oPara2 As Word.Paragraph = WrdRng.Tables(1).Cell(6, 2).Range.Paragraphs.Add()
                            ParaTextList.Add(Left(BasicStrings(4), BoldScopeStartPoint - 1))
                            ParaFormatList.Add("No")
                            BasicStrings(4) = BasicStrings(4).Remove(0, BoldScopeStartPoint - 1) ' tabort det jag redan har skrivit
                        End If

                        EndPos = InStr(BasicStrings(4), "</b>")
                        ParaFormatList.Add("Bold")
                        ParaTextList.Add(Left(BasicStrings(4), EndPos - 1))
                        BasicStrings(4) = BasicStrings(4).Remove(0, EndPos - 1) ' tabort det jag redan har skrivit
                        BasicStrings(4) = BasicStrings(4).Remove(0, 4) ' </b> = 3 tecken  
                        FoundFirstPara = True

                        BoldScopeStartPoint = InStr(BasicStrings(4), "<b>") ' Instr visar 0 om str 2 inte finns i 
                        If BoldScopeStartPoint > 0 Then
                            Dim TmpPos As Integer = 1
                            TmpPos = InStr(BasicStrings(4), Chr(10))
                            If TmpPos > 0 And (TmpPos < BoldScopeStartPoint) Then
                                While TmpPos < BoldScopeStartPoint And TmpPos > 0
                                    If TmpPos = 1 Then
                                        BasicStrings(4) = BasicStrings(4).Remove(0, 1) ' tabort det jag redan har skrivit
                                        TmpPos = InStr(BasicStrings(4), Chr(10))
                                        BoldScopeStartPoint = InStr(BasicStrings(4), "<b>") ' Instr visar 0 om str 2 inte finns i 
                                        If TmpPos > 0 And TmpPos < BoldScopeStartPoint Then
                                            ParaTextList.Add(Left(BasicStrings(4), TmpPos - 1))
                                            ParaFormatList.Add("No")
                                            BasicStrings(4) = BasicStrings(4).Remove(0, TmpPos - 1) ' tabort det jag redan har skrivit
                                        ElseIf TmpPos > BoldScopeStartPoint Then
                                            ParaTextList.Add(Left(BasicStrings(4), BoldScopeStartPoint - 1))
                                            ParaFormatList.Add("No")
                                            BasicStrings(4) = BasicStrings(4).Remove(0, BoldScopeStartPoint - 1) ' tabort det jag redan har skrivit
                                        End If
                                    Else
                                        ParaTextList.Add(Left(BasicStrings(4), TmpPos - 1))
                                        ParaFormatList.Add("No")
                                        BasicStrings(4) = BasicStrings(4).Remove(0, TmpPos - 1) ' tabort det jag redan har skrivit
                                    End If
                                    TmpPos = InStr(BasicStrings(4), Chr(10))
                                    BoldScopeStartPoint = InStr(BasicStrings(4), "<b>") ' Instr visar 0 om str 2 inte finns i 
                                End While
                            Else
                                ParaFormatList.Add("No")
                                ParaTextList.Add(Left(BasicStrings(4), BoldScopeStartPoint - 1))
                                BasicStrings(4) = BasicStrings(4).Remove(0, BoldScopeStartPoint - 1) ' tabort det jag redan har skrivit
                                BoldScopeStartPoint = InStr(BasicStrings(4), "<b>") ' Instr visar 0 om str 2 inte finns i 
                            End If
                            
                        Else
                            ParaFormatList.Add("No")
                            ParaTextList.Add(BasicStrings(4))
                        End If
                    End While
                    j = ParaTextList.Count
                    While j > 0
                        j = j - 1
                        Dim oPara As Word.Paragraph = oDoc.Content.Paragraphs.Add(WrdRng.Tables(1).Cell(6, 2).Range)
                        oPara.ID = "Para" & j
                    End While

                    For j = 1 To ParaTextList.Count
                        Dim oPara As Word.Paragraph = WrdRng.Tables(1).Cell(6, 2).Range.Paragraphs(j)
                        oPara.Range.Text = ParaTextList(j - 1)
                        Select Case ParaFormatList(j - 1)
                            Case "No"
                                oPara.Range.Font.Bold = False
                                oPara.Range.Font.Underline = WdUnderline.wdUnderlineNone
                            Case "Bold"
                                oPara.Range.Font.Bold = True
                                oPara.Range.Font.Underline = WdUnderline.wdUnderlineNone
                            Case "Underline"
                                oPara.Range.Font.Bold = False
                                oPara.Range.Font.Underline = WdUnderline.wdUnderlineSingle
                            Case "BoldUnderline"
                                oPara.Range.Font.Bold = True
                                oPara.Range.Font.Underline = WdUnderline.wdUnderlineSingle
                            Case Else
                                oPara.Range.Font.Bold = False
                                oPara.Range.Font.Underline = WdUnderline.wdUnderlineNone
                        End Select
                    Next
                    '''''******************** Result Cell *******************'''''
                    WrdRng.Tables(1).Cell(7, 2).Range.Text = BasicStrings(5) ' resultat
                    '''''********************  OUTPUT TABLE *****************'''''
                    i = 0
                    If Deltable.Length > 0 Then
                        Deliverytable = oDoc.Sections(1).Range.Tables.Add(WrdRng.Tables(1).Cell(8, 2).Range, 1, 2)
                        Deliverytable.Borders.Enable = True
                        Deliverytable.Borders(WdBorderType.wdBorderVertical).LineStyle = WdLineStyle.wdLineStyleSingle
                        Deliverytable.Borders.InsideLineStyle = WdLineStyle.wdLineStyleSingle
                        Deliverytable.Borders.OutsideLineStyle = WdLineStyle.wdLineStyleNone
                        Deliverytable.Cell(1, 1).Range.Text = "Leverans"
                        Deliverytable.Cell(1, 2).Range.Text = "Påverkad aktivitet/deluppdrag"
                        For Each Row As DataRow In Deltable
                            Deliverytable.Rows.Add()
                            If IsDBNull(Row.Item("FreeText1")) = False Then
                                Deliverytable.Cell(i + 2, 1).Range.Text = Row.Item("FreeText1")
                            End If

                            If IsDBNull(Row.Item("FreeText2")) = True Then
                                Deliverytable.Cell(i + 2, 2).Range.Text = " "
                            Else
                                Deliverytable.Cell(i + 2, 2).Range.Text = Row.Item("FreeText2")
                            End If
                            i = i + 1
                        Next
                        Deliverytable.Rows(1).Range.Font.Bold = True
                    End If


                    WrdRng.Tables(1).Cell(9, 2).Range.Text = "Startdatum: " & BasicStrings(6) ' Startdatum
                    WrdRng.Tables(1).Cell(9, 3).Range.Text = "Slutdatum: " & BasicStrings(7) ' Slutdatum

                    '''''********************  RISKS TABLE *****************'''''
                    If RiTable.Length > 0 Then
                        RiskTable = oDoc.Sections(1).Range.Tables.Add(WrdRng.Tables(1).Cell(10, 2).Range, 1, 2)
                        RiskTable.Borders.Enable = True
                        RiskTable.Borders(WdBorderType.wdBorderVertical).LineStyle = WdLineStyle.wdLineStyleSingle
                        RiskTable.Borders.InsideLineStyle = WdLineStyle.wdLineStyleSingle
                        RiskTable.Borders.OutsideLineStyle = WdLineStyle.wdLineStyleNone
                        RiskTable.Cell(1, 1).Range.Text = "Risk"
                        RiskTable.Cell(1, 2).Range.Text = "Riskminimering"
                        i = 0
                        For Each Row As DataRow In RiTable
                            RiskTable.Rows.Add()
                            If IsDBNull(Row.Item("FreeText1")) = False Then
                                RiskTable.Cell(i + 2, 1).Range.Text = Row.Item("FreeText1")
                            End If

                            If IsDBNull(Row.Item("FreeText2")) = True Then
                                RiskTable.Cell(i + 2, 2).Range.Text = " "
                            Else
                                RiskTable.Cell(i + 2, 2).Range.Text = Row.Item("FreeText2")
                            End If
                            i = i + 1
                        Next
                        RiskTable.Rows(1).Range.Font.Bold = True
                    End If

                    'WrdRng.Tables(1).Cell(10, 2).Range.Text = BasicStrings(8) ' Risker
                    'WrdRng.Tables(1).Cell(12, 1).Range.Text = WrdRng.Tables(1).Cell(12, 1).Range.Text & Environment.NewLine

                    '''''********************  RESOURCE TABLE *****************'''''
                    If ATRTable.Length > 0 Then
                        ResourceTable = oDoc.Sections(1).Range.Tables.Add(WrdRng.Tables(1).Cell(12, 1).Range, 1, 5)
                        ResourceTable.Borders.InsideLineStyle = WdLineStyle.wdLineStyleSingle
                        ResourceTable.Columns(1).Width = 140
                        ResourceTable.Columns(2).Width = 140
                        ResourceTable.Columns(3).Width = 40
                        ResourceTable.Columns(4).Width = 40
                        ResourceTable.Columns(5).Width = 60
                        ResourceTable.Range.Font.Size = 9
                        ResourceTable.Rows(1).Borders(WdBorderType.wdBorderBottom).LineStyle = WdLineStyle.wdLineStyleDouble
                        ResourceTable.Cell(1, 1).Range.Text = "Resurser"
                        ResourceTable.Cell(1, 2).Range.Text = "Rollbeskrivning"
                        ResourceTable.Cell(1, 3).Range.Text = "Timmar"
                        ResourceTable.Cell(1, 4).Range.Text = "Timpris"
                        ResourceTable.Cell(1, 5).Range.Text = "Tidskostnad"




                        Lenght = ATRTable.Length
                        For i = 0 To Lenght
                            ResourceTable.Rows.Add()
                        Next

                        i = 0
                        For Each Row As DataRow In ATRTable
                            Dim FindResourceKey(1) As Object
                            FindResourceKey(0) = Row.Item("FK_RID")
                            FindResourceKey(1) = Row.Item("FK_CatID")

                            Dim CatRow As DataRow = CatTable.Rows.Find(Row.Item("FK_CatID"))
                            Dim ResRow As DataRow = ResTable.Rows.Find(FindResourceKey)
                            Dim Tmpstring As String = ""
                            'Tmpstring = FindInString(CatRow.Item("Freetext"), "(") ' leta upp eventuell parantes och ta bort
                            ResourceTable.Cell(i + 2, 1).Range.Text = ResRow.Item("FreeText")
                            If IsDBNull(ResRow.Item("RoleText")) = True Then
                                ResourceTable.Cell(i + 2, 2).Range.Text = " "
                            Else
                                ResourceTable.Cell(i + 2, 2).Range.Text = ResRow.Item("RoleText")
                            End If
                            ResourceTable.Cell(i + 2, 3).Range.Text = String.Format("{0:N0}", Row.Item("TimeH"))
                            ResourceTable.Cell(i + 2, 4).Range.Text = String.Format("{0:N0}", CatRow.Item("Cost"))
                            ResourceTable.Cell(i + 2, 5).Range.Text = String.Format("{0:N0}", Row.Item("TimeH") * CatRow.Item("Cost"))
                            TotalResourceCost = TotalResourceCost + (Row.Item("TimeH") * CatRow.Item("Cost"))
                            TotalHours = TotalHours + Row.Item("TimeH")
                            i = i + 1
                        Next

                        ResourceTable.Rows(Lenght + 2).Borders(WdBorderType.wdBorderTop).LineStyle = WdLineStyle.wdLineStyleDouble
                        ResourceTable.Cell(Lenght + 2, 1).Range.Text = "Totalt Resurser"
                        ResourceTable.Cell(Lenght + 2, 3).Range.Text = String.Format("{0:N0}", TotalHours)
                        ResourceTable.Cell(Lenght + 2, 5).Range.Text = String.Format("{0:N0}", TotalResourceCost)
                        ResourceTable.Rows(Lenght + 2).Range.Font.Bold = True
                        ResourceTable.Rows(1).Range.Font.Bold = True

                    End If
                    '''''********************  COST TABLE *****************'''''
                    If CTable.Length > 0 Then
                        CostTable = oDoc.Sections(1).Range.Tables.Add(WrdRng.Tables(1).Cell(13, 1).Range, 1, 2)
                        CostTable.Borders.InsideLineStyle = WdLineStyle.wdLineStyleSingle
                        CostTable.Columns(1).Width = 356
                        CostTable.Columns(2).Width = 60
                        CostTable.Range.Font.Size = 9
                        CostTable.Rows(1).Borders(WdBorderType.wdBorderBottom).LineStyle = WdLineStyle.wdLineStyleDouble
                        CostTable.Cell(1, 1).Range.Text = "Särskild ersättning"
                        CostTable.Cell(1, 2).Range.Text = "Totalt"

                        Lenght = CTable.Length

                        For i = 0 To Lenght
                            CostTable.Rows.Add()
                        Next

                        i = 0
                        For Each Row As DataRow In CTable
                            If IsDBNull(Row.Item("FreeText")) = False Then
                                CostTable.Cell(i + 2, 1).Range.Text = Row.Item("FreeText")
                            End If
                            If IsDBNull(Row.Item("Cost")) = False Then
                                CostTable.Cell(i + 2, 2).Range.Text = String.Format("{0:N0}", Row.Item("Cost"))
                                TotalFixedCost = TotalFixedCost + Row.Item("Cost")
                            End If
                            i = i + 1
                        Next

                        CostTable.Cell(Lenght + 2, 1).Range.Text = "Totalt Kostnad särskild ersättning"
                        CostTable.Cell(Lenght + 2, 2).Range.Text = String.Format("{0:N0}", TotalFixedCost)
                        CostTable.Rows(Lenght + 2).Range.Font.Bold = True
                        CostTable.Rows(1).Range.Font.Bold = True
                    End If


                    '''''********************  TOTAL SUMMA ******************''''''
                    WrdRng.Tables(1).Cell(14, 1).Range.Text = "Total ATR kostnad"
                    WrdRng.Tables(1).Cell(14, 2).Range.Text = String.Format("{0:N0}", TotalResourceCost + TotalFixedCost)
                    WrdRng.Tables(1).Rows(14).Range.Font.Bold = True

                End With
            End With
        Catch ex As Exception
            MsgBox("Fel vid skrivning till textblock i Word")
            MakeErrorReport("TextToBody: ", ex)
            oDoc = Nothing
            Runtime.InteropServices.Marshal.FinalReleaseComObject(oWordApp)
            oWordApp = Nothing

            Throw New ApplicationException("TextToBody:")

        End Try
    End Sub
    Public Sub ExitDoc(ATRInWord As Boolean)
        Dim PDFName As String = ""
        Dim WordFileName As String = ""
        Try
            Dim FilePath As String = ""
            Dim FileAtribute As System.IO.FileAttributes
            FilePath = oDoc.Path
            If StrComp(FilePath, TemplatePath) <> 0 Then ' förstör inte mallen utan kasta bara bort kopplingen till den 
                oDoc.Save()

                WordFileName = oDoc.FullName
                PDFName = Strings.Left(WordFileName, Strings.Len(WordFileName) - 4) & "pdf"
                If System.IO.File.Exists(PDFName) Then
                    FileAtribute = System.IO.File.GetAttributes(PDFName)
                    System.IO.File.Delete(PDFName)
                End If
                oDoc.SaveAs2(PDFName, Word.WdSaveFormat.wdFormatPDF)
                oDoc.Close()
                If ATRInWord = False Then
                    System.IO.File.Delete(WordFileName)
                End If
            End If
            oDoc = Nothing

        Catch ex As Exception
            If StrComp(ex.Message, "The process cannot access the file '" & PDFName & "' because it is being used by another process.") = 0 Then
                oDoc.Close()
                System.IO.File.Delete(WordFileName)
                oDoc = Nothing
                MsgBox("PDF-filen som skall skapas är öppen." & Environment.NewLine & "Stäng filen och skapa ATR-bland igen")
            Else
                MsgBox("Fel vid stängning av dokument")
                MakeErrorReport("ExitDoc: ", ex)
                Throw New ApplicationException("ExitDoc:")
                oWordApp = Nothing
                oDoc = Nothing
            End If

        End Try
    End Sub

    Public Sub ExitWord()
        Try
            oWordApp.Quit()
            oWordApp = Nothing
        Catch ex As Exception
            MakeErrorReport("ExitWord: ", ex)
            Throw New ApplicationException("ExitWord:")
            oWordApp = Nothing
            oDoc = Nothing
        End Try
    End Sub
End Module
