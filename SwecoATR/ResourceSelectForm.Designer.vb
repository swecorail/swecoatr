﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class ResourceSelectForm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.ResourceSelectGrid = New System.Windows.Forms.DataGridView()
        Me.LeaveSelect = New System.Windows.Forms.Button()
        CType(Me.ResourceSelectGrid, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ResourceSelectGrid
        '
        Me.ResourceSelectGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.ResourceSelectGrid.Location = New System.Drawing.Point(12, 12)
        Me.ResourceSelectGrid.Name = "ResourceSelectGrid"
        Me.ResourceSelectGrid.RowTemplate.Height = 24
        Me.ResourceSelectGrid.Size = New System.Drawing.Size(787, 211)
        Me.ResourceSelectGrid.TabIndex = 0
        '
        'LeaveSelect
        '
        Me.LeaveSelect.Location = New System.Drawing.Point(614, 246)
        Me.LeaveSelect.Name = "LeaveSelect"
        Me.LeaveSelect.Size = New System.Drawing.Size(185, 32)
        Me.LeaveSelect.TabIndex = 1
        Me.LeaveSelect.Text = "OK"
        Me.LeaveSelect.UseVisualStyleBackColor = True
        '
        'ResourceSelectForm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(811, 290)
        Me.Controls.Add(Me.LeaveSelect)
        Me.Controls.Add(Me.ResourceSelectGrid)
        Me.Name = "ResourceSelectForm"
        Me.Text = "Välj kategori"
        CType(Me.ResourceSelectGrid, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents ResourceSelectGrid As System.Windows.Forms.DataGridView
    Friend WithEvents LeaveSelect As System.Windows.Forms.Button
End Class
